#
# Makefile for TotemBioNet
#
# Author: Erick Gallesio [eg@unice.fr]
#
# ----------------------------------------------------------------------
# Configuration
# ----------------------------------------------------------------------

# Set the installation directory
INSTALL_DIR=/usr/local/TotemBioNet

# Assign NuSMV executable path
# If you don't have it already installed, a zip file is in lib/NuSMV/linux folder
# (unzip it and precise the path of the executable)
NUSMV=lib/NuSMV/linux/NuSMV-2.6.0-Linux/bin/NuSMV


# ----------------------------------------------------------------------
# Code below should not be changed
# ----------------------------------------------------------------------

# The generated shell scripts
SCRIPTS= ./totembionet ./ASTTreeViewer

# ---- Libraries
ANTLR=lib/antlr4/antlr-4.7.1-complete.jar
ANTTL_RUNTIME=lib/antlr4/antlr-runtime-4.7.2.jar
JSON=lib/json-20180813.jar

# ---- Java stuff
JAVAC=javac
JAVA=java
JPATH=$(ANTLR):$(JSON)
JAVAC_FLAGS= -nowarn -d TotemBioNet/bin -cp $(JPATH):$(ANTLR_RUNTIME) -sourcepath TotemBioNet/src
JAVA_FLAGS = TotemBioNet/bin:$(JPATH)


.PHONY: all  updateSMBGrammar updateHoareGrammar install clean distclean

all: $(SCRIPTS)
	mkdir -p TotemBioNet/bin
	$(JAVAC) $(JAVAC_FLAGS) TotemBioNet/src/back/run/Main.java

$(SCRIPTS): Makefile
	for file in $(SCRIPTS) ;do \
	  (sed -e "s%@JAVA@%$(JAVA)%"                  |  \
	   sed -e "s%@NUSMVPATH@%$(NUSMV)%"            |  \
	   sed -e "s%@JAVA_FLAGS@%$(JAVA_FLAGS)%")        \
          < TotemBioNet/templates/$$file.templ > $$file; \
	done
	chmod +x $(SCRIPTS)

install: 
	mkdir -p $(INSTALL_DIR)
	cp -a $(SCRIPTS) TotemBioNet lib $(INSTALL_DIR)
	@echo "Installation done in $(INSTALL_DIR) directory"

clean:
	rm -rf TotemBioNet/bin
	rm -f  $(SCRIPTS)

distclean: clean
	rm -rf ./lib/NuSMV/linux/NuSMV-2.6.0-Linux 

# ----------------------------------------------------------------------
# Grammar Updates
# ----------------------------------------------------------------------
updateSMBGrammar:
	@echo "Updating SMB Grammar..."
	lib/antlr4/antlr4.sh -package front.smbParser.grammar \
		-visitor TotemBioNet/src/front/smbParser/grammar/RRG.g4
	$(JAVAC) $(JAVAC_FLAGS) TotemBioNet/src/back/run/Main.java
	@echo "Done".

updateHoareGrammar:
	@echo "Updating Hoare Grammar..."
	lib/antlr4/antlr4.sh -package front.hoareFol.grammar \
		-visitor TotemBioNet/src/front/hoareFol/grammar/HoareFol.g4
	$(JAVAC) $(JAVAC_FLAGS) TotemBioNet/src/back/run/Main.java
	@echo "Done."
