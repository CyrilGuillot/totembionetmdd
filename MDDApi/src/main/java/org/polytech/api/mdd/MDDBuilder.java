package org.polytech.api.mdd;

import org.colomoto.mddlib.MDDManager;
import org.colomoto.mddlib.MDDManagerFactory;
import org.colomoto.mddlib.MDDVariable;
import org.colomoto.mddlib.MDDVariableFactory;
import org.polytech.api.util.CSVReader;
import org.polytech.api.util.ShellLogger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MDDBuilder {

    public static MDDManager create(String name, String path, String type) {

        List<List<String>> rows;
        // get csv data
        try {
            CSVReader csvReader = new CSVReader(path);
            rows = csvReader.getRecords();
        }catch (IOException e) {
            ShellLogger.mdd_info(name, "Invalid path");
            return null;
        }

        // get variables
        int n_variables = rows.get(0).size() - 3 ;
        List<String> variables = rows.get(0).subList(2,n_variables+2);
        ShellLogger.mdd_info(name, variables.size() + " variables detected, " + variables);

        // get rows
        List<int[]> valid_rows = new ArrayList<>();
        for (List<String> row : rows) {
            if (row.get(1).equals(type)) {
                List<String> str_values = row.subList(2, n_variables+2);
                int[] values = new int[str_values.size()];
                // convert str row to int
                for (int i = 0 ; i < values.length; i++) {
                    values[i] = Integer.parseInt(str_values.get(i));
                }
                valid_rows.add(values);
            }
        }
        ShellLogger.mdd_info(name, valid_rows.size() + " " + type + " set detected.");


        // create variable factory
        MDDVariableFactory vbuilder = create_variable_factory(variables, rows);

        // build mdd using mddManager
        MDDManager mddmanager = MDDManagerFactory.getManager( vbuilder, 1);
        MDDVariable[] mdd_variables = mddmanager.getAllVariables();
        add_all_paths_from_rows(mddmanager, valid_rows, mdd_variables);
        ShellLogger.mdd_info(name, mddmanager.getNodeCount() + " nodes created");

        return mddmanager;
    }


    private static MDDVariableFactory create_variable_factory(List<String> variables, List<List<String>> rows) {
        MDDVariableFactory vbuilder = new MDDVariableFactory();
        for (int i = 0 ; i < variables.size(); i++) {
            String var_name = variables.get(i);
            //since we dont have the possible values for each variable in the CSV, we take the maximum value
            Integer max = 0;
            for (List<String> row : rows.subList(1, rows.size() )) {
                Integer value = Integer.parseInt(row.get(i + 2));
                if (value > max) max = value;
            }
            max += 1;
            vbuilder.add(var_name, max.byteValue());
        }
        return vbuilder;
    }

    private static int add_path_from_row(MDDVariable[] variables, int[] row) {
        int n_var = variables.length;
        int last_node = 0;
        for (int i = n_var-1 ; i >= 0; i--) {
            MDDVariable variable = variables[i];
            int[] values = new int[variable.nbval];
            Arrays.fill(values, -1);
            values[row[i]] = last_node;
            last_node = variable.getNode(values);
        }
        return last_node;
    }

    private static void add_all_paths_from_rows(MDDManager mddManager, List<int[]> rows, MDDVariable[] mdd_variables) {
        ArrayList<Integer> start_nodes = new ArrayList<>();
        for (int[] row : rows) {
            start_nodes.add(add_path_from_row(mdd_variables, row));
        }
    }

}
