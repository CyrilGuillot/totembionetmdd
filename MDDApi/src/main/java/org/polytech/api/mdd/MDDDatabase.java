package org.polytech.api.mdd;

import org.colomoto.mddlib.MDDManager;
import java.util.HashMap;
import org.polytech.api.util.ShellLogger;

public class MDDDatabase {

    private HashMap<String, MDDManager> mddmanagers;

    public MDDDatabase() {
        mddmanagers = new HashMap<>();
    }

    public void create(String name, String path, String type) {

        // check if name exists
        if (mddmanagers.containsKey(name)) {
            ShellLogger.mdd_info(name, "name not available");
            return;
        }
        //build and save manager
        MDDManager mddmanager = MDDBuilder.create(name, path, type);
        if (mddmanager != null) mddmanagers.put(name, mddmanager);
    }
}
