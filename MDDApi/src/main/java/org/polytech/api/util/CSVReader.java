package org.polytech.api.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.polytech.api.util.ShellLogger;


public class CSVReader {

    String line = "";
    String cvsSplitBy = ",";

    List<List<String>> records = new ArrayList<>();
    BufferedReader br;

    public CSVReader(String path) throws IOException {
        br = new BufferedReader(new FileReader(path));
        while ((line = br.readLine()) != null) {
            String[] values = line.split(";");
            records.add(Arrays.asList(values));
        }
    }

    public List<List<String>> getRecords() {
        return records;
    }
}
