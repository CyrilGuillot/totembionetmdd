package org.polytech.api.util;

public class ShellLogger {

    public static void mdd_info(String mdd_name, String msg) {
        String out = "[" + mdd_name + "] : " + msg;
        System.out.println(out);
    }

}
