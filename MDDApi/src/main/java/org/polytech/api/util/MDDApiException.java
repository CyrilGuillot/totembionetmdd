package org.polytech.api.util;

/**
 * @author Cyril Guillot
 */

public class MDDApiException extends Exception {

    public MDDApiException(String m) {
        super("[MDDApi error] " + m);
    }
}
