package org.polytech.api.run;


import org.polytech.api.mdd.MDDDatabase;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class shell {

    public static MDDDatabase mddDatabase = new MDDDatabase();

    public static void main(String[] args) throws java.io.IOException {

        String commandLine;
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        while (true) {

            System.out.print("mdd_shell>");
            commandLine = console.readLine();

            //loop
            if (commandLine.equals("")) {
                continue;
            }

            //help command
            if (commandLine.equals("help"))
            {
                System.out.println();
                System.out.println("Commands to use:");
                System.out.println("1) create mdd_name csv_path type=[OK or KO]");
                System.out.println("2) exit");
                System.out.println("3) clear");
                System.out.println("---------------------");
                System.out.println();
            }

            if (commandLine.equals("clear"))
            {

                for ( int cls = 0; cls < 10; cls++ )
                {
                    System.out.print("");
                }

            }

            if (commandLine.startsWith("create"))
            {
                String[] parts = commandLine.split(" ");
                if (parts.length >= 4) {
                    String mdd_name = parts[1];
                    String path = parts[2];
                    String type = parts[3];
                    mddDatabase.create(mdd_name, path, type);
                }
                else {
                    System.out.println("Missing arguments, syntax is : create mdd_name csv_path");
                }
            }

            if (commandLine.equals("exit"))
            {

                System.out.println("...Terminating the Virtual Machine");
                System.out.println("...Done");
                System.exit(0);
            }

        }
    }
}
