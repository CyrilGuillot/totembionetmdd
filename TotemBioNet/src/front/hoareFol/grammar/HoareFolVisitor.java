// Generated from TotemBioNet/src/front/hoareFol/grammar/HoareFol.g4 by ANTLR 4.7.1
package front.hoareFol.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link HoareFolParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface HoareFolVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link HoareFolParser#root}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoot(HoareFolParser.RootContext ctx);
	/**
	 * Visit a parse tree produced by {@link HoareFolParser#rootMember}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRootMember(HoareFolParser.RootMemberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_neg}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_neg(HoareFolParser.Expr_negContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_bool_op}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_bool_op(HoareFolParser.Expr_bool_opContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_brackets}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_brackets(HoareFolParser.Expr_bracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_false}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_false(HoareFolParser.Expr_falseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_true}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_true(HoareFolParser.Expr_trueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_atome}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_atome(HoareFolParser.Expr_atomeContext ctx);
}