// Generated from TotemBioNet/src/front/hoareFol/grammar/HoareFol.g4 by ANTLR 4.7.1
package front.hoareFol.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HoareFolParser}.
 */
public interface HoareFolListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HoareFolParser#root}.
	 * @param ctx the parse tree
	 */
	void enterRoot(HoareFolParser.RootContext ctx);
	/**
	 * Exit a parse tree produced by {@link HoareFolParser#root}.
	 * @param ctx the parse tree
	 */
	void exitRoot(HoareFolParser.RootContext ctx);
	/**
	 * Enter a parse tree produced by {@link HoareFolParser#rootMember}.
	 * @param ctx the parse tree
	 */
	void enterRootMember(HoareFolParser.RootMemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link HoareFolParser#rootMember}.
	 * @param ctx the parse tree
	 */
	void exitRootMember(HoareFolParser.RootMemberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_neg}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_neg(HoareFolParser.Expr_negContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_neg}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_neg(HoareFolParser.Expr_negContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_bool_op}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_bool_op(HoareFolParser.Expr_bool_opContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_bool_op}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_bool_op(HoareFolParser.Expr_bool_opContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_brackets}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_brackets(HoareFolParser.Expr_bracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_brackets}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_brackets(HoareFolParser.Expr_bracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_false}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_false(HoareFolParser.Expr_falseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_false}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_false(HoareFolParser.Expr_falseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_true}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_true(HoareFolParser.Expr_trueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_true}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_true(HoareFolParser.Expr_trueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_atome}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_atome(HoareFolParser.Expr_atomeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_atome}
	 * labeled alternative in {@link HoareFolParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_atome(HoareFolParser.Expr_atomeContext ctx);
}