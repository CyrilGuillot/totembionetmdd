package front.hoareFol.hoareParser;

import front.hoareFol.grammar.HoareFolLexer;
import front.hoareFol.grammar.HoareFolParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.IOException;

/**
 * classe de lancement du parsing
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 */
public class Parser {

    public boolean parse(String path) throws Exception {
        CharStream inputStream = null;
        String inputFile = path;
        try {
            //System.out.println(inputFile);
            inputStream = CharStreams.fromFileName(inputFile);
        } catch (IOException io) {
            io.printStackTrace();
            return false;
        }

        HoareFolLexer lexer = new HoareFolLexer(inputStream);
        CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
        HoareFolParser parser = new HoareFolParser(commonTokenStream);
        HoareFolParser.RootContext rootContext = parser.root();

        int syntaxErrorsCount = parser.getNumberOfSyntaxErrors();
        if (syntaxErrorsCount > 0) {
            throw new Exception(" *** Failure - " + syntaxErrorsCount + " syntax errors *** ");
        } else {
            Visitor visitor = new Visitor();
            visitor.visit(rootContext);
            // si c'est non valide, on génère quand même un JSON
            // pour que le back récupère l'information
            // TODO : ne pas passer par tous les scripts et
            //        intégrer HoareFol comme un package => on se servira de l'exception
            visitor.getOutputJSON(inputFile.substring(0, inputFile.length() - 4));
            if (!visitor.isValid()) {
                //Throws exception
                throw new Exception(" *** Failure - HoareTriple Unsatisfied*** ");
            }
        }
        return true;
    }
}
