package front.hoareFol.hoareParser;


import back.net.Para;
import front.hoareFol.grammar.HoareFolBaseVisitor;
import front.hoareFol.grammar.HoareFolParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * visiteurs à modifier si besoin
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 * @author helen
 */

public class Visitor extends HoareFolBaseVisitor<String> {

    private boolean valid;
    private List<String> expressions;

    public Visitor() {
        this.expressions = new ArrayList<>();
        this.valid = true;
    }

    public boolean isValid(){
        return valid;
    }

    public String visitExpr_bool_op(HoareFolParser.Expr_bool_opContext ctx) {
        return visitChildren(ctx);
    }

    // visite successivement le ctx de la pre et de la valeur
    // comme il n'y a pas de règle pour false, si la valeur calculée est
    // false, ctx est null et donc on déduit que le triplet n'est pas valide.
    public String visitRootMember(HoareFolParser.RootMemberContext ctx) {
        if(ctx.expr() != null ){
            this.expressions.add(createJsonFromExpr(ctx.expr()));
        } else {
            // TODO : la grammaire accepte seulement false
            // rootMember    : '(' expr ')' | 'false' ;
            // donc si on obtient true c'est une erreur de syntaxe.
            //   Peut-on obtenir true pour le triplet  ? oui si on a aussi en pre les
            //   valeurs des premisses ok pour les K
            this.expressions.add("false");
            this.valid = false;
        }
        return visitChildren(ctx);
    }

    public String createJsonFromExpr(HoareFolParser.ExprContext ctx){
        String jsonExpr = "";
       switch(ctx.getClass().getSimpleName()) {
           case "Expr_trueContext": {
                jsonExpr += "\"type\": \"boolean\", \"value\": true";
           }
           break;
           case "Expr_falseContext": {
               jsonExpr += "\"type\": \"boolean\", \"value\": false";
           }
           break;
           case "Expr_negContext": {
                jsonExpr += " \"type\": \"negExpr\", \"op\" : \"" + ((HoareFolParser.Expr_negContext)ctx).NEG().getText()
                         + "\", \"expr\" : { " + createJsonFromExpr(((HoareFolParser.Expr_negContext)ctx).expr()) + " }";
           }
           break;

           case "Expr_bool_opContext": {
               jsonExpr += " \"type\": \"boolExpr\", \"op\" : \"" + ((HoareFolParser.Expr_bool_opContext)ctx).BOOL_OP().getText()
                       + "\", \"expr1\" : { " + createJsonFromExpr(((HoareFolParser.Expr_bool_opContext)ctx).expr(0)) + " },"
                       + " \"expr2\" : { " + createJsonFromExpr(((HoareFolParser.Expr_bool_opContext)ctx).expr(1)) + " }";
           }
           break;

           case "Expr_bracketsContext": {
                jsonExpr += createJsonFromExpr(((HoareFolParser.Expr_bracketsContext)ctx).expr());
           }
           break;

           case "Expr_atomeContext": {
               String idText = "";
               // on trie pour que ce soit cohérent avec le nom issu du fichier .smb
               if (((HoareFolParser.Expr_atomeContext)ctx).KID() != null)
                   idText =  Para.sortName(((HoareFolParser.Expr_atomeContext)ctx).KID().getText());
               else
                   idText =((HoareFolParser.Expr_atomeContext)ctx).ID().getText();
                jsonExpr += " \"type\": \"atome\", \"op\" : \"" + ((HoareFolParser.Expr_atomeContext)ctx).COMP().getText()
                        + "\", \"id\": \"" + idText
                        + "\", \"value\" : " + ((HoareFolParser.Expr_atomeContext)ctx).NUM().getText();
               //System.out.println("val " + jsonExpr);
           }
           break;
           default: {
                System.err.println("Error : Unknwown Expr type.");
           }
           break;
       }
        return jsonExpr;
   }

    public void getOutputJSON(String outputFilename){
        if(expressions.size() > 0){
            String json = "{\"blocktype\": \"hoareOutput\", \"value\" : {";
            json += " \"pre\" : { " + this.expressions.get(0)
                    + "}, \"res\" :  ";
            if (!isValid())
                json += "{\"type\" : \"unsatisfiable\"}";
            else {

                // TODO : à vérifier
                // il y a seulement 1 expression ?
                for (int i = 1; i < expressions.size(); i++) {
                    json += "{ " + expressions.get(i) + "}";
                }
            }
            //json = json.substring(0, json.length() - 1);

            json += "}}";

            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(outputFilename + ".json"));
                out.write(json);
                out.close();
                System.out.println(" *** Success : weakest precondition has been generated in : " + outputFilename + ".out and .json files *** ");
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            System.err.println("No expression parsed - output is empty");
        }

    }
}
