#ANTLR Setup

ANTLR is actually made up of two main parts: the tool, used to generate the lexer and
parser, and the runtime, needed to back.run them.

The tool will be needed just by you, the language engineer, while the runtime will be
included in the final software using your language.

The tool is always the same no matter which language you are targeting: it’s a Java
program that you need on your development machine. While the runtime is different for
every language and must be available both to the developer and to the user.
The only requirement for the tool is that you have installed at least Java 1.7 . To install the
Java program you need to download the latest version from the official site, which at the
moment is:

`http://www.antlr.org/download/antlr-4.7.1-complete.jar`

Also, download the runtime package jar :

`http://www.antlr.org/download/antlr-runtime-4.7.2.jar`


Instructions
1. Create an antlr4 folder where you usually put third-party java libraries (ex. C:\Program Files\Java\libs for Windows or /usr/local/lib) 
   & Copy the downloaded tool there.
2. Add the tool to your CLASSPATH (environment variable). Add it to your startup script (ex. .bash_profile)
3. (Optional) add also aliases to your startup script to simplify the usage of ANTLR

#### Executing the instructions on Windows
1. Copy antlr-4.7-complete.jar in C:\Program Files\Java\libs\antlr (or wherever you prefer or C:\Program Files\Java\antlr)
2. Create or append to the CLASSPATH variable the location of antlr complete and runtime packages :
   	You can do to that by going to WIN + R and typing sysdm.cpl
    Then selecting Advanced (tab) > Environment variables > System Variables
    Add a CLASSPATH environment variable if it doesn't already exist
    Then append C:\Program Files\Java\antlr4\antlr-4.7.1-complete.jar;C:\Program Files\Java\antlr4\antlr-runtime-4.7.2.jar
3. Add aliases
   // create antlr4.bat in the antlr4 folder then paste

   `java org.antlr.v4.Tool %*`

   // create grun.bat then paste

   `java -cp ".;C:\Program Files\Java\antlr4\antlr-4.7.1-complete.jar" org.antlr.v4.gui.TestRig %*`

   //put them in the system path or any of the directories included in then %path% environment variable
   	See Step 2 to modify environment variables
   	Append to the "Path" variable the path to the antlr4 folder


#### Executing the instructions on Linux/Mac OS (To be tested)
1. sudo -cp antlr-4.7-complete.jar/usr/local/lib/

2. Add this to your .bash_profile

   `export CLASSPATH = ".:/usr/local/lib/antlr-4.7-complete.jar:$CLASSPATH"`
  
3.  Simplify the use of the tool to generate lexer and parser
   
   `alias antlr4='java -jar /usr/local/lib/antlr-4.7-complete.jar'`


# Adding a grammar rule