package front.smbParser;

import back.jlogic.Formula;
import back.jlogic.blogic.*;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 * @author helen
 *
 */

enum RegType {
    OPERATION, ATOME;
}

public class RegOp {
    private RegType type;

    private String operateur;
    private RegOp expr1;
    private RegOp expr2;

    private Var var;
    private String seuil;


    public RegOp(RegType type, String operateur, RegOp expr1, RegOp expr2) {
        this(type,operateur,expr1,expr2,null,null);
    }

    public RegOp(RegType type, Var var, String seuil) {
        this(type,null,null,null,var,seuil);
    }

    public RegOp(RegType type, String operateur, RegOp expr1, RegOp expr2, Var var, String seuil) {
        this.type = type;
        this.operateur = operateur;
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.var = var;
        this.seuil = seuil;
    }

    public Formula toFormula() {
        Formula formula = null;
        if(type == RegType.ATOME){
            formula = new GreatEq(var.toBack(), new Int(Integer.parseInt(seuil)));
        }else{
            switch(operateur){
                case "&": {
                    formula = new And(expr1.toFormula(), expr2.toFormula());
                }
                break;
                case "|": {
                    formula = new Or(expr1.toFormula(), expr2.toFormula());
                }
                break;
                case "!": {
                    formula = new Not(expr1.toFormula());
                }
                break;
                default : {
                    System.err.println("RegOp to Formula conversion : Unknown operator");
                    break;
                }
            }
        }
        return formula;
    }

    public void setType(RegType type) {
        this.type = type;
    }

    public void setOperateur(String operateur) {
        this.operateur = operateur;
    }

    public void setExpr1(RegOp expr1) {
        this.expr1 = expr1;
    }

    public void setExpr2(RegOp expr2) {
        this.expr2 = expr2;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public void setSeuil(String seuil) {
        this.seuil = seuil;
    }

    public RegType getType() {
        return type;
    }

    public String getOperateur() {
        return operateur;
    }

    public RegOp getExpr1() {
        return expr1;
    }

    public RegOp getExpr2() {
        return expr2;
    }

    public Var getVar() {
        return var;
    }

    public String getSeuil() {
        return seuil;
    }
}