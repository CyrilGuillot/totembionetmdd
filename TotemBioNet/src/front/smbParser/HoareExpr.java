package front.smbParser;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class HoareExpr {
    private String id;
    private String operation;
    private Integer constant;

    public HoareExpr(String id, String operation, Integer constant) {
        this.id = id;
        this.operation = operation;
        this.constant = constant;
    }

    public HoareExpr(String id, String operation) {
        this.id = id;
        this.operation = operation;
    }

    public String getId() {
        return id;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getConstant() {
        return constant;
    }
}