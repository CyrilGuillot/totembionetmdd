package front.smbParser;

import back.net.Reg;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class RegBlock {

    // TODO Create Regul instances from visitors

    private List<Regul> regs;
    private Set<String> targets;

    public RegBlock() {
        this.regs = new ArrayList<Regul>();
        this.targets = new HashSet<String>();
    }

    public List<Regul> getRegs() {
        return regs;
    }

    public Regul getRegWithId(String id) {
        for(Regul reg : regs){
            if(reg.getId().equals(id)){
                return reg;
            }
        }
        return null;
    }

    public boolean existsReg(String regId){
        return regs.stream().filter(reg -> reg.getId().equals(regId)).count()>0;
    }

    public void addReg(String id, List<String> targets, String formulaString, RegOp operation) {
        //TODO - Exception if a regulation already exists ??
        this.targets.addAll(targets);
        this.regs.add(new Regul(id, targets, formulaString, operation));
    }

    public String toJSON(){
        String regBlock = "{\"blocktype\" : \"reg\",\n \"value\" : [";

        //Pour chaque target, filtrer les reg ayant ce target et construire le block
        for(String target: targets){
            regBlock += "{\"cible\" : \"" + target + "\",\n \"regs\" : [";
            List<Regul> targetRegS = regs.stream().filter(reg -> reg.getTargets().contains(target)).collect(Collectors.toList());
            for(Regul reg: targetRegS){
                regBlock += "{ \"id\": \"" + reg.getId() +
                            "\",\n \"formulaString\" : \"" + reg.getFormulaString() +
                            "\",\n \"formulaTree\" : ";
                regBlock += formulaTreeJSON(reg.getFormulaTree());
                regBlock += "},";
            }
            regBlock = regBlock.substring(0, regBlock.length() - 1);
            regBlock += "]},";
        }
        regBlock = regBlock.substring(0, regBlock.length() - 1);
        regBlock += "]}";
        return regBlock;
    }

    public String formulaTreeJSON (RegOp root) {
        String formula = "{";
        if(root.getType() == RegType.ATOME) {
            formula +=  "\"type\" : \"ATOME\"" +
                        ",\"var\" : \"" + root.getVar().getId() +
                        "\",\n \"seuil\" : " + root.getSeuil();
        } else {
            boolean isExpr2 = root.getExpr2() != null;
            formula += "\"type\" : \"EXPR\"" +
                        ",\"operateur\" : \"" + root.getOperateur() +
                        "\",\"expr1\" : " + formulaTreeJSON(root.getExpr1()) +
                        ",\"expr2\" : " + ((isExpr2) ?  formulaTreeJSON(root.getExpr2()) : "\"null\"");
        }
        formula += "}";
        return formula;
    }

    public List<Reg> getBackFormat() throws Exception {
        List<Reg> regList = new ArrayList<>();
        for(Regul regul : this.regs){
            regList.add(regul.toBack());
        }
        return regList;
    }

}