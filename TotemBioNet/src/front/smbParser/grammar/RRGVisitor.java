// Generated from TotemBioNet/src/front/smbParser/grammar/RRG.g4 by ANTLR 4.7.1
package front.smbParser.grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link RRGParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface RRGVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link RRGParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(RRGParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#env_var_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnv_var_block(RRGParser.Env_var_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#env_var_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnv_var_decl(RRGParser.Env_var_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#var_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_block(RRGParser.Var_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#var_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_decl(RRGParser.Var_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#reg_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReg_block(RRGParser.Reg_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#reg_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReg_decl(RRGParser.Reg_declContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_neg}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_neg(RRGParser.Expr_negContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_bool_op}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_bool_op(RRGParser.Expr_bool_opContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_brackets}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_brackets(RRGParser.Expr_bracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_atome}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_atome(RRGParser.Expr_atomeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expr_mux_name}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_mux_name(RRGParser.Expr_mux_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#para_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPara_block(RRGParser.Para_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#para_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPara_decl(RRGParser.Para_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#ctl_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_block(RRGParser.Ctl_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#ctl_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_decl(RRGParser.Ctl_declContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_temp_op1}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_temp_op1(RRGParser.Ctl_temp_op1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_bool_op}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_bool_op(RRGParser.Ctl_bool_opContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_temp_op2}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_temp_op2(RRGParser.Ctl_temp_op2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_brackets}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_brackets(RRGParser.Ctl_bracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_neg_op}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_neg_op(RRGParser.Ctl_neg_opContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_atome}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_atome(RRGParser.Ctl_atomeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ctl_impl}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCtl_impl(RRGParser.Ctl_implContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#hoare_block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_block(RRGParser.Hoare_blockContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#hoare_pre_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_pre_decl(RRGParser.Hoare_pre_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#hoare_post_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_post_decl(RRGParser.Hoare_post_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link RRGParser#hoare_trace_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_decl(RRGParser.Hoare_trace_declContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_affect}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_affect(RRGParser.Hoare_trace_affectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_decr}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_decr(RRGParser.Hoare_trace_decrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_brackets}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_brackets(RRGParser.Hoare_trace_bracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_while}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_while(RRGParser.Hoare_trace_whileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_forall}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_forall(RRGParser.Hoare_trace_forallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_seq}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_seq(RRGParser.Hoare_trace_seqContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_skip}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_skip(RRGParser.Hoare_trace_skipContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_exists}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_exists(RRGParser.Hoare_trace_existsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_incr}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_incr(RRGParser.Hoare_trace_incrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_if}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_if(RRGParser.Hoare_trace_ifContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_trace_assert}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_trace_assert(RRGParser.Hoare_trace_assertContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_term_simple}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_term_simple(RRGParser.Hoare_term_simpleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_term_oper}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_term_oper(RRGParser.Hoare_term_operContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_term_brackets}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_term_brackets(RRGParser.Hoare_term_bracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_assert_bool}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_assert_bool(RRGParser.Hoare_assert_boolContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_assert_comp}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_assert_comp(RRGParser.Hoare_assert_compContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_assert_brackets}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_assert_brackets(RRGParser.Hoare_assert_bracketsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hoare_assert_neg}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHoare_assert_neg(RRGParser.Hoare_assert_negContext ctx);
}