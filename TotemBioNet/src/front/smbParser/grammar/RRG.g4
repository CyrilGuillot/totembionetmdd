//                                      -*- mode: antlr -*-
//
// Grammaire des fichiers de description de la régulation des réseaux de genes
//
//           Author: Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
//                   Eric Galesio [eg@unice.fr]
//                   Hélène Collavizza [helen@unice.fr]
//    Creation date: 19-Jun-2019
// Last file update: 20-Jun-2019 (nss)
// Last file update: 19-Nov-2019 (helen)

grammar RRG;


prog          : env_var_block?
                var_block
                reg_block
                para_block?
                hoare_block?
                ctl_block*
                'END'
              ;

/****** Description du graphe : variables et régulations ********/

env_var_block : KENV_VAR env_var_decl+
              ;
env_var_decl  : ID EQ NUM SEMI
              ;

var_block     : KVAR var_decl+
              ;
var_decl      : ID EQ NUM '..' NUM NS? SEMI
              ;

reg_block     : KREG reg_decl+
              ;
reg_decl      : ID '['reg_expr']' CIBLE ID+ SEMI
              ;
reg_expr      : NEG reg_expr                        # expr_neg
              | reg_expr BOOL_OP reg_expr           # expr_bool_op
              | '('reg_expr')'                      # expr_brackets
              | ID SEUIL NUM                        # expr_atome
              | ID                                  # expr_mux_name
              ;

/****** Block PARA pour fixer les paramètres ********/

para_block    : KPARA para_decl*
              ;
para_decl     : KID '=' NUM ('..' NUM)? SEMI
              ;
/****** Block des propriétés CTL ********/

ctl_block     : KCTL ctl_decl+
              ;
ctl_decl      : (ID '=')? ctl SEMI
              ;
ctl           : NEG ctl                             # ctl_neg_op
              | ctl IMPL ctl                        # ctl_impl
              | ctl BOOL_OP ctl                     # ctl_bool_op
              | '(' ctl ')'                         # ctl_brackets
              | CTL_PREFIX_1 '(' ctl ')'            # ctl_temp_op1
              | CTL_PREFIX_2 '(' ctl 'U' ctl')'     # ctl_temp_op2
              | ID (COMP|EQ|SEUIL) NUM              # ctl_atome

/** re-use of id is too much confusing without a declaration block for CTL formulas**/
/**              | ID                                  # ctl_name **/
//*** the semantics of OSCIL and STABLE needs to be discussed (to be add in FAIR part) ***/
/**              | 'OSCIL' '(' ID ',' NUM ',' NUM ')'  # ctl_oscil
              | 'STABLE' '(' ID ',' NUM  ')'        # ctl_stable  **/

              ;
/****** Langage d'entrée de Hoare ********/

hoare_block         : KHOARE
                        hoare_pre_decl
                        hoare_trace_decl
                        hoare_post_decl
                    ;

hoare_pre_decl      : KPRE ':' '{' (ID EQ NUM (',')? )* '}'
                    ;

hoare_post_decl     : KPOST ':' '{' (hoare_assert (',')? )* '}'
                    ;

hoare_trace_decl    : KTRACE ':' hoare_trace SEMI
                    ;

hoare_trace         : 'Skip'                                                            #hoare_trace_skip
                    | ID ':=' NUM			                                            #hoare_trace_affect           // := affectation variable
                    | ID '+'                                                            #hoare_trace_incr             // + incrémentation variable
                    | ID '-'                                                            #hoare_trace_decr             // + incrémentation variable
                    | hoare_trace SEMI hoare_trace                                      #hoare_trace_seq              //
                    | 'If' hoare_assert 'Then' hoare_trace ('Else' hoare_trace)?        #hoare_trace_if               // conditionnelle
                    | 'While' hoare_assert 'With' hoare_assert 'Do' hoare_trace         #hoare_trace_while            // boucle avec condition et invariant
                    | 'Forall' '(' hoare_trace  (',' hoare_trace)* ')'                  #hoare_trace_forall           // tous les programmes dans une liste délimitée par () éléments séparés par ,
                    | 'Exists' '(' hoare_trace (',' hoare_trace)* ')'                   #hoare_trace_exists           // existence d'un programme parmi une liste délimitée par () éléments séparés par ,
                    | 'Assert' '(' hoare_assert ')'                                     #hoare_trace_assert           // assertion
                    | '(' hoare_trace ')'                                               #hoare_trace_brackets         // brackets
                    ;
hoare_term          : (NUM|ID|KID)                                                      #hoare_term_simple
                    | hoare_term OPER hoare_term                                        #hoare_term_oper
                    | '(' hoare_term ')'                                                #hoare_term_brackets
                    ;
hoare_assert        : hoare_term (EQ|COMP|SEUIL) hoare_term                             #hoare_assert_comp
                    | '(' hoare_assert ')'                                              #hoare_assert_brackets
                    | hoare_assert (BOOL_OP|IMPL) hoare_assert                          #hoare_assert_bool
                    | NEG hoare_assert                                                  #hoare_assert_neg
                    ;

/****** Rajouter une regle de grammaire ici ********/

/** Lexical Analysis - Lexer rules **/

KENV_VAR    : 'ENV_VAR';
KVAR        : 'VAR';
KREG        : 'REG';
KPARA       : 'PARA';
KCTL        : ('CTL'|'FAIRCTL');
KHOARE      : 'HOARE';
KPRE        : 'PRE';
KTRACE      : 'TRACE';
KPOST       : 'POST';

CTL_PREFIX_1: ('EX'|'AX'|'EF'|'AF'|'EG'|'AG');
CTL_PREFIX_2: ('E'|'A');
KID         : 'K_'[a-zA-Z][a-zA-Z0-9_]*(':'[a-zA-Z][a-zA-Z0-9_]*)*;
ID          : [a-zA-Z][a-zA-Z0-9_]*;
NUM         : [0-9];
NS          : '(NS)'; /** no Snoussi **/

IMPL        : '->';
CIBLE       : '=>';
COMP        : ('<='|'>'|'<');
EQ          : '=';
SEUIL       : '>=';
SEMI        : ';';
NEG         : '!';
BOOL_OP     : ('&'|'|');
OPER        : ('+'|'-');

WS          : [ \r\n\t]+      -> skip;
COMMENT     : '#' ~('\n')*    -> skip;
