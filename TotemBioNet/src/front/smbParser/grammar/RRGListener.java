// Generated from TotemBioNet/src/front/smbParser/grammar/RRG.g4 by ANTLR 4.7.1
package front.smbParser.grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RRGParser}.
 */
public interface RRGListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RRGParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(RRGParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(RRGParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#env_var_block}.
	 * @param ctx the parse tree
	 */
	void enterEnv_var_block(RRGParser.Env_var_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#env_var_block}.
	 * @param ctx the parse tree
	 */
	void exitEnv_var_block(RRGParser.Env_var_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#env_var_decl}.
	 * @param ctx the parse tree
	 */
	void enterEnv_var_decl(RRGParser.Env_var_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#env_var_decl}.
	 * @param ctx the parse tree
	 */
	void exitEnv_var_decl(RRGParser.Env_var_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#var_block}.
	 * @param ctx the parse tree
	 */
	void enterVar_block(RRGParser.Var_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#var_block}.
	 * @param ctx the parse tree
	 */
	void exitVar_block(RRGParser.Var_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void enterVar_decl(RRGParser.Var_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#var_decl}.
	 * @param ctx the parse tree
	 */
	void exitVar_decl(RRGParser.Var_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#reg_block}.
	 * @param ctx the parse tree
	 */
	void enterReg_block(RRGParser.Reg_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#reg_block}.
	 * @param ctx the parse tree
	 */
	void exitReg_block(RRGParser.Reg_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#reg_decl}.
	 * @param ctx the parse tree
	 */
	void enterReg_decl(RRGParser.Reg_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#reg_decl}.
	 * @param ctx the parse tree
	 */
	void exitReg_decl(RRGParser.Reg_declContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_neg}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_neg(RRGParser.Expr_negContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_neg}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_neg(RRGParser.Expr_negContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_bool_op}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_bool_op(RRGParser.Expr_bool_opContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_bool_op}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_bool_op(RRGParser.Expr_bool_opContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_brackets}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_brackets(RRGParser.Expr_bracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_brackets}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_brackets(RRGParser.Expr_bracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_atome}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_atome(RRGParser.Expr_atomeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_atome}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_atome(RRGParser.Expr_atomeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expr_mux_name}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_mux_name(RRGParser.Expr_mux_nameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expr_mux_name}
	 * labeled alternative in {@link RRGParser#reg_expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_mux_name(RRGParser.Expr_mux_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#para_block}.
	 * @param ctx the parse tree
	 */
	void enterPara_block(RRGParser.Para_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#para_block}.
	 * @param ctx the parse tree
	 */
	void exitPara_block(RRGParser.Para_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#para_decl}.
	 * @param ctx the parse tree
	 */
	void enterPara_decl(RRGParser.Para_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#para_decl}.
	 * @param ctx the parse tree
	 */
	void exitPara_decl(RRGParser.Para_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#ctl_block}.
	 * @param ctx the parse tree
	 */
	void enterCtl_block(RRGParser.Ctl_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#ctl_block}.
	 * @param ctx the parse tree
	 */
	void exitCtl_block(RRGParser.Ctl_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#ctl_decl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_decl(RRGParser.Ctl_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#ctl_decl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_decl(RRGParser.Ctl_declContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_temp_op1}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_temp_op1(RRGParser.Ctl_temp_op1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_temp_op1}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_temp_op1(RRGParser.Ctl_temp_op1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_bool_op}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_bool_op(RRGParser.Ctl_bool_opContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_bool_op}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_bool_op(RRGParser.Ctl_bool_opContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_temp_op2}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_temp_op2(RRGParser.Ctl_temp_op2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_temp_op2}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_temp_op2(RRGParser.Ctl_temp_op2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_brackets}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_brackets(RRGParser.Ctl_bracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_brackets}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_brackets(RRGParser.Ctl_bracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_neg_op}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_neg_op(RRGParser.Ctl_neg_opContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_neg_op}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_neg_op(RRGParser.Ctl_neg_opContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_atome}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_atome(RRGParser.Ctl_atomeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_atome}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_atome(RRGParser.Ctl_atomeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ctl_impl}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void enterCtl_impl(RRGParser.Ctl_implContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ctl_impl}
	 * labeled alternative in {@link RRGParser#ctl}.
	 * @param ctx the parse tree
	 */
	void exitCtl_impl(RRGParser.Ctl_implContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#hoare_block}.
	 * @param ctx the parse tree
	 */
	void enterHoare_block(RRGParser.Hoare_blockContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#hoare_block}.
	 * @param ctx the parse tree
	 */
	void exitHoare_block(RRGParser.Hoare_blockContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#hoare_pre_decl}.
	 * @param ctx the parse tree
	 */
	void enterHoare_pre_decl(RRGParser.Hoare_pre_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#hoare_pre_decl}.
	 * @param ctx the parse tree
	 */
	void exitHoare_pre_decl(RRGParser.Hoare_pre_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#hoare_post_decl}.
	 * @param ctx the parse tree
	 */
	void enterHoare_post_decl(RRGParser.Hoare_post_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#hoare_post_decl}.
	 * @param ctx the parse tree
	 */
	void exitHoare_post_decl(RRGParser.Hoare_post_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link RRGParser#hoare_trace_decl}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_decl(RRGParser.Hoare_trace_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link RRGParser#hoare_trace_decl}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_decl(RRGParser.Hoare_trace_declContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_affect}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_affect(RRGParser.Hoare_trace_affectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_affect}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_affect(RRGParser.Hoare_trace_affectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_decr}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_decr(RRGParser.Hoare_trace_decrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_decr}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_decr(RRGParser.Hoare_trace_decrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_brackets}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_brackets(RRGParser.Hoare_trace_bracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_brackets}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_brackets(RRGParser.Hoare_trace_bracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_while}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_while(RRGParser.Hoare_trace_whileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_while}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_while(RRGParser.Hoare_trace_whileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_forall}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_forall(RRGParser.Hoare_trace_forallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_forall}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_forall(RRGParser.Hoare_trace_forallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_seq}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_seq(RRGParser.Hoare_trace_seqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_seq}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_seq(RRGParser.Hoare_trace_seqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_skip}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_skip(RRGParser.Hoare_trace_skipContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_skip}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_skip(RRGParser.Hoare_trace_skipContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_exists}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_exists(RRGParser.Hoare_trace_existsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_exists}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_exists(RRGParser.Hoare_trace_existsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_incr}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_incr(RRGParser.Hoare_trace_incrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_incr}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_incr(RRGParser.Hoare_trace_incrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_if}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_if(RRGParser.Hoare_trace_ifContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_if}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_if(RRGParser.Hoare_trace_ifContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_trace_assert}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void enterHoare_trace_assert(RRGParser.Hoare_trace_assertContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_trace_assert}
	 * labeled alternative in {@link RRGParser#hoare_trace}.
	 * @param ctx the parse tree
	 */
	void exitHoare_trace_assert(RRGParser.Hoare_trace_assertContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_term_simple}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 */
	void enterHoare_term_simple(RRGParser.Hoare_term_simpleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_term_simple}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 */
	void exitHoare_term_simple(RRGParser.Hoare_term_simpleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_term_oper}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 */
	void enterHoare_term_oper(RRGParser.Hoare_term_operContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_term_oper}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 */
	void exitHoare_term_oper(RRGParser.Hoare_term_operContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_term_brackets}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 */
	void enterHoare_term_brackets(RRGParser.Hoare_term_bracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_term_brackets}
	 * labeled alternative in {@link RRGParser#hoare_term}.
	 * @param ctx the parse tree
	 */
	void exitHoare_term_brackets(RRGParser.Hoare_term_bracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_assert_bool}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void enterHoare_assert_bool(RRGParser.Hoare_assert_boolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_assert_bool}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void exitHoare_assert_bool(RRGParser.Hoare_assert_boolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_assert_comp}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void enterHoare_assert_comp(RRGParser.Hoare_assert_compContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_assert_comp}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void exitHoare_assert_comp(RRGParser.Hoare_assert_compContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_assert_brackets}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void enterHoare_assert_brackets(RRGParser.Hoare_assert_bracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_assert_brackets}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void exitHoare_assert_brackets(RRGParser.Hoare_assert_bracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hoare_assert_neg}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void enterHoare_assert_neg(RRGParser.Hoare_assert_negContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hoare_assert_neg}
	 * labeled alternative in {@link RRGParser#hoare_assert}.
	 * @param ctx the parse tree
	 */
	void exitHoare_assert_neg(RRGParser.Hoare_assert_negContext ctx);
}