// Generated from TotemBioNet/src/front/smbParser/grammar/RRG.g4 by ANTLR 4.7.1
package front.smbParser.grammar;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RRGParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		KENV_VAR=25, KVAR=26, KREG=27, KPARA=28, KCTL=29, KHOARE=30, KPRE=31, 
		KTRACE=32, KPOST=33, CTL_PREFIX_1=34, CTL_PREFIX_2=35, KID=36, ID=37, 
		NUM=38, NS=39, IMPL=40, CIBLE=41, COMP=42, EQ=43, SEUIL=44, SEMI=45, NEG=46, 
		BOOL_OP=47, OPER=48, WS=49, COMMENT=50;
	public static final int
		RULE_prog = 0, RULE_env_var_block = 1, RULE_env_var_decl = 2, RULE_var_block = 3, 
		RULE_var_decl = 4, RULE_reg_block = 5, RULE_reg_decl = 6, RULE_reg_expr = 7, 
		RULE_para_block = 8, RULE_para_decl = 9, RULE_ctl_block = 10, RULE_ctl_decl = 11, 
		RULE_ctl = 12, RULE_hoare_block = 13, RULE_hoare_pre_decl = 14, RULE_hoare_post_decl = 15, 
		RULE_hoare_trace_decl = 16, RULE_hoare_trace = 17, RULE_hoare_term = 18, 
		RULE_hoare_assert = 19;
	public static final String[] ruleNames = {
		"prog", "env_var_block", "env_var_decl", "var_block", "var_decl", "reg_block", 
		"reg_decl", "reg_expr", "para_block", "para_decl", "ctl_block", "ctl_decl", 
		"ctl", "hoare_block", "hoare_pre_decl", "hoare_post_decl", "hoare_trace_decl", 
		"hoare_trace", "hoare_term", "hoare_assert"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'END'", "'..'", "'['", "']'", "'('", "')'", "'U'", "':'", "'{'", 
		"','", "'}'", "'Skip'", "':='", "'+'", "'-'", "'If'", "'Then'", "'Else'", 
		"'While'", "'With'", "'Do'", "'Forall'", "'Exists'", "'Assert'", "'ENV_VAR'", 
		"'VAR'", "'REG'", "'PARA'", null, "'HOARE'", "'PRE'", "'TRACE'", "'POST'", 
		null, null, null, null, null, "'(NS)'", "'->'", "'=>'", null, "'='", "'>='", 
		"';'", "'!'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, "KENV_VAR", "KVAR", "KREG", "KPARA", "KCTL", "KHOARE", "KPRE", "KTRACE", 
		"KPOST", "CTL_PREFIX_1", "CTL_PREFIX_2", "KID", "ID", "NUM", "NS", "IMPL", 
		"CIBLE", "COMP", "EQ", "SEUIL", "SEMI", "NEG", "BOOL_OP", "OPER", "WS", 
		"COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "RRG.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RRGParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public Var_blockContext var_block() {
			return getRuleContext(Var_blockContext.class,0);
		}
		public Reg_blockContext reg_block() {
			return getRuleContext(Reg_blockContext.class,0);
		}
		public Env_var_blockContext env_var_block() {
			return getRuleContext(Env_var_blockContext.class,0);
		}
		public Para_blockContext para_block() {
			return getRuleContext(Para_blockContext.class,0);
		}
		public Hoare_blockContext hoare_block() {
			return getRuleContext(Hoare_blockContext.class,0);
		}
		public List<Ctl_blockContext> ctl_block() {
			return getRuleContexts(Ctl_blockContext.class);
		}
		public Ctl_blockContext ctl_block(int i) {
			return getRuleContext(Ctl_blockContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitProg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitProg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KENV_VAR) {
				{
				setState(40);
				env_var_block();
				}
			}

			setState(43);
			var_block();
			setState(44);
			reg_block();
			setState(46);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KPARA) {
				{
				setState(45);
				para_block();
				}
			}

			setState(49);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==KHOARE) {
				{
				setState(48);
				hoare_block();
				}
			}

			setState(54);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==KCTL) {
				{
				{
				setState(51);
				ctl_block();
				}
				}
				setState(56);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(57);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Env_var_blockContext extends ParserRuleContext {
		public TerminalNode KENV_VAR() { return getToken(RRGParser.KENV_VAR, 0); }
		public List<Env_var_declContext> env_var_decl() {
			return getRuleContexts(Env_var_declContext.class);
		}
		public Env_var_declContext env_var_decl(int i) {
			return getRuleContext(Env_var_declContext.class,i);
		}
		public Env_var_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_env_var_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterEnv_var_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitEnv_var_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitEnv_var_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Env_var_blockContext env_var_block() throws RecognitionException {
		Env_var_blockContext _localctx = new Env_var_blockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_env_var_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			match(KENV_VAR);
			setState(61); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(60);
				env_var_decl();
				}
				}
				setState(63); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Env_var_declContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public TerminalNode EQ() { return getToken(RRGParser.EQ, 0); }
		public TerminalNode NUM() { return getToken(RRGParser.NUM, 0); }
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public Env_var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_env_var_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterEnv_var_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitEnv_var_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitEnv_var_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Env_var_declContext env_var_decl() throws RecognitionException {
		Env_var_declContext _localctx = new Env_var_declContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_env_var_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65);
			match(ID);
			setState(66);
			match(EQ);
			setState(67);
			match(NUM);
			setState(68);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_blockContext extends ParserRuleContext {
		public TerminalNode KVAR() { return getToken(RRGParser.KVAR, 0); }
		public List<Var_declContext> var_decl() {
			return getRuleContexts(Var_declContext.class);
		}
		public Var_declContext var_decl(int i) {
			return getRuleContext(Var_declContext.class,i);
		}
		public Var_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterVar_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitVar_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitVar_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_blockContext var_block() throws RecognitionException {
		Var_blockContext _localctx = new Var_blockContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_var_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			match(KVAR);
			setState(72); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(71);
				var_decl();
				}
				}
				setState(74); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_declContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public TerminalNode EQ() { return getToken(RRGParser.EQ, 0); }
		public List<TerminalNode> NUM() { return getTokens(RRGParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(RRGParser.NUM, i);
		}
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public TerminalNode NS() { return getToken(RRGParser.NS, 0); }
		public Var_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterVar_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitVar_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitVar_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_declContext var_decl() throws RecognitionException {
		Var_declContext _localctx = new Var_declContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_var_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(ID);
			setState(77);
			match(EQ);
			setState(78);
			match(NUM);
			setState(79);
			match(T__1);
			setState(80);
			match(NUM);
			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NS) {
				{
				setState(81);
				match(NS);
				}
			}

			setState(84);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Reg_blockContext extends ParserRuleContext {
		public TerminalNode KREG() { return getToken(RRGParser.KREG, 0); }
		public List<Reg_declContext> reg_decl() {
			return getRuleContexts(Reg_declContext.class);
		}
		public Reg_declContext reg_decl(int i) {
			return getRuleContext(Reg_declContext.class,i);
		}
		public Reg_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reg_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterReg_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitReg_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitReg_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Reg_blockContext reg_block() throws RecognitionException {
		Reg_blockContext _localctx = new Reg_blockContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_reg_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(86);
			match(KREG);
			setState(88); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(87);
				reg_decl();
				}
				}
				setState(90); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Reg_declContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(RRGParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(RRGParser.ID, i);
		}
		public Reg_exprContext reg_expr() {
			return getRuleContext(Reg_exprContext.class,0);
		}
		public TerminalNode CIBLE() { return getToken(RRGParser.CIBLE, 0); }
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public Reg_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reg_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterReg_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitReg_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitReg_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Reg_declContext reg_decl() throws RecognitionException {
		Reg_declContext _localctx = new Reg_declContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_reg_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			match(ID);
			setState(93);
			match(T__2);
			setState(94);
			reg_expr(0);
			setState(95);
			match(T__3);
			setState(96);
			match(CIBLE);
			setState(98); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(97);
				match(ID);
				}
				}
				setState(100); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==ID );
			setState(102);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Reg_exprContext extends ParserRuleContext {
		public Reg_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reg_expr; }
	 
		public Reg_exprContext() { }
		public void copyFrom(Reg_exprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Expr_negContext extends Reg_exprContext {
		public TerminalNode NEG() { return getToken(RRGParser.NEG, 0); }
		public Reg_exprContext reg_expr() {
			return getRuleContext(Reg_exprContext.class,0);
		}
		public Expr_negContext(Reg_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterExpr_neg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitExpr_neg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitExpr_neg(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_bool_opContext extends Reg_exprContext {
		public List<Reg_exprContext> reg_expr() {
			return getRuleContexts(Reg_exprContext.class);
		}
		public Reg_exprContext reg_expr(int i) {
			return getRuleContext(Reg_exprContext.class,i);
		}
		public TerminalNode BOOL_OP() { return getToken(RRGParser.BOOL_OP, 0); }
		public Expr_bool_opContext(Reg_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterExpr_bool_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitExpr_bool_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitExpr_bool_op(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_bracketsContext extends Reg_exprContext {
		public Reg_exprContext reg_expr() {
			return getRuleContext(Reg_exprContext.class,0);
		}
		public Expr_bracketsContext(Reg_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterExpr_brackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitExpr_brackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitExpr_brackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_atomeContext extends Reg_exprContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public TerminalNode SEUIL() { return getToken(RRGParser.SEUIL, 0); }
		public TerminalNode NUM() { return getToken(RRGParser.NUM, 0); }
		public Expr_atomeContext(Reg_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterExpr_atome(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitExpr_atome(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitExpr_atome(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expr_mux_nameContext extends Reg_exprContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public Expr_mux_nameContext(Reg_exprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterExpr_mux_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitExpr_mux_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitExpr_mux_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Reg_exprContext reg_expr() throws RecognitionException {
		return reg_expr(0);
	}

	private Reg_exprContext reg_expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Reg_exprContext _localctx = new Reg_exprContext(_ctx, _parentState);
		Reg_exprContext _prevctx = _localctx;
		int _startState = 14;
		enterRecursionRule(_localctx, 14, RULE_reg_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				_localctx = new Expr_negContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(105);
				match(NEG);
				setState(106);
				reg_expr(5);
				}
				break;
			case 2:
				{
				_localctx = new Expr_bracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(107);
				match(T__4);
				setState(108);
				reg_expr(0);
				setState(109);
				match(T__5);
				}
				break;
			case 3:
				{
				_localctx = new Expr_atomeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(111);
				match(ID);
				setState(112);
				match(SEUIL);
				setState(113);
				match(NUM);
				}
				break;
			case 4:
				{
				_localctx = new Expr_mux_nameContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(114);
				match(ID);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(122);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Expr_bool_opContext(new Reg_exprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_reg_expr);
					setState(117);
					if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
					setState(118);
					match(BOOL_OP);
					setState(119);
					reg_expr(5);
					}
					} 
				}
				setState(124);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Para_blockContext extends ParserRuleContext {
		public TerminalNode KPARA() { return getToken(RRGParser.KPARA, 0); }
		public List<Para_declContext> para_decl() {
			return getRuleContexts(Para_declContext.class);
		}
		public Para_declContext para_decl(int i) {
			return getRuleContext(Para_declContext.class,i);
		}
		public Para_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_para_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterPara_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitPara_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitPara_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Para_blockContext para_block() throws RecognitionException {
		Para_blockContext _localctx = new Para_blockContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_para_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(KPARA);
			setState(129);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==KID) {
				{
				{
				setState(126);
				para_decl();
				}
				}
				setState(131);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Para_declContext extends ParserRuleContext {
		public TerminalNode KID() { return getToken(RRGParser.KID, 0); }
		public List<TerminalNode> NUM() { return getTokens(RRGParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(RRGParser.NUM, i);
		}
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public Para_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_para_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterPara_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitPara_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitPara_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Para_declContext para_decl() throws RecognitionException {
		Para_declContext _localctx = new Para_declContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_para_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132);
			match(KID);
			setState(133);
			match(EQ);
			setState(134);
			match(NUM);
			setState(137);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(135);
				match(T__1);
				setState(136);
				match(NUM);
				}
			}

			setState(139);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ctl_blockContext extends ParserRuleContext {
		public TerminalNode KCTL() { return getToken(RRGParser.KCTL, 0); }
		public List<Ctl_declContext> ctl_decl() {
			return getRuleContexts(Ctl_declContext.class);
		}
		public Ctl_declContext ctl_decl(int i) {
			return getRuleContext(Ctl_declContext.class,i);
		}
		public Ctl_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ctl_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ctl_blockContext ctl_block() throws RecognitionException {
		Ctl_blockContext _localctx = new Ctl_blockContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_ctl_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			match(KCTL);
			setState(143); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(142);
				ctl_decl();
				}
				}
				setState(145); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << CTL_PREFIX_1) | (1L << CTL_PREFIX_2) | (1L << ID) | (1L << NEG))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ctl_declContext extends ParserRuleContext {
		public CtlContext ctl() {
			return getRuleContext(CtlContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public Ctl_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ctl_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ctl_declContext ctl_decl() throws RecognitionException {
		Ctl_declContext _localctx = new Ctl_declContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_ctl_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(147);
				match(ID);
				setState(148);
				match(EQ);
				}
				break;
			}
			setState(151);
			ctl(0);
			setState(152);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CtlContext extends ParserRuleContext {
		public CtlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ctl; }
	 
		public CtlContext() { }
		public void copyFrom(CtlContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Ctl_temp_op1Context extends CtlContext {
		public TerminalNode CTL_PREFIX_1() { return getToken(RRGParser.CTL_PREFIX_1, 0); }
		public CtlContext ctl() {
			return getRuleContext(CtlContext.class,0);
		}
		public Ctl_temp_op1Context(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_temp_op1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_temp_op1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_temp_op1(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ctl_bool_opContext extends CtlContext {
		public List<CtlContext> ctl() {
			return getRuleContexts(CtlContext.class);
		}
		public CtlContext ctl(int i) {
			return getRuleContext(CtlContext.class,i);
		}
		public TerminalNode BOOL_OP() { return getToken(RRGParser.BOOL_OP, 0); }
		public Ctl_bool_opContext(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_bool_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_bool_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_bool_op(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ctl_temp_op2Context extends CtlContext {
		public TerminalNode CTL_PREFIX_2() { return getToken(RRGParser.CTL_PREFIX_2, 0); }
		public List<CtlContext> ctl() {
			return getRuleContexts(CtlContext.class);
		}
		public CtlContext ctl(int i) {
			return getRuleContext(CtlContext.class,i);
		}
		public Ctl_temp_op2Context(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_temp_op2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_temp_op2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_temp_op2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ctl_bracketsContext extends CtlContext {
		public CtlContext ctl() {
			return getRuleContext(CtlContext.class,0);
		}
		public Ctl_bracketsContext(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_brackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_brackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_brackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ctl_neg_opContext extends CtlContext {
		public TerminalNode NEG() { return getToken(RRGParser.NEG, 0); }
		public CtlContext ctl() {
			return getRuleContext(CtlContext.class,0);
		}
		public Ctl_neg_opContext(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_neg_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_neg_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_neg_op(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ctl_atomeContext extends CtlContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public TerminalNode NUM() { return getToken(RRGParser.NUM, 0); }
		public TerminalNode COMP() { return getToken(RRGParser.COMP, 0); }
		public TerminalNode EQ() { return getToken(RRGParser.EQ, 0); }
		public TerminalNode SEUIL() { return getToken(RRGParser.SEUIL, 0); }
		public Ctl_atomeContext(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_atome(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_atome(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_atome(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Ctl_implContext extends CtlContext {
		public List<CtlContext> ctl() {
			return getRuleContexts(CtlContext.class);
		}
		public CtlContext ctl(int i) {
			return getRuleContext(CtlContext.class,i);
		}
		public TerminalNode IMPL() { return getToken(RRGParser.IMPL, 0); }
		public Ctl_implContext(CtlContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterCtl_impl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitCtl_impl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitCtl_impl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CtlContext ctl() throws RecognitionException {
		return ctl(0);
	}

	private CtlContext ctl(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		CtlContext _localctx = new CtlContext(_ctx, _parentState);
		CtlContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_ctl, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NEG:
				{
				_localctx = new Ctl_neg_opContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(155);
				match(NEG);
				setState(156);
				ctl(7);
				}
				break;
			case T__4:
				{
				_localctx = new Ctl_bracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(157);
				match(T__4);
				setState(158);
				ctl(0);
				setState(159);
				match(T__5);
				}
				break;
			case CTL_PREFIX_1:
				{
				_localctx = new Ctl_temp_op1Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(161);
				match(CTL_PREFIX_1);
				setState(162);
				match(T__4);
				setState(163);
				ctl(0);
				setState(164);
				match(T__5);
				}
				break;
			case CTL_PREFIX_2:
				{
				_localctx = new Ctl_temp_op2Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(166);
				match(CTL_PREFIX_2);
				setState(167);
				match(T__4);
				setState(168);
				ctl(0);
				setState(169);
				match(T__6);
				setState(170);
				ctl(0);
				setState(171);
				match(T__5);
				}
				break;
			case ID:
				{
				_localctx = new Ctl_atomeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(173);
				match(ID);
				setState(174);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << COMP) | (1L << EQ) | (1L << SEUIL))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(175);
				match(NUM);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(186);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(184);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
					case 1:
						{
						_localctx = new Ctl_implContext(new CtlContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ctl);
						setState(178);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(179);
						match(IMPL);
						setState(180);
						ctl(7);
						}
						break;
					case 2:
						{
						_localctx = new Ctl_bool_opContext(new CtlContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_ctl);
						setState(181);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(182);
						match(BOOL_OP);
						setState(183);
						ctl(6);
						}
						break;
					}
					} 
				}
				setState(188);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Hoare_blockContext extends ParserRuleContext {
		public TerminalNode KHOARE() { return getToken(RRGParser.KHOARE, 0); }
		public Hoare_pre_declContext hoare_pre_decl() {
			return getRuleContext(Hoare_pre_declContext.class,0);
		}
		public Hoare_trace_declContext hoare_trace_decl() {
			return getRuleContext(Hoare_trace_declContext.class,0);
		}
		public Hoare_post_declContext hoare_post_decl() {
			return getRuleContext(Hoare_post_declContext.class,0);
		}
		public Hoare_blockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_block(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_block(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_block(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_blockContext hoare_block() throws RecognitionException {
		Hoare_blockContext _localctx = new Hoare_blockContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_hoare_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189);
			match(KHOARE);
			setState(190);
			hoare_pre_decl();
			setState(191);
			hoare_trace_decl();
			setState(192);
			hoare_post_decl();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Hoare_pre_declContext extends ParserRuleContext {
		public TerminalNode KPRE() { return getToken(RRGParser.KPRE, 0); }
		public List<TerminalNode> ID() { return getTokens(RRGParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(RRGParser.ID, i);
		}
		public List<TerminalNode> EQ() { return getTokens(RRGParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(RRGParser.EQ, i);
		}
		public List<TerminalNode> NUM() { return getTokens(RRGParser.NUM); }
		public TerminalNode NUM(int i) {
			return getToken(RRGParser.NUM, i);
		}
		public Hoare_pre_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_pre_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_pre_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_pre_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_pre_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_pre_declContext hoare_pre_decl() throws RecognitionException {
		Hoare_pre_declContext _localctx = new Hoare_pre_declContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_hoare_pre_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			match(KPRE);
			setState(195);
			match(T__7);
			setState(196);
			match(T__8);
			setState(205);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(197);
				match(ID);
				setState(198);
				match(EQ);
				setState(199);
				match(NUM);
				setState(201);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__9) {
					{
					setState(200);
					match(T__9);
					}
				}

				}
				}
				setState(207);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(208);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Hoare_post_declContext extends ParserRuleContext {
		public TerminalNode KPOST() { return getToken(RRGParser.KPOST, 0); }
		public List<Hoare_assertContext> hoare_assert() {
			return getRuleContexts(Hoare_assertContext.class);
		}
		public Hoare_assertContext hoare_assert(int i) {
			return getRuleContext(Hoare_assertContext.class,i);
		}
		public Hoare_post_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_post_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_post_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_post_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_post_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_post_declContext hoare_post_decl() throws RecognitionException {
		Hoare_post_declContext _localctx = new Hoare_post_declContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_hoare_post_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			match(KPOST);
			setState(211);
			match(T__7);
			setState(212);
			match(T__8);
			setState(219);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << KID) | (1L << ID) | (1L << NUM) | (1L << NEG))) != 0)) {
				{
				{
				setState(213);
				hoare_assert(0);
				setState(215);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__9) {
					{
					setState(214);
					match(T__9);
					}
				}

				}
				}
				setState(221);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(222);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Hoare_trace_declContext extends ParserRuleContext {
		public TerminalNode KTRACE() { return getToken(RRGParser.KTRACE, 0); }
		public Hoare_traceContext hoare_trace() {
			return getRuleContext(Hoare_traceContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public Hoare_trace_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_trace_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_trace_declContext hoare_trace_decl() throws RecognitionException {
		Hoare_trace_declContext _localctx = new Hoare_trace_declContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_hoare_trace_decl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(224);
			match(KTRACE);
			setState(225);
			match(T__7);
			setState(226);
			hoare_trace(0);
			setState(227);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Hoare_traceContext extends ParserRuleContext {
		public Hoare_traceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_trace; }
	 
		public Hoare_traceContext() { }
		public void copyFrom(Hoare_traceContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Hoare_trace_affectContext extends Hoare_traceContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public TerminalNode NUM() { return getToken(RRGParser.NUM, 0); }
		public Hoare_trace_affectContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_affect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_affect(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_affect(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_decrContext extends Hoare_traceContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public Hoare_trace_decrContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_decr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_decr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_decr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_bracketsContext extends Hoare_traceContext {
		public Hoare_traceContext hoare_trace() {
			return getRuleContext(Hoare_traceContext.class,0);
		}
		public Hoare_trace_bracketsContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_brackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_brackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_brackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_whileContext extends Hoare_traceContext {
		public List<Hoare_assertContext> hoare_assert() {
			return getRuleContexts(Hoare_assertContext.class);
		}
		public Hoare_assertContext hoare_assert(int i) {
			return getRuleContext(Hoare_assertContext.class,i);
		}
		public Hoare_traceContext hoare_trace() {
			return getRuleContext(Hoare_traceContext.class,0);
		}
		public Hoare_trace_whileContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_while(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_while(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_forallContext extends Hoare_traceContext {
		public List<Hoare_traceContext> hoare_trace() {
			return getRuleContexts(Hoare_traceContext.class);
		}
		public Hoare_traceContext hoare_trace(int i) {
			return getRuleContext(Hoare_traceContext.class,i);
		}
		public Hoare_trace_forallContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_forall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_forall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_forall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_seqContext extends Hoare_traceContext {
		public List<Hoare_traceContext> hoare_trace() {
			return getRuleContexts(Hoare_traceContext.class);
		}
		public Hoare_traceContext hoare_trace(int i) {
			return getRuleContext(Hoare_traceContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(RRGParser.SEMI, 0); }
		public Hoare_trace_seqContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_seq(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_seq(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_seq(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_skipContext extends Hoare_traceContext {
		public Hoare_trace_skipContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_skip(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_skip(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_skip(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_existsContext extends Hoare_traceContext {
		public List<Hoare_traceContext> hoare_trace() {
			return getRuleContexts(Hoare_traceContext.class);
		}
		public Hoare_traceContext hoare_trace(int i) {
			return getRuleContext(Hoare_traceContext.class,i);
		}
		public Hoare_trace_existsContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_exists(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_exists(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_exists(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_incrContext extends Hoare_traceContext {
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public Hoare_trace_incrContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_incr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_incr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_incr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_ifContext extends Hoare_traceContext {
		public Hoare_assertContext hoare_assert() {
			return getRuleContext(Hoare_assertContext.class,0);
		}
		public List<Hoare_traceContext> hoare_trace() {
			return getRuleContexts(Hoare_traceContext.class);
		}
		public Hoare_traceContext hoare_trace(int i) {
			return getRuleContext(Hoare_traceContext.class,i);
		}
		public Hoare_trace_ifContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_if(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_if(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_if(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_trace_assertContext extends Hoare_traceContext {
		public Hoare_assertContext hoare_assert() {
			return getRuleContext(Hoare_assertContext.class,0);
		}
		public Hoare_trace_assertContext(Hoare_traceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_trace_assert(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_trace_assert(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_trace_assert(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_traceContext hoare_trace() throws RecognitionException {
		return hoare_trace(0);
	}

	private Hoare_traceContext hoare_trace(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Hoare_traceContext _localctx = new Hoare_traceContext(_ctx, _parentState);
		Hoare_traceContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_hoare_trace, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(286);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				{
				_localctx = new Hoare_trace_skipContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(230);
				match(T__11);
				}
				break;
			case 2:
				{
				_localctx = new Hoare_trace_affectContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(231);
				match(ID);
				setState(232);
				match(T__12);
				setState(233);
				match(NUM);
				}
				break;
			case 3:
				{
				_localctx = new Hoare_trace_incrContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(234);
				match(ID);
				setState(235);
				match(T__13);
				}
				break;
			case 4:
				{
				_localctx = new Hoare_trace_decrContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(236);
				match(ID);
				setState(237);
				match(T__14);
				}
				break;
			case 5:
				{
				_localctx = new Hoare_trace_ifContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(238);
				match(T__15);
				setState(239);
				hoare_assert(0);
				setState(240);
				match(T__16);
				setState(241);
				hoare_trace(0);
				setState(244);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
				case 1:
					{
					setState(242);
					match(T__17);
					setState(243);
					hoare_trace(0);
					}
					break;
				}
				}
				break;
			case 6:
				{
				_localctx = new Hoare_trace_whileContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(246);
				match(T__18);
				setState(247);
				hoare_assert(0);
				setState(248);
				match(T__19);
				setState(249);
				hoare_assert(0);
				setState(250);
				match(T__20);
				setState(251);
				hoare_trace(5);
				}
				break;
			case 7:
				{
				_localctx = new Hoare_trace_forallContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(253);
				match(T__21);
				setState(254);
				match(T__4);
				setState(255);
				hoare_trace(0);
				setState(260);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__9) {
					{
					{
					setState(256);
					match(T__9);
					setState(257);
					hoare_trace(0);
					}
					}
					setState(262);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(263);
				match(T__5);
				}
				break;
			case 8:
				{
				_localctx = new Hoare_trace_existsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(265);
				match(T__22);
				setState(266);
				match(T__4);
				setState(267);
				hoare_trace(0);
				setState(272);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__9) {
					{
					{
					setState(268);
					match(T__9);
					setState(269);
					hoare_trace(0);
					}
					}
					setState(274);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(275);
				match(T__5);
				}
				break;
			case 9:
				{
				_localctx = new Hoare_trace_assertContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(277);
				match(T__23);
				setState(278);
				match(T__4);
				setState(279);
				hoare_assert(0);
				setState(280);
				match(T__5);
				}
				break;
			case 10:
				{
				_localctx = new Hoare_trace_bracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(282);
				match(T__4);
				setState(283);
				hoare_trace(0);
				setState(284);
				match(T__5);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(293);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Hoare_trace_seqContext(new Hoare_traceContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_hoare_trace);
					setState(288);
					if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
					setState(289);
					match(SEMI);
					setState(290);
					hoare_trace(8);
					}
					} 
				}
				setState(295);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Hoare_termContext extends ParserRuleContext {
		public Hoare_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_term; }
	 
		public Hoare_termContext() { }
		public void copyFrom(Hoare_termContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Hoare_term_simpleContext extends Hoare_termContext {
		public TerminalNode NUM() { return getToken(RRGParser.NUM, 0); }
		public TerminalNode ID() { return getToken(RRGParser.ID, 0); }
		public TerminalNode KID() { return getToken(RRGParser.KID, 0); }
		public Hoare_term_simpleContext(Hoare_termContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_term_simple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_term_simple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_term_simple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_term_operContext extends Hoare_termContext {
		public List<Hoare_termContext> hoare_term() {
			return getRuleContexts(Hoare_termContext.class);
		}
		public Hoare_termContext hoare_term(int i) {
			return getRuleContext(Hoare_termContext.class,i);
		}
		public TerminalNode OPER() { return getToken(RRGParser.OPER, 0); }
		public Hoare_term_operContext(Hoare_termContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_term_oper(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_term_oper(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_term_oper(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_term_bracketsContext extends Hoare_termContext {
		public Hoare_termContext hoare_term() {
			return getRuleContext(Hoare_termContext.class,0);
		}
		public Hoare_term_bracketsContext(Hoare_termContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_term_brackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_term_brackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_term_brackets(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_termContext hoare_term() throws RecognitionException {
		return hoare_term(0);
	}

	private Hoare_termContext hoare_term(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Hoare_termContext _localctx = new Hoare_termContext(_ctx, _parentState);
		Hoare_termContext _prevctx = _localctx;
		int _startState = 36;
		enterRecursionRule(_localctx, 36, RULE_hoare_term, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case KID:
			case ID:
			case NUM:
				{
				_localctx = new Hoare_term_simpleContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(297);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << KID) | (1L << ID) | (1L << NUM))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case T__4:
				{
				_localctx = new Hoare_term_bracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(298);
				match(T__4);
				setState(299);
				hoare_term(0);
				setState(300);
				match(T__5);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(309);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Hoare_term_operContext(new Hoare_termContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_hoare_term);
					setState(304);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(305);
					match(OPER);
					setState(306);
					hoare_term(3);
					}
					} 
				}
				setState(311);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,28,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Hoare_assertContext extends ParserRuleContext {
		public Hoare_assertContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hoare_assert; }
	 
		public Hoare_assertContext() { }
		public void copyFrom(Hoare_assertContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Hoare_assert_boolContext extends Hoare_assertContext {
		public List<Hoare_assertContext> hoare_assert() {
			return getRuleContexts(Hoare_assertContext.class);
		}
		public Hoare_assertContext hoare_assert(int i) {
			return getRuleContext(Hoare_assertContext.class,i);
		}
		public TerminalNode BOOL_OP() { return getToken(RRGParser.BOOL_OP, 0); }
		public TerminalNode IMPL() { return getToken(RRGParser.IMPL, 0); }
		public Hoare_assert_boolContext(Hoare_assertContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_assert_bool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_assert_bool(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_assert_bool(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_assert_compContext extends Hoare_assertContext {
		public List<Hoare_termContext> hoare_term() {
			return getRuleContexts(Hoare_termContext.class);
		}
		public Hoare_termContext hoare_term(int i) {
			return getRuleContext(Hoare_termContext.class,i);
		}
		public TerminalNode EQ() { return getToken(RRGParser.EQ, 0); }
		public TerminalNode COMP() { return getToken(RRGParser.COMP, 0); }
		public TerminalNode SEUIL() { return getToken(RRGParser.SEUIL, 0); }
		public Hoare_assert_compContext(Hoare_assertContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_assert_comp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_assert_comp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_assert_comp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_assert_bracketsContext extends Hoare_assertContext {
		public Hoare_assertContext hoare_assert() {
			return getRuleContext(Hoare_assertContext.class,0);
		}
		public Hoare_assert_bracketsContext(Hoare_assertContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_assert_brackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_assert_brackets(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_assert_brackets(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Hoare_assert_negContext extends Hoare_assertContext {
		public TerminalNode NEG() { return getToken(RRGParser.NEG, 0); }
		public Hoare_assertContext hoare_assert() {
			return getRuleContext(Hoare_assertContext.class,0);
		}
		public Hoare_assert_negContext(Hoare_assertContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).enterHoare_assert_neg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RRGListener ) ((RRGListener)listener).exitHoare_assert_neg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RRGVisitor ) return ((RRGVisitor<? extends T>)visitor).visitHoare_assert_neg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Hoare_assertContext hoare_assert() throws RecognitionException {
		return hoare_assert(0);
	}

	private Hoare_assertContext hoare_assert(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Hoare_assertContext _localctx = new Hoare_assertContext(_ctx, _parentState);
		Hoare_assertContext _prevctx = _localctx;
		int _startState = 38;
		enterRecursionRule(_localctx, 38, RULE_hoare_assert, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				{
				_localctx = new Hoare_assert_compContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(313);
				hoare_term(0);
				setState(314);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << COMP) | (1L << EQ) | (1L << SEUIL))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(315);
				hoare_term(0);
				}
				break;
			case 2:
				{
				_localctx = new Hoare_assert_bracketsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(317);
				match(T__4);
				setState(318);
				hoare_assert(0);
				setState(319);
				match(T__5);
				}
				break;
			case 3:
				{
				_localctx = new Hoare_assert_negContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(321);
				match(NEG);
				setState(322);
				hoare_assert(1);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(330);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Hoare_assert_boolContext(new Hoare_assertContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_hoare_assert);
					setState(325);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(326);
					_la = _input.LA(1);
					if ( !(_la==IMPL || _la==BOOL_OP) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(327);
					hoare_assert(3);
					}
					} 
				}
				setState(332);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 7:
			return reg_expr_sempred((Reg_exprContext)_localctx, predIndex);
		case 12:
			return ctl_sempred((CtlContext)_localctx, predIndex);
		case 17:
			return hoare_trace_sempred((Hoare_traceContext)_localctx, predIndex);
		case 18:
			return hoare_term_sempred((Hoare_termContext)_localctx, predIndex);
		case 19:
			return hoare_assert_sempred((Hoare_assertContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean reg_expr_sempred(Reg_exprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		}
		return true;
	}
	private boolean ctl_sempred(CtlContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 6);
		case 2:
			return precpred(_ctx, 5);
		}
		return true;
	}
	private boolean hoare_trace_sempred(Hoare_traceContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 7);
		}
		return true;
	}
	private boolean hoare_term_sempred(Hoare_termContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean hoare_assert_sempred(Hoare_assertContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\64\u0150\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\5\2,\n\2\3\2\3\2\3\2\5\2\61\n\2\3\2"+
		"\5\2\64\n\2\3\2\7\2\67\n\2\f\2\16\2:\13\2\3\2\3\2\3\3\3\3\6\3@\n\3\r\3"+
		"\16\3A\3\4\3\4\3\4\3\4\3\4\3\5\3\5\6\5K\n\5\r\5\16\5L\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\5\6U\n\6\3\6\3\6\3\7\3\7\6\7[\n\7\r\7\16\7\\\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\6\be\n\b\r\b\16\bf\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\5\tv\n\t\3\t\3\t\3\t\7\t{\n\t\f\t\16\t~\13\t\3\n\3\n\7\n\u0082"+
		"\n\n\f\n\16\n\u0085\13\n\3\13\3\13\3\13\3\13\3\13\5\13\u008c\n\13\3\13"+
		"\3\13\3\f\3\f\6\f\u0092\n\f\r\f\16\f\u0093\3\r\3\r\5\r\u0098\n\r\3\r\3"+
		"\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u00b3\n\16\3\16\3\16"+
		"\3\16\3\16\3\16\3\16\7\16\u00bb\n\16\f\16\16\16\u00be\13\16\3\17\3\17"+
		"\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\5\20\u00cc\n\20\7\20"+
		"\u00ce\n\20\f\20\16\20\u00d1\13\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21"+
		"\5\21\u00da\n\21\7\21\u00dc\n\21\f\21\16\21\u00df\13\21\3\21\3\21\3\22"+
		"\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\5\23\u00f7\n\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u0105\n\23\f\23\16\23\u0108\13\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u0111\n\23\f\23\16\23\u0114\13"+
		"\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u0121"+
		"\n\23\3\23\3\23\3\23\7\23\u0126\n\23\f\23\16\23\u0129\13\23\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\5\24\u0131\n\24\3\24\3\24\3\24\7\24\u0136\n\24\f"+
		"\24\16\24\u0139\13\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\5\25\u0146\n\25\3\25\3\25\3\25\7\25\u014b\n\25\f\25\16\25\u014e"+
		"\13\25\3\25\2\7\20\32$&(\26\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \""+
		"$&(\2\5\3\2,.\3\2&(\4\2**\61\61\2\u0168\2+\3\2\2\2\4=\3\2\2\2\6C\3\2\2"+
		"\2\bH\3\2\2\2\nN\3\2\2\2\fX\3\2\2\2\16^\3\2\2\2\20u\3\2\2\2\22\177\3\2"+
		"\2\2\24\u0086\3\2\2\2\26\u008f\3\2\2\2\30\u0097\3\2\2\2\32\u00b2\3\2\2"+
		"\2\34\u00bf\3\2\2\2\36\u00c4\3\2\2\2 \u00d4\3\2\2\2\"\u00e2\3\2\2\2$\u0120"+
		"\3\2\2\2&\u0130\3\2\2\2(\u0145\3\2\2\2*,\5\4\3\2+*\3\2\2\2+,\3\2\2\2,"+
		"-\3\2\2\2-.\5\b\5\2.\60\5\f\7\2/\61\5\22\n\2\60/\3\2\2\2\60\61\3\2\2\2"+
		"\61\63\3\2\2\2\62\64\5\34\17\2\63\62\3\2\2\2\63\64\3\2\2\2\648\3\2\2\2"+
		"\65\67\5\26\f\2\66\65\3\2\2\2\67:\3\2\2\28\66\3\2\2\289\3\2\2\29;\3\2"+
		"\2\2:8\3\2\2\2;<\7\3\2\2<\3\3\2\2\2=?\7\33\2\2>@\5\6\4\2?>\3\2\2\2@A\3"+
		"\2\2\2A?\3\2\2\2AB\3\2\2\2B\5\3\2\2\2CD\7\'\2\2DE\7-\2\2EF\7(\2\2FG\7"+
		"/\2\2G\7\3\2\2\2HJ\7\34\2\2IK\5\n\6\2JI\3\2\2\2KL\3\2\2\2LJ\3\2\2\2LM"+
		"\3\2\2\2M\t\3\2\2\2NO\7\'\2\2OP\7-\2\2PQ\7(\2\2QR\7\4\2\2RT\7(\2\2SU\7"+
		")\2\2TS\3\2\2\2TU\3\2\2\2UV\3\2\2\2VW\7/\2\2W\13\3\2\2\2XZ\7\35\2\2Y["+
		"\5\16\b\2ZY\3\2\2\2[\\\3\2\2\2\\Z\3\2\2\2\\]\3\2\2\2]\r\3\2\2\2^_\7\'"+
		"\2\2_`\7\5\2\2`a\5\20\t\2ab\7\6\2\2bd\7+\2\2ce\7\'\2\2dc\3\2\2\2ef\3\2"+
		"\2\2fd\3\2\2\2fg\3\2\2\2gh\3\2\2\2hi\7/\2\2i\17\3\2\2\2jk\b\t\1\2kl\7"+
		"\60\2\2lv\5\20\t\7mn\7\7\2\2no\5\20\t\2op\7\b\2\2pv\3\2\2\2qr\7\'\2\2"+
		"rs\7.\2\2sv\7(\2\2tv\7\'\2\2uj\3\2\2\2um\3\2\2\2uq\3\2\2\2ut\3\2\2\2v"+
		"|\3\2\2\2wx\f\6\2\2xy\7\61\2\2y{\5\20\t\7zw\3\2\2\2{~\3\2\2\2|z\3\2\2"+
		"\2|}\3\2\2\2}\21\3\2\2\2~|\3\2\2\2\177\u0083\7\36\2\2\u0080\u0082\5\24"+
		"\13\2\u0081\u0080\3\2\2\2\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0083"+
		"\u0084\3\2\2\2\u0084\23\3\2\2\2\u0085\u0083\3\2\2\2\u0086\u0087\7&\2\2"+
		"\u0087\u0088\7-\2\2\u0088\u008b\7(\2\2\u0089\u008a\7\4\2\2\u008a\u008c"+
		"\7(\2\2\u008b\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\3\2\2\2\u008d"+
		"\u008e\7/\2\2\u008e\25\3\2\2\2\u008f\u0091\7\37\2\2\u0090\u0092\5\30\r"+
		"\2\u0091\u0090\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u0091\3\2\2\2\u0093\u0094"+
		"\3\2\2\2\u0094\27\3\2\2\2\u0095\u0096\7\'\2\2\u0096\u0098\7-\2\2\u0097"+
		"\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u009a\5\32"+
		"\16\2\u009a\u009b\7/\2\2\u009b\31\3\2\2\2\u009c\u009d\b\16\1\2\u009d\u009e"+
		"\7\60\2\2\u009e\u00b3\5\32\16\t\u009f\u00a0\7\7\2\2\u00a0\u00a1\5\32\16"+
		"\2\u00a1\u00a2\7\b\2\2\u00a2\u00b3\3\2\2\2\u00a3\u00a4\7$\2\2\u00a4\u00a5"+
		"\7\7\2\2\u00a5\u00a6\5\32\16\2\u00a6\u00a7\7\b\2\2\u00a7\u00b3\3\2\2\2"+
		"\u00a8\u00a9\7%\2\2\u00a9\u00aa\7\7\2\2\u00aa\u00ab\5\32\16\2\u00ab\u00ac"+
		"\7\t\2\2\u00ac\u00ad\5\32\16\2\u00ad\u00ae\7\b\2\2\u00ae\u00b3\3\2\2\2"+
		"\u00af\u00b0\7\'\2\2\u00b0\u00b1\t\2\2\2\u00b1\u00b3\7(\2\2\u00b2\u009c"+
		"\3\2\2\2\u00b2\u009f\3\2\2\2\u00b2\u00a3\3\2\2\2\u00b2\u00a8\3\2\2\2\u00b2"+
		"\u00af\3\2\2\2\u00b3\u00bc\3\2\2\2\u00b4\u00b5\f\b\2\2\u00b5\u00b6\7*"+
		"\2\2\u00b6\u00bb\5\32\16\t\u00b7\u00b8\f\7\2\2\u00b8\u00b9\7\61\2\2\u00b9"+
		"\u00bb\5\32\16\b\u00ba\u00b4\3\2\2\2\u00ba\u00b7\3\2\2\2\u00bb\u00be\3"+
		"\2\2\2\u00bc\u00ba\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\33\3\2\2\2\u00be"+
		"\u00bc\3\2\2\2\u00bf\u00c0\7 \2\2\u00c0\u00c1\5\36\20\2\u00c1\u00c2\5"+
		"\"\22\2\u00c2\u00c3\5 \21\2\u00c3\35\3\2\2\2\u00c4\u00c5\7!\2\2\u00c5"+
		"\u00c6\7\n\2\2\u00c6\u00cf\7\13\2\2\u00c7\u00c8\7\'\2\2\u00c8\u00c9\7"+
		"-\2\2\u00c9\u00cb\7(\2\2\u00ca\u00cc\7\f\2\2\u00cb\u00ca\3\2\2\2\u00cb"+
		"\u00cc\3\2\2\2\u00cc\u00ce\3\2\2\2\u00cd\u00c7\3\2\2\2\u00ce\u00d1\3\2"+
		"\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00d0\3\2\2\2\u00d0\u00d2\3\2\2\2\u00d1"+
		"\u00cf\3\2\2\2\u00d2\u00d3\7\r\2\2\u00d3\37\3\2\2\2\u00d4\u00d5\7#\2\2"+
		"\u00d5\u00d6\7\n\2\2\u00d6\u00dd\7\13\2\2\u00d7\u00d9\5(\25\2\u00d8\u00da"+
		"\7\f\2\2\u00d9\u00d8\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00dc\3\2\2\2\u00db"+
		"\u00d7\3\2\2\2\u00dc\u00df\3\2\2\2\u00dd\u00db\3\2\2\2\u00dd\u00de\3\2"+
		"\2\2\u00de\u00e0\3\2\2\2\u00df\u00dd\3\2\2\2\u00e0\u00e1\7\r\2\2\u00e1"+
		"!\3\2\2\2\u00e2\u00e3\7\"\2\2\u00e3\u00e4\7\n\2\2\u00e4\u00e5\5$\23\2"+
		"\u00e5\u00e6\7/\2\2\u00e6#\3\2\2\2\u00e7\u00e8\b\23\1\2\u00e8\u0121\7"+
		"\16\2\2\u00e9\u00ea\7\'\2\2\u00ea\u00eb\7\17\2\2\u00eb\u0121\7(\2\2\u00ec"+
		"\u00ed\7\'\2\2\u00ed\u0121\7\20\2\2\u00ee\u00ef\7\'\2\2\u00ef\u0121\7"+
		"\21\2\2\u00f0\u00f1\7\22\2\2\u00f1\u00f2\5(\25\2\u00f2\u00f3\7\23\2\2"+
		"\u00f3\u00f6\5$\23\2\u00f4\u00f5\7\24\2\2\u00f5\u00f7\5$\23\2\u00f6\u00f4"+
		"\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u0121\3\2\2\2\u00f8\u00f9\7\25\2\2"+
		"\u00f9\u00fa\5(\25\2\u00fa\u00fb\7\26\2\2\u00fb\u00fc\5(\25\2\u00fc\u00fd"+
		"\7\27\2\2\u00fd\u00fe\5$\23\7\u00fe\u0121\3\2\2\2\u00ff\u0100\7\30\2\2"+
		"\u0100\u0101\7\7\2\2\u0101\u0106\5$\23\2\u0102\u0103\7\f\2\2\u0103\u0105"+
		"\5$\23\2\u0104\u0102\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0104\3\2\2\2\u0106"+
		"\u0107\3\2\2\2\u0107\u0109\3\2\2\2\u0108\u0106\3\2\2\2\u0109\u010a\7\b"+
		"\2\2\u010a\u0121\3\2\2\2\u010b\u010c\7\31\2\2\u010c\u010d\7\7\2\2\u010d"+
		"\u0112\5$\23\2\u010e\u010f\7\f\2\2\u010f\u0111\5$\23\2\u0110\u010e\3\2"+
		"\2\2\u0111\u0114\3\2\2\2\u0112\u0110\3\2\2\2\u0112\u0113\3\2\2\2\u0113"+
		"\u0115\3\2\2\2\u0114\u0112\3\2\2\2\u0115\u0116\7\b\2\2\u0116\u0121\3\2"+
		"\2\2\u0117\u0118\7\32\2\2\u0118\u0119\7\7\2\2\u0119\u011a\5(\25\2\u011a"+
		"\u011b\7\b\2\2\u011b\u0121\3\2\2\2\u011c\u011d\7\7\2\2\u011d\u011e\5$"+
		"\23\2\u011e\u011f\7\b\2\2\u011f\u0121\3\2\2\2\u0120\u00e7\3\2\2\2\u0120"+
		"\u00e9\3\2\2\2\u0120\u00ec\3\2\2\2\u0120\u00ee\3\2\2\2\u0120\u00f0\3\2"+
		"\2\2\u0120\u00f8\3\2\2\2\u0120\u00ff\3\2\2\2\u0120\u010b\3\2\2\2\u0120"+
		"\u0117\3\2\2\2\u0120\u011c\3\2\2\2\u0121\u0127\3\2\2\2\u0122\u0123\f\t"+
		"\2\2\u0123\u0124\7/\2\2\u0124\u0126\5$\23\n\u0125\u0122\3\2\2\2\u0126"+
		"\u0129\3\2\2\2\u0127\u0125\3\2\2\2\u0127\u0128\3\2\2\2\u0128%\3\2\2\2"+
		"\u0129\u0127\3\2\2\2\u012a\u012b\b\24\1\2\u012b\u0131\t\3\2\2\u012c\u012d"+
		"\7\7\2\2\u012d\u012e\5&\24\2\u012e\u012f\7\b\2\2\u012f\u0131\3\2\2\2\u0130"+
		"\u012a\3\2\2\2\u0130\u012c\3\2\2\2\u0131\u0137\3\2\2\2\u0132\u0133\f\4"+
		"\2\2\u0133\u0134\7\62\2\2\u0134\u0136\5&\24\5\u0135\u0132\3\2\2\2\u0136"+
		"\u0139\3\2\2\2\u0137\u0135\3\2\2\2\u0137\u0138\3\2\2\2\u0138\'\3\2\2\2"+
		"\u0139\u0137\3\2\2\2\u013a\u013b\b\25\1\2\u013b\u013c\5&\24\2\u013c\u013d"+
		"\t\2\2\2\u013d\u013e\5&\24\2\u013e\u0146\3\2\2\2\u013f\u0140\7\7\2\2\u0140"+
		"\u0141\5(\25\2\u0141\u0142\7\b\2\2\u0142\u0146\3\2\2\2\u0143\u0144\7\60"+
		"\2\2\u0144\u0146\5(\25\3\u0145\u013a\3\2\2\2\u0145\u013f\3\2\2\2\u0145"+
		"\u0143\3\2\2\2\u0146\u014c\3\2\2\2\u0147\u0148\f\4\2\2\u0148\u0149\t\4"+
		"\2\2\u0149\u014b\5(\25\5\u014a\u0147\3\2\2\2\u014b\u014e\3\2\2\2\u014c"+
		"\u014a\3\2\2\2\u014c\u014d\3\2\2\2\u014d)\3\2\2\2\u014e\u014c\3\2\2\2"+
		"!+\60\638ALT\\fu|\u0083\u008b\u0093\u0097\u00b2\u00ba\u00bc\u00cb\u00cf"+
		"\u00d9\u00dd\u00f6\u0106\u0112\u0120\u0127\u0130\u0137\u0145\u014c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}