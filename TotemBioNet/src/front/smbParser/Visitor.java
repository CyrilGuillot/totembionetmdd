package front.smbParser;

import front.smbParser.grammar.RRGBaseVisitor;
import front.smbParser.grammar.RRGParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import util.TotemBionetException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;
import java.util.stream.Collectors;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


/**
 * Classe à modifier pour changer la génération du fichier .smb
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 * @author helen
 *
 */

public class Visitor extends RRGBaseVisitor<String> {

    int errorCounter;
    boolean hasHoare;

    private VarBlock varBlock;
    private RegBlock regBlock;
    private ParaBlock paraBlock;
    private CtlBlock ctlBlock;
    private HoareBlock hoareBlock;

    public Visitor() {
        this.errorCounter = 0;
        this.hasHoare = false;
        this.varBlock = new VarBlock();
        this.regBlock = new RegBlock();
        this.paraBlock = new ParaBlock();
        this.ctlBlock = new CtlBlock();
        this.hoareBlock = new HoareBlock();
    }

    public VarBlock getVarBlock() {
        return varBlock;
    }

    public RegBlock getRegBlock() {
        return regBlock;
    }

    public ParaBlock getParaBlock() {
        return paraBlock;
    }

    public CtlBlock getCtlBlock() {
        return ctlBlock;
    }

    public HoareBlock getHoareBlock() {
        return hoareBlock;
    }

    /********************************   Utilities  *********************************/
    private String getLineNumber(ParserRuleContext ctx){
        return "Line " + ctx.getStart().getLine() + ":" + ctx.getStart().getCharPositionInLine();
    }

    private String getLineNumber(ParserRuleContext ctx, TerminalNode node){
        return "Line " + ctx.getStart().getLine() + ":" + node.getSymbol().	getCharPositionInLine();
    }

    int getErrorCounter() {
        return this.errorCounter;
    }

    public boolean hasHoareBlock() {
        return this.hasHoare;
    }

    private void newErrorToPrint(String message) {
        errorCounter ++;
        System.out.println(message);
    }

    /********************************   Visitors  **********************************/

    public String visitProg(RRGParser.ProgContext ctx) { return visitChildren(ctx); } //Default implementation

    /*****************  Env_var Visitors  *****************/

    public String visitEnv_var_decl(RRGParser.Env_var_declContext ctx) {
        if(varBlock.existsVar(ctx.ID().getText(), null)){
            newErrorToPrint(getLineNumber(ctx, ctx.ID()) + " env_var \"" +  ctx.ID().getText() + "\" is already declared.");
        }
        Integer val = Integer.parseInt(ctx.NUM().getText());
        varBlock.addVar(VarType.ENV_VAR, ctx.ID().getText(),val , val,false);
        return visitChildren(ctx);
    }


    /*****************  Var Visitors  *****************/

    public String visitVar_decl(RRGParser.Var_declContext ctx) {

        if (varBlock.existsVar(ctx.ID().getText(), null)) {
            newErrorToPrint(getLineNumber(ctx, ctx.ID()) + " var \"" + ctx.ID().getText() + "\" is already declared.");
        }
        int min = Integer.parseInt(ctx.NUM(0).getText());
        int max = Integer.parseInt(ctx.NUM(1).getText());
        if (min>max){
            newErrorToPrint(getLineNumber(ctx, ctx.ID()) + " Bad bounds for var \"" + ctx.ID().getText() );
        }
        boolean snoussi = ctx.NS()==null;
        varBlock.addVar(VarType.VAR, ctx.ID().getText(), min, max,snoussi);
        return visitChildren(ctx);
    }


    /*****************  REG Visitors  *****************/

    public String visitReg_decl(RRGParser.Reg_declContext ctx) {
        List<String> targets = ctx.ID().stream().map(node -> node.getText()).collect(Collectors.toList());
        targets.remove(0);

        if(regBlock.existsReg(ctx.ID(0).getText())){
            newErrorToPrint(getLineNumber(ctx, ctx.ID(0)) + " REG block : reg SMBname \"" +  ctx.ID(0).getText() + "\" is already declared.");
        }

        if(varBlock.existsVar(ctx.ID(0).getText(), null)){
            newErrorToPrint(getLineNumber(ctx, ctx.ID(0)) + " REG block : reg SMBname \"" +  ctx.ID(0).getText() + "\" already exists as var SMBname.");
        }

        for(int i=0; i<targets.size(); i++){
            if(!varBlock.existsVar(targets.get(i), VarType.VAR)){
                newErrorToPrint(getLineNumber(ctx, ctx.ID(i+1)) + " REG block : reg target \"" +  targets.get(i) + "\" is not declared as a var.");
            }
        }

        RegOp firstOperation = createREGfrom(ctx.reg_expr());
        this.regBlock.addReg(ctx.ID(0).getText(), targets, "", firstOperation);
        return visitChildren(ctx);
    }

    private RegOp createREGfrom(RRGParser.Reg_exprContext ctx){
        switch(ctx.getClass().getSimpleName()){
            case "Expr_bracketsContext": {
                return createREGfrom(((RRGParser.Expr_bracketsContext)ctx).reg_expr());
            }
            case "Expr_bool_opContext": {
                RegOp expr1= createREGfrom(((RRGParser.Expr_bool_opContext) ctx).reg_expr(0));
                RegOp expr2= createREGfrom(((RRGParser.Expr_bool_opContext) ctx).reg_expr(1));
                return new RegOp(RegType.OPERATION,((RRGParser.Expr_bool_opContext) ctx).BOOL_OP().getText(),expr1,expr2);
            }
            case "Expr_negContext": {
                RegOp expr1=createREGfrom(((RRGParser.Expr_negContext)ctx).reg_expr());
                return new RegOp(RegType.OPERATION,"!",expr1,null);
            }
            case "Expr_atomeContext": {
                String id = ((RRGParser.Expr_atomeContext)ctx).ID().getText();
                Var v = varBlock.getVarWithId(id);
                if(!varBlock.existsVar(id, null)){
                    newErrorToPrint(getLineNumber((RRGParser.Expr_atomeContext)ctx) + " REG block : var \"" +  id + "\" used in declaration is not declared.");
                }
                else {
                    return new RegOp(RegType.ATOME,v,((RRGParser.Expr_atomeContext)ctx).NUM().getText());
                }
            }
            break;
            case "Expr_mux_nameContext": {
                String id = ((RRGParser.Expr_mux_nameContext)ctx).ID().getText();
                Regul regulation = this.regBlock.getRegWithId(id);
                if(regulation != null) {
                    RegOp op = regulation.getFormulaTree();
                    if(op.getType() == RegType.ATOME){
                        return new RegOp(RegType.ATOME,op.getVar(),op.getSeuil());
                    }else {
                        return new RegOp(op.getType(),op.getOperateur(),op.getExpr1(),op.getExpr2());
                    }
                } else {
                    newErrorToPrint(getLineNumber((RRGParser.Expr_mux_nameContext)ctx) + " REG block : multiplex SMBname \"" +  id + "\" used in declaration has not been declared previously.");
                }
            }
            break;
            default: {
                System.err.println(getLineNumber(ctx) + "Unknown REG expression type");
                break;
            }
        }
        return null;
    }

//    private void createREGfrom(RRGParser.Reg_exprContext ctx, RegOp operation){
//        switch(ctx.getClass().getSimpleName()){
//            case "Expr_bracketsContext": {
//                createREGfrom(((RRGParser.Expr_bracketsContext)ctx).reg_expr(), operation);
//            }
//            break;
//            case "Expr_bool_opContext": {
//                operation.setType(RegType.OPERATION);
//                operation.setOperateur(((RRGParser.Expr_bool_opContext) ctx).BOOL_OP().getText());
//                operation.setExpr1(new RegOp());
//                createREGfrom(((RRGParser.Expr_bool_opContext) ctx).reg_expr(0), operation.getExpr1());
//                operation.setExpr2(new RegOp());
//                createREGfrom(((RRGParser.Expr_bool_opContext) ctx).reg_expr(1), operation.getExpr2());
//            }
//            break;
//            case "Expr_negContext": {
//                operation.setType(RegType.OPERATION);
//                operation.setOperateur("!");
//                operation.setExpr1(new RegOp());
//                createREGfrom(((RRGParser.Expr_negContext)ctx).reg_expr(), operation.getExpr1());
//                operation.setExpr2(null);
//            }
//            break;
//            case "Expr_atomeContext": {
//                String id = ((RRGParser.Expr_atomeContext)ctx).ID().getText();
//                operation.setType(RegType.ATOME);
//                operation.setVar(varBlock.getVarWithId(id));
//                operation.setSeuil(((RRGParser.Expr_atomeContext)ctx).NUM().getText());
//                if(!varBlock.existsVar(id, null)){
//                    this.errorCounter ++;
//                    System.out.println(getLineNumber((RRGParser.Expr_atomeContext)ctx) + " REG block : var \"" +  id + "\" used in declaration is not declared.");
//                }
//            }
//            break;
//            case "Expr_mux_nameContext": {
//                String id = ((RRGParser.Expr_mux_nameContext)ctx).ID().getText();
//                Regul regulation = this.regBlock.getRegWithId(id);
//                if(regulation != null) {
//                    RegOp op = regulation.getFormulaTree();
//                    operation.setType(op.getType());
//
//                    if(op.getType() == RegType.ATOME){
//                        operation.setVar(op.getVar());
//                        operation.setSeuil(op.getSeuil());
//                    }else {
//                        operation.setOperateur(op.getOperateur());
//                        operation.setExpr1(op.getExpr1());
//                        if(op.getExpr2() != null){
//                              operation.setExpr2(op.getExpr2());
//                        }
//                    }
//
//                } else {
//                    this.errorCounter ++;
//                    operation = null;
//                    System.out.println(getLineNumber((RRGParser.Expr_mux_nameContext)ctx) + " REG block : multiplex SMBname \"" +  id + "\" used in declaration has not been declared previously.");
//                }
//            }
//            break;
//            default: {
//                System.err.println(getLineNumber(ctx) + "Unknown REG expression type");
//                break;
//            }
//        }
//    }

    /*****************  Param Visitors  *****************/

    public String visitPara_decl(RRGParser.Para_declContext ctx) {
        String id = ctx.KID().getText().substring(2); //Deletes "K_"

        String[] parts = null;
        if(id.contains(":")){
            parts = id.split("\\:");
        }else {
            parts = new String[]{id};
        }
        String gene = parts[0];

        if(!varBlock.existsVar(gene, null)){
            newErrorToPrint(getLineNumber(ctx, ctx.KID()) + " PARA block : \"" +  gene + "\" must be declared as var.");
        }

        List<String> regs = new ArrayList<>(Arrays.asList(parts));
        regs.remove(0); //Deletes gene SMBname
        Collections.sort(regs);
        Iterator<String> regsIterator = regs.iterator();
        while(regsIterator.hasNext()) {
            String reg = regsIterator.next();
           
            if(!regBlock.existsReg(reg)){
                newErrorToPrint(getLineNumber(ctx, ctx.KID()) + " PARA block : \"" +  reg + "\" must be declared as reg.");
            }

            //Verify duplications
            List<String> tmp = new ArrayList<>();
            tmp.addAll(regs);
            tmp.remove(reg);
            if(tmp.contains(reg)){
                newErrorToPrint(getLineNumber(ctx, ctx.KID()) + " PARA block : regulation SMBname \"" +  reg + "\" is duplicated.");
                regsIterator.remove();
            }
        }

        String finalId = "K" + gene;
        if (regs.size()!=0)
            finalId += "_" + String.join("_", regs);
        List<Regul> regulations = new ArrayList<>(regs.stream().map(reg -> regBlock.getRegWithId(reg)).collect(Collectors.toList()));
        int max = (ctx.NUM().size() > 1)?Integer.parseInt(ctx.NUM(1).getText()):Integer.parseInt(ctx.NUM(0).getText());
        this.paraBlock.addPara(gene, "K_"+id,finalId, regulations, Integer.parseInt(ctx.NUM(0).getText()), max);
        return visitChildren(ctx); }

    /*****************  CTL Visitors  *****************/


    public String visitCtl_block(RRGParser.Ctl_blockContext ctx) {
        CtlType type;
        if ("CTL".equals(ctx.KCTL().getText()))
            type = CtlType.CTL;
        else
            type = CtlType.FAIR_CTL;
        for (RRGParser.Ctl_declContext ctld : ctx.ctl_decl()) {
            String name = null;
            if (ctld.ID() != null) {
                name = ctld.ID().getText();
            }
            Ctl formula = createCtlfrom(ctld.ctl());
            CtlProp rootProp = new CtlProp(type,name,formula);
            this.ctlBlock.addProperty(rootProp);
        }
        return visitChildren(ctx);
    }

    private Ctl createCtlfrom(RRGParser.CtlContext ctx){
        switch(ctx.getClass().getSimpleName()){
            case "Ctl_bracketsContext": {
                return createCtlfrom(((RRGParser.Ctl_bracketsContext)ctx).ctl());
            }
            case "Ctl_bool_opContext": {
                String oper =((RRGParser.Ctl_bool_opContext) ctx).BOOL_OP().getText();
                Ctl expr1 = createCtlfrom(((RRGParser.Ctl_bool_opContext) ctx).ctl(0));
                Ctl expr2 = createCtlfrom(((RRGParser.Ctl_bool_opContext) ctx).ctl(1));
                return new Ctl(CtlOpType.OP_BOOL,oper,expr1,expr2);
            }
            case "Ctl_implContext": {
                String oper =((RRGParser.Ctl_implContext) ctx).IMPL().getText();
                Ctl expr1 = createCtlfrom(((RRGParser.Ctl_implContext) ctx).ctl(0));
                Ctl expr2 = createCtlfrom(((RRGParser.Ctl_implContext) ctx).ctl(1));
                return new Ctl(CtlOpType.IMPLICATION,oper,expr1,expr2);
            }
            case "Ctl_neg_opContext": {
                Ctl expr1 = createCtlfrom(((RRGParser.Ctl_neg_opContext)ctx).ctl());
                return new Ctl(CtlOpType.OP_BOOL,"!",expr1);
            }
            case "Ctl_temp_op1Context": {
                String oper =(((RRGParser.Ctl_temp_op1Context) ctx).CTL_PREFIX_1().getText());
                Ctl expr1 = createCtlfrom(((RRGParser.Ctl_temp_op1Context)ctx).ctl());
                return new Ctl(CtlOpType.TEMP_CTL,oper,expr1);
            }
            case "Ctl_temp_op2Context": {
                String oper =(((RRGParser.Ctl_temp_op2Context) ctx).CTL_PREFIX_2().getText());
                Ctl expr1 = createCtlfrom(((RRGParser.Ctl_temp_op2Context)ctx).ctl(0));
                Ctl expr2 = createCtlfrom(((RRGParser.Ctl_temp_op2Context) ctx).ctl(1));
                return new Ctl(CtlOpType.TEMP_CTL_U,oper,expr1,expr2);
            }
            //TODO : stable and oscill must be in FAIRCTL block
                // the semantics must be well defined
//            case "Ctl_oscilContext": {
//                String idVar =((RRGParser.Ctl_oscilContext) ctx).ID().getText();
//                Var var = varBlock.getVarWithId(idVar);
//                if(!varBlock.existsVar(idVar, null)){
//                    errorCounter++;
//                    System.out.println(getLineNumber((RRGParser.Ctl_oscilContext)ctx,((RRGParser.Ctl_oscilContext)ctx).ID()) + " OSCIL : \"" +  idVar + "\" is not declared.");
//                    break;
//                }
//                else {
//                    String num1 = ((RRGParser.Ctl_oscilContext) ctx).NUM(0).getText();
//                    String num2 = ((RRGParser.Ctl_oscilContext) ctx).NUM(1).getText();
//                    Ctl ctl1 = new Ctl(CtlOpType.ATOME, "=", var, num1);
//                    Ctl ctl2 = new Ctl(CtlOpType.ATOME, "=", var, num2);
//                    Ctl impl1 = new Ctl(CtlOpType.IMPLICATION,"->",ctl1,new Ctl(CtlOpType.TEMP_CTL,"AG",new Ctl(CtlOpType.OP_BOOL,"!",ctl2)));
//                    Ctl impl2 = new Ctl(CtlOpType.IMPLICATION,"->",ctl2,new Ctl(CtlOpType.TEMP_CTL,"AG",new Ctl(CtlOpType.OP_BOOL,"!",ctl1)));
//                    return new Ctl(CtlOpType.OP_BOOL, "&", impl1, impl2);
//                }
//            }
//            case "Ctl_stableContext": {
//                String idVar =((RRGParser.Ctl_stableContext) ctx).ID().getText();
//                Var var = varBlock.getVarWithId(idVar);
//                if(!varBlock.existsVar(idVar, null)){
//                    errorCounter++;
//                    System.out.println(getLineNumber((RRGParser.Ctl_stableContext)ctx,((RRGParser.Ctl_stableContext)ctx).ID()) + " STABLE : \"" +  idVar + "\" is not declared.");
//                    break;
//                }
//                else {
//                    String num = ((RRGParser.Ctl_stableContext) ctx).NUM().getText();
//                    Ctl ctl = new Ctl(CtlOpType.ATOME, "=", var, num);
//
//                    return new Ctl(CtlOpType.TEMP_CTL, "AF",new Ctl(CtlOpType.TEMP_CTL, "AG",ctl));
//                }
//            }
            case "Ctl_atomeContext": {
                String op = (((RRGParser.Ctl_atomeContext) ctx).COMP() != null ?
                        ((RRGParser.Ctl_atomeContext) ctx).COMP() :
                        ( ((RRGParser.Ctl_atomeContext) ctx).EQ() != null ?
                                ((RRGParser.Ctl_atomeContext) ctx).EQ() :
                                ((RRGParser.Ctl_atomeContext) ctx).SEUIL())).getText();
                Var v = varBlock.getVarWithId(((RRGParser.Ctl_atomeContext)ctx).ID().getText());
                String c = ((RRGParser.Ctl_atomeContext)ctx).NUM().getText();
                String id = ((RRGParser.Ctl_atomeContext)ctx).ID().getText();
                if(!varBlock.existsVar(id, null)){
                    newErrorToPrint(getLineNumber((RRGParser.Ctl_atomeContext)ctx,((RRGParser.Ctl_atomeContext)ctx).ID()) + " CTL block : \"" +  id + "\" is not declared.");
                    break;
                }
                else
                    return new Ctl(CtlOpType.ATOME,op,v,c);
            }
            // re-use of id is too much confusing without a declaration block for CTL formulas
//            case "Ctl_nameContext": {
//                String name = ((RRGParser.Ctl_nameContext)ctx).ID().getText();
//                CtlProp property = this.ctlBlock.getPropertyWithName(name);
//                if(property != null) {
//                    CtlOpType ct = property.getOpType();
//                    String oper =property.getOperateur();
//
//                    if(ct == CtlOpType.ATOME){
//                        return new Ctl(ct,oper,property.getVar(),property.getConstant());
//                    }else {
//                        return new Ctl(ct,oper,property.getExpr1(),property.getExpr2());
//                    }
//
//                } else {
//                    this.errorCounter ++;
//                    System.out.println(getLineNumber((RRGParser.Ctl_nameContext)ctx) + " CTL block : property SMBname \"" +  name + "\" used in this property declaration has not been declared previously.");
//                    break;
//                }
//            }
            default: {
                System.err.println(getLineNumber(ctx) + "Unknown CTL property type");
                break;
            }
        }
        return null;
    }



    /*****************  Hoare Visitors  *****************/

    public String visitHoare_block(RRGParser.Hoare_blockContext ctx) {
        this.hasHoare = true;
        return visitChildren(ctx);
    }


    public String visitHoare_pre_decl(RRGParser.Hoare_pre_declContext ctx) {
        int size = ctx.ID().size();
        for(int i=0; i<size; i++){
            if(!varBlock.existsVar(ctx.ID(i).getText(), null)){
                newErrorToPrint(getLineNumber(ctx, ctx.ID(i)) + " Hoare block : \"" +  ctx.ID(i).getText() + "\" is not declared as a var or env_var.");
            }
            varBlock.addPreVar(ctx.ID(i).getText());
            this.hoareBlock.addPreExpression(ctx.ID(i).getText(), ctx.EQ(i).getText(), Integer.parseInt(ctx.NUM(i).getText()));
        }
        return visitChildren(ctx); 
    }

    public String visitHoare_post_decl(RRGParser.Hoare_post_declContext ctx) {
        int size = ctx.hoare_assert().size();
        for(int i=0; i<size; i++){
            HoareAssert assertion = new HoareAssert();
            createAssertFrom( ((RRGParser.Hoare_post_declContext)ctx).hoare_assert(i), assertion);
            this.hoareBlock.addPostExpression(assertion);
        }
        return visitChildren(ctx);
    }

    public String visitHoare_trace_decl(RRGParser.Hoare_trace_declContext ctx) { 

        HoareTrace trace = new HoareTrace();
        createTraceFrom(ctx.hoare_trace(), trace);
        this.hoareBlock.setTraceExpression(trace);

        return visitChildren(ctx); 
    }


    // TODO : gérer les priorité correctement dans les assertions
    //      - dans la grammaire mettre la négation en 1ere ligne
    //      - dans ce code, rectifier en faisant de return (cf pb CTL)
    private void createAssertFrom(RRGParser.Hoare_assertContext ctx, HoareAssert assertion){
        switch(ctx.getClass().getSimpleName()){
            case "Hoare_assert_boolContext": {
                assertion.setType(HoareAssertType.BOOL_OP);
                assertion.setExpr1(new HoareAssert());
                createAssertFrom(((RRGParser.Hoare_assert_boolContext)ctx).hoare_assert(0), assertion.getExpr1());
                assertion.setExpr2(new HoareAssert());
                createAssertFrom(((RRGParser.Hoare_assert_boolContext)ctx).hoare_assert(1), assertion.getExpr2());
                 String op = ((RRGParser.Hoare_assert_boolContext)ctx).BOOL_OP() != null ?
                            ((RRGParser.Hoare_assert_boolContext)ctx).BOOL_OP().getText() 
                            : ((RRGParser.Hoare_assert_boolContext)ctx).IMPL().getText();
                assertion.setOp(op);
            }
            break;
            case "Hoare_assert_compContext": {
                assertion.setType(HoareAssertType.COMP);
                assertion.setExpr1(new HoareAssert());
                createAssertFromTerm(((RRGParser.Hoare_assert_compContext)ctx).hoare_term(0), assertion.getExpr1());
                assertion.setExpr2(new HoareAssert());
                createAssertFromTerm(((RRGParser.Hoare_assert_compContext)ctx).hoare_term(1), assertion.getExpr2());
                String op = ((RRGParser.Hoare_assert_compContext)ctx).EQ() != null ?
                            ((RRGParser.Hoare_assert_compContext)ctx).EQ().getText() 
                            : (((RRGParser.Hoare_assert_compContext)ctx).COMP() != null ?
                                ((RRGParser.Hoare_assert_compContext)ctx).COMP().getText() 
                                : ((RRGParser.Hoare_assert_compContext)ctx).SEUIL().getText());
                assertion.setOp(op);
            }
            break;
            case "Hoare_assert_bracketsContext": {
                createAssertFrom(((RRGParser.Hoare_assert_bracketsContext)ctx).hoare_assert(), assertion);
            }
            break;
            case "Hoare_assert_negContext": {
                assertion.setType(HoareAssertType.NEG);
                assertion.setOp(((RRGParser.Hoare_assert_negContext)ctx).NEG().getText());
                assertion.setExpr1(new HoareAssert());
                createAssertFrom(((RRGParser.Hoare_assert_negContext)ctx).hoare_assert(), assertion.getExpr1());
            }
            break;

            default :{
                System.err.println("Error : Unknown Hoare_Assert type");
            }
            break;

        } 
    }

    private void createAssertFromTerm(RRGParser.Hoare_termContext ctx, HoareAssert assertion){
        switch(ctx.getClass().getSimpleName()){
            case "Hoare_term_simpleContext": {
                String term;
                if( ((RRGParser.Hoare_term_simpleContext)ctx).NUM() != null ){
                    assertion.setType(HoareAssertType.SIMPLE_TERM_NUM);
                    term =  ((RRGParser.Hoare_term_simpleContext)ctx).NUM().getText();

                } else if( ((RRGParser.Hoare_term_simpleContext)ctx).ID() != null ){
                                    /* TODO - check errors */
                    assertion.setType(HoareAssertType.SIMPLE_TERM_ID);
                    term = ((RRGParser.Hoare_term_simpleContext)ctx).ID().getText();

                } else {
                                    /* TODO - check errors */
                    assertion.setType(HoareAssertType.SIMPLE_TERM_KID);
                    term = ((RRGParser.Hoare_term_simpleContext)ctx).KID().getText();

                }
                assertion.setTerm(term);
            }
            break;
            case "Hoare_term_operContext": {
                assertion.setType(HoareAssertType.OPER_TERM);
                assertion.setExpr1(new HoareAssert());
                createAssertFromTerm(((RRGParser.Hoare_term_operContext)ctx).hoare_term(0), assertion.getExpr1());
                assertion.setExpr2(new HoareAssert());
                createAssertFromTerm(((RRGParser.Hoare_term_operContext)ctx).hoare_term(1), assertion.getExpr2());
                String op = ((RRGParser.Hoare_term_operContext)ctx).OPER().getText();
                assertion.setOp(op);

            }
            break;
            case "Hoare_term_bracketsContext": {
                createAssertFromTerm(((RRGParser.Hoare_term_bracketsContext)ctx).hoare_term(), assertion);
            }
            break;
            default :{
                System.err.println("Error : Unknown Hoare_Term type");
            }
            break;
        }
    }

    private void createTraceFrom(RRGParser.Hoare_traceContext ctx, HoareTrace trace){
        switch(ctx.getClass().getSimpleName()){
            case "Hoare_trace_affectContext": {
                trace.setType(HoareTraceType.AFFECT);
                trace.setId(((RRGParser.Hoare_trace_affectContext) ctx).ID().getText());
                trace.setConstant(Integer.parseInt(((RRGParser.Hoare_trace_affectContext) ctx).NUM().getText()));
            }break;
            case "Hoare_trace_decrContext": {
                trace.setType(HoareTraceType.OPER);
                trace.setId(((RRGParser.Hoare_trace_decrContext) ctx).ID().getText());
                trace.setOp("-");
            }break;
            case "Hoare_trace_incrContext": {
               trace.setType(HoareTraceType.OPER);
               trace.setId(((RRGParser.Hoare_trace_incrContext) ctx).ID().getText());
               trace.setOp("+");
            }break;
            case "Hoare_trace_ifContext": {
                trace.setType(HoareTraceType.IF);
                HoareAssert condition = new HoareAssert();
                createAssertFrom(((RRGParser.Hoare_trace_ifContext) ctx).hoare_assert(), condition);
                trace.addAssert(condition);
                int size = ((RRGParser.Hoare_trace_ifContext) ctx).hoare_trace().size();
                for(int i=0; i<size; i++){
                    HoareTrace tr = new HoareTrace();
                    createTraceFrom(((RRGParser.Hoare_trace_ifContext) ctx).hoare_trace(i), tr);
                    trace.addTrace(tr);
                }
            }break;
            case " Hoare_trace_whileContext": {
                trace.setType(HoareTraceType.WHILE);
                int size = ((RRGParser.Hoare_trace_whileContext) ctx).hoare_assert().size();
                for(int i=0; i<size; i++){
                    HoareAssert cond = new HoareAssert();
                    createAssertFrom(((RRGParser.Hoare_trace_whileContext) ctx).hoare_assert(i), cond);
                    trace.addAssert(cond);
                }
                HoareTrace tr = new HoareTrace();
                createTraceFrom(((RRGParser.Hoare_trace_whileContext) ctx).hoare_trace(), tr);
                trace.addTrace(tr);

            }break;
            case "Hoare_trace_forallContext": {
                int size = ((RRGParser.Hoare_trace_forallContext) ctx).hoare_trace().size();
                trace.setType(HoareTraceType.FORALL);
                for(int i=0; i<size; i++){
                    HoareTrace tr = new HoareTrace();
                    createTraceFrom(((RRGParser.Hoare_trace_forallContext) ctx).hoare_trace(i), tr);
                    trace.addTrace(tr);
                }

            }break;
            case "Hoare_trace_existsContext": {
                int size = ((RRGParser.Hoare_trace_existsContext) ctx).hoare_trace().size();
                trace.setType(HoareTraceType.EXISTS);
                for(int i=0; i<size; i++){
                    HoareTrace tr = new HoareTrace();
                    createTraceFrom(((RRGParser.Hoare_trace_existsContext) ctx).hoare_trace(i), tr);
                    trace.addTrace(tr);
                }
               
            }break;
            case "Hoare_trace_assertContext": {
                trace.setType(HoareTraceType.ASSERT);
                HoareAssert condition = new HoareAssert();
                createAssertFrom(((RRGParser.Hoare_trace_assertContext) ctx).hoare_assert(), condition);
                trace.addAssert(condition);
            }break;
            case "Hoare_trace_seqContext": {
                trace.setType(HoareTraceType.SEQ);
                int size = ((RRGParser.Hoare_trace_seqContext) ctx).hoare_trace().size();
                for(int i=0; i<size; i++){
                    HoareTrace tr = new HoareTrace();
                    createTraceFrom(((RRGParser.Hoare_trace_seqContext) ctx).hoare_trace(i), tr);
                    trace.addTrace(tr);
                }
            }break;
            case "Hoare_trace_skipContext": {
                trace.setType(HoareTraceType.SKIP);
            }break;

            case "Hoare_trace_bracketsContext": {
                createTraceFrom(((RRGParser.Hoare_trace_bracketsContext) ctx).hoare_trace(), trace);
            }break;
            default: {
                System.err.println("Error : Unknown Hoare_trace type " + ctx.getClass().getSimpleName());
            }break;
            
        }
    }

    /****************  Generate visitor ***************/

    public void generateJSON(String outputFilename){
        String result = "{\"result\":[";

        String varDeclaration = this.varBlock.toJSON();
        result += varDeclaration;

        String regDeclaration = this.regBlock.toJSON();
        result += ",";
        result += regDeclaration;

        String paraDeclaration = this.paraBlock.toJSON();
        result += ",";
        result += paraDeclaration;

        String ctlDeclaration = this.ctlBlock.toJSON();
        result += ",";
        result += ctlDeclaration;

        result += "]}";

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(outputFilename));
            out.write(result);
            out.close();
            System.out.println(" *** Success - " + outputFilename + " file has been generated *** ");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void generateBRNOCaml(FileOutputStream outstream) {
        try {
            String s = this.hoareBlock.toBRNOcaml(this.varBlock.getData(),
                                                this.regBlock.getRegs());
            outstream.write(s.getBytes());
            System.out.println(" *** Success - BRN generated *** ");
        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
    }


    public boolean generateTripletOCaml(FileOutputStream outstream) {
        if (!varBlock.allVarInPreHoare()) {
            String message = " Hoare triple is not used\n";
            message += "Pre-condition of Hoare triple does not assign a value to each variable";
            System.out.println(" *** FAILURE - " + message);
            return false;
        }
        try {
            outstream.write(this.hoareBlock.toTripletOcaml().getBytes());
            System.out.println(" *** Success - Triplets generated *** ");

        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
        return true;
    }
}
