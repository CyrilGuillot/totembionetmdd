package front.smbParser;


import back.jlogic.Formula;
import java.util.*;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class CtlBlock {

    private List<CtlProp> properties;

    public CtlBlock() {
        this.properties = new ArrayList<CtlProp>();
    }

    public void addProperty(CtlProp prop){
        this.properties.add(prop);
    }

    public CtlProp getPropertyWithName(String name){
        for(CtlProp prop : this.properties){
            if(prop.getName().equals(name)){
                return prop;
            }
        }
        return null;
    }

    public String toJSON(){
        String ctlBlock = "{\"blocktype\" : \"ctl\",\n \"value\" : [";
        for(CtlProp prop: properties){
            ctlBlock += prop.toJSON();
            ctlBlock += ",";
        }
        ctlBlock = ctlBlock.substring(0, ctlBlock.length() - 1);
        ctlBlock += "]}";
        return ctlBlock;
    }


    public List<Formula>[] getBackFormat() throws Exception {
        List<Formula> ctlList = new ArrayList<>();
        List<Formula> fairCtlList = new ArrayList<>();
        for(CtlProp prop : this.properties){
            Formula f = prop.toBack();
            f.name = prop.getName();
            if (prop.getType()==CtlType.CTL) {
                ctlList.add(f);
            }
            else {
                fairCtlList.add(f);
            }
        }
        return new List[]{ctlList, fairCtlList};
    }


}