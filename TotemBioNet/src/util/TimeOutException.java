package util;

/**
 * @author Hélène Collavizza
 * Not used, only for formal version
 */
public class TimeOutException extends RuntimeException {

    TimeOutException() {
        super("NuSMV TIME OUT");
    }
}
