package back.jlist;

public interface ObjectWithStringID {
     String stringId();
}
