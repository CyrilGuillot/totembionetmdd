package back.net;

import back.jlist.*;
import back.jlogic.Formula;

import java.util.*;


/**
 * @author Adrien Richard
 * @author helen Collavizza
 */


public class Para implements ObjectWithStringID{

    //Nom du paramètre dans le fichier SMB
    private String SMBname;

    // nom du paramètre en syntaxe obligée pour NuSMV
    private String nuSMVname;

    //Liste des regulations du parametre
    public final OrderedArrayList<Reg> regs;

    //Intervalles possibles pour le param�tres
    ArrayList<Interval> intervals = new ArrayList<Interval>();

    //Indice du plus petit et du plus grand intervalle que le
    //param�tre peut prendre
    private int minInt;
    private int maxInt;

    //Indice de l'interval courant associ� au param�tre
    private int currentInt;

    //Indice de l'interval maximum courant associ� au param�tre
    private int currentMaxInt;

    //Niveau maximum est minimum pour le param�tre <donn�s par
    //l'utilisateur>
    private int minLevel;
    private int maxLevel;

    // Niveau min et max quand la variable est libre
    private int minFormalLevel;
    private int maxFormalLevel;

    //Param�tres dont l'ensemble des regulations contient
    //"imm�diatement" l'ensemble des regulations de regs
    public OrderedArrayList<Para> succs = new OrderedArrayList<Para>();

    // pour propagation de bornes en formel
    public OrderedArrayList<Para> preds = new OrderedArrayList<Para>();

    // fixe = true ssi on a fix� une valeur pour ce
    // param�tre dans le bloc PARA du ficher .smb
    public boolean fixe;

    // libre = true ssi cette var est formelle
    public boolean libre;

    // nb de fois où il apparait dans la table des ressources
    int nbTableRessource = 0;

    //Constructeur

    // dans cette version, par d�faut les param�tres ne sont
    // pas libres, y compris ceux qui n'ont pas de valeurs pr�d�finies
    // dans le bloc PARA
    public Para(String nuSMVname, String SMBname, OrderedArrayList<Reg> regs) {
        this(nuSMVname,SMBname, regs, false, false);
    }

    public Para(String nuSMVname, String SMBname, OrderedArrayList<Reg> regs, boolean libre, boolean fixe) {
        this.SMBname =SMBname;
        this.regs = regs;
        this.libre = libre;
        this.fixe = fixe;
        this.nuSMVname = nuSMVname;
    }

    //La description = le nom nuSMV
    public String toString() {
        return nuSMVname;
    }

    // utilitaires pour passer de l'écriture de noms de Para
    // format SMB à format NuSMV
    /////////////////////////////////////////////////////

    public String getSMBname() {return SMBname; }

    public String getNuSMVname() {
        return nuSMVname; }


    /**
     *
     * @param name : un nom de para issu de Hoare non trié
     * @return : un nom de para format SMB trié
     */
    public static String sortName(String name) {
        String[] parts = null;
        if(name.contains(":")){
            parts = name.split("\\:");
        }else {
            parts = new String[]{name};
        }
        String gene = parts[0];

        List<String> regs = new ArrayList<>(Arrays.asList(parts));
        regs.remove(0); //Deletes gene SMBname
        Collections.sort(regs);
        String finalId = gene;
        if (regs.size() != 0)
            finalId += ":" + String.join(":", regs);
        return finalId;
    }

    //ecriture des intervalles possibles,
    // y compris ceux qui ne sont pas utilisés du fait de Snoussi
    public String allIntervalsToString() {
        String s = "";
        for (int i = 0; i < minInt; i++)
            s += intervals.get(i).toString();
        s += " {";
        for (int i = minInt; i <= maxInt; i++)
            s += intervals.get(i).toString();
        s += "} ";
        for (int i = maxInt + 1; i < intervals.size(); i++)
            s += intervals.get(i).toString();
        return s;
    }

    //ecriture des intervalles possibles,
    // y compris ceux qui ne sont pas utilisés du fait de Snoussi
    public String intervalsToString() {
        String s = "Intervals used for " + SMBname + " : ";
        for (int i = minInt; i <= maxInt; i++)
            s += intervals.get(i).toString();
        return s;
    }

    // urtuilisé pour les variables auto-régulées en version formelle
    public ArrayList<Integer> valueSet() {
        // System.out.println("para " + this.SMBname + allIntervalsToString());
        ArrayList<Integer> val = new ArrayList<Integer>();
        for (Interval inter : intervals) {
            val.add(inter.min);
        }
        //System.out.println("end para " + this.SMBname + val);
        return val;
    }

    //CONSULTATIONS SUR LES INTERVALLES ET VALEURS POSSIBLES

    //Donne le plus grand intervalle possible
    public Interval maxInt() {
        return intervals.get(maxInt);
    }

    //Donne le plus petit intervalle possible
    public Interval minInt() {
        return intervals.get(minInt);
    }

    //Donne l'intervalle courant
    public Interval currentInt() {
        return intervals.get(currentInt);
    }

    //Donne le niveau minimal (indiqu� par l'utilisateur)
    public int minLevel() {
        return minLevel;
    }

    //Donne le niveau maximal (indiqu� par l'utilisateur)
    public int maxLevel() {
        return maxLevel;
    }

    // niveau min de l'intervalle courant
    // c'est ce niveau qui est utilisé pour généré les paramétrages
    public int currentMin() {return intervals.get(currentInt).min; }

    public int minMin() {return minInt().min;}
    public int maxMax() {return maxInt().max;}

    //MODIFICATION DES INTERVALLES MIN ET MAX ET DES NIVEAUX MIN ET MAX

    //Apr�s cette m�thode: le plus petit (grand) interval possible est
    //celui contenant min (max)
    public boolean setMinMax(int minLevel, int maxLevel) {
        int minInt = -1;
        int maxInt = -1;
        for (int i = 0; i < intervals.size(); i++) {
            if (intervals.get(i).min <= minLevel && minLevel <= intervals.get(i).max)
                minInt = i;
            if (intervals.get(i).min <= maxLevel && maxLevel <= intervals.get(i).max)
                maxInt = i;
        }
        //Si tout va bien, on change effectivement minInt, LastInt,
        //minLevel et maxLevel
        if (minLevel <= maxLevel && minInt >= 0 && maxInt >= 0) {
            this.minLevel = minLevel;
            this.maxLevel = maxLevel;
            this.minFormalLevel = minLevel;
            this.maxFormalLevel = maxLevel;
            this.minInt = minInt;
            this.maxInt = maxInt;
            return true;
        }
        return false;
    }

    //Re-initialise minInt et maxInt en fonction de minLevel et maxLevel

    public void updateMinIntMaxInt() {
        setMinMax(minLevel, maxLevel);
    }

    //R�duit l'intervalle max jusqu'� ce qu'il soit <=, au sens
    //faible, � l'intervalle transmis en argument
    public boolean setMaxIntWeakLessThan(Interval interval) {
        while (!maxInt().weakLess(interval) && maxInt >= minInt)
            maxInt--;
        return maxInt().weakLess(interval);
    }

    //Augmente l'intervalle min jusqu'� ce qu'il soit >=, au sens
    //faible, � l'intervalle transmis en argument
    public boolean setMinIntWeakGreaterThan(Interval interval) {
        while (!interval.weakLess(minInt()) && minInt <= maxInt)
            minInt++;
        return interval.weakLess(minInt());
    }

    //MODIFICATION DE L'INTERVALLE COURANT ET DE L'INTERVALLE MAX COURANT

    //L'intervalle courant est le plus petit possible

    public void firstCurrentInt() {
        currentInt = minInt;
    }

    //Augmentation de l'intervalle courant, s'il atteint l'intervalle
    //max courant on le met au min et on retourne faux, sinon on
    //retourne true

    public boolean nextCurrentInt() {
        if (currentInt < currentMaxInt) {
            currentInt++;
            return true;
        }
        currentInt = minInt;
        return false;
    }

    //Apr�s l'appel, l'intervalle max courant est l'intervalle max
    public void setCurrentMaxToMax() {
        currentMaxInt = maxInt;
    }

    //Après l'appel à cette méthode, l'intervalle max courant est le
    //plus grand intervalle <=, au sens faible, à l'intervalle courant
    //des successeurs du paramètre

    public void setCurrentMaxLessThanSuccs() {
        //On regarde la plus petite borne sup de l'intervalle courant
        //des successeurs
        int smallMax = intervals.get(maxInt).max;
        for (int i = 0; i < succs.size(); i++)
            if (succs.get(i).currentInt().max < smallMax)
                smallMax = succs.get(i).currentInt().max;
        //L'intervalle max courant est celui qui contient cette plus
        //petite borne sup
        for (int i = 0; i < intervals.size(); i++)
            if (intervals.get(i).min <= smallMax && smallMax <= intervals.get(i).max) {
                currentMaxInt = i;
                break;
            }
    }

    public Formula getFormula(int i) {
        return regs.get(i).formula;
    }


    /////////////////////////////////////////////////////
    // utilisé pour les OrderedArrayList
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Para para = (Para) o;
//        return minLevel == para.minLevel &&
//                maxLevel == para.maxLevel &&
//                fixe == para.fixe &&
//                libre == para.libre &&
//                Objects.equals(SMBname, para.SMBname) &&
//                Objects.equals(nuSMVname, para.nuSMVname);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(SMBname, nuSMVname, minLevel, maxLevel, fixe, libre);
//    }


    ////////////////////////////////////////////
    // utilisé pour la version formelle

    //Donne le niveau minimal (indiqu� par l'utilisateur)
    public int minFormalLevel() {
        return minFormalLevel;
    }

    public void setMinFormalLevel(int m) {
        minFormalLevel = m;
    }

    //Donne le niveau maximal (indiqu� par l'utilisateur)
    public int maxFormalLevel() {
        return maxFormalLevel;
    }

    public void setMaxFormalLevel(int m) {
        maxFormalLevel = m;
    }

    public void resetFormalLevels() {
        this.minFormalLevel = minLevel;
        this.maxFormalLevel = maxLevel;
    }

    @Override
    public String stringId() {
        return nuSMVname;
    }
}
