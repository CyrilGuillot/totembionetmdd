package back.net;

import back.jlist.ObjectWithStringID;

/**
 * @author Adrien Richard
 */

public class Interval {

    final int min;
    final int max;
    private boolean special;

    public Interval(int min, int max) {
        this.min = min;
        this.max = max;
        this.special =false;
    }

    public Interval(int i) {
        this(i,i);
        this.special=true;
    }

    //Un intervalle A est "faiblement inf�rieur" � un intervalle B si
    //min(A)<=max(B).

    public boolean weakLess(Interval interval) {
        return min <= interval.max;
    }

    public String toString() {
        String name = "";
        if (min == max)
            name = "(" + min + ")";
        else
            name = "(" + min + "~" + max + ")";
        if (special)
            name = "[" + min + "]";
        return name;
    }

}