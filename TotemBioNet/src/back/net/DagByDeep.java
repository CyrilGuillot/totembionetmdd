package back.net;

import back.jlist.OrderedArrayList;
import util.Out;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Hélène Collavizza à partir du code d'Adrien
 * @author Adrien Richard
 */
public class DagByDeep {
    private OrderedArrayList<Para> paras;
    private List<List<Para>> dagByDeep;

    public DagByDeep(OrderedArrayList<Para> paras) {
        this.paras=paras;
        this.dagByDeep=  new ArrayList<List<Para>>();
        try {
            initDag();
        } catch (Exception e) {
            System.out.println("Problem when creating the DAG");
            e.printStackTrace();
        }
    }

    //On construit le graphe d'inclusion entre les param�tres: (1)
    //C'est le plus petit graphe, tel qui si l'ensemble des
    //r�gulations de psf est inclus dans l'ensemble des r�gulations de
    //psf', alors il y a un chemin de psf � psf' dans le graphe; (2) S'il y
    //a un arc de psf � psf', alors la condition de monotonicit� de psf->psf'
    //peut �tre viol�e; (3) Pour chaque arc psf->psf' on supprime les
    //intervalles de psf et de psf' qui rendent impossibles la condition de
    //monotonicit�
    private void initDag() throws Exception {
        //On initialise les arcs entrant et sortant et le domaine de
        //variation des param�tres (qui sera �ventuellement r�duit
        //lors de la construction du DAG)
        for (int i = 0; i < paras.size(); i++) {
            paras.get(i).succs.clear();
            paras.get(i).updateMinIntMaxInt();
        }
        //Calcul des arcs (en profitant du fait que la liste des
        //param�tres soit tri�e par nombre de r�gulateurs croissant)
        for (int i = 0; i < paras.size(); i++) {
            Para p1 = paras.get(i);
            for (int j = i + 1; j < paras.size(); j++) {
                Para p2 = paras.get(j);
                if (p1.regs.sublist(p2.regs)) {
                    //p2 est candidat pour �tre un successeur de p1,
                    //on regarde s'il est minimum pour l'inclusion
                    boolean arc = true;
                    for (int k = i + 1; k < paras.size(); k++)
                        if (p1.regs.sublist(paras.get(k).regs) &&
                                paras.get(k).regs.strictSublist(p2.regs)) {
                            arc = false;
                            break;
                        }

                    //Avant de cr�er l'arc, on regarde en plus si la
                    //condition de monotonicit� p1->p2 peut �tre fausse
                    if (arc && !p1.maxInt().weakLess(p2.minInt())) {
                        //Cr�ation d'un arc de p1 vers p2
                        p1.succs.add(p2);
                        p2.preds.add(p1);
                        //Si la condition de monotonicit� ne peut
                        //jamais �tre v�rifi�e, on a une exception
                        if (!p1.minInt().weakLess(p2.maxInt()))
                            throw new Exception("No possible parameterization for gene " + this +
                                    "(parameter " + p1 + " cannot be less than " + p2 + ")");
                        //R�duction de l'intervalle max de p1
                        p1.setMaxIntWeakLessThan(p2.maxInt());
                        //Augmentation de l'intervalle min de p2
                        p2.setMinIntWeakGreaterThan(p1.minInt());
                    }
                }
            }
        }
        //Construction du "DAG par profondeur"
        setDagByDeep(0);
    }

    //Construction du "DAG par profondeur". C'est une liste de listes:
    //la liste 0 contient tous les param�tres sans successeur, et la
    //liste psf>0 contient tous les param�tres dont tous les successeurs
    //sont dans la liste psf-1;
    private void setDagByDeep(int deep) {
        //Profondeur 0 = cas de base = tous les param�tres sans successeur
        if (deep == 0) {
            OrderedArrayList<Para> l = new OrderedArrayList<Para>();
            for (int i = 0; i < paras.size(); i++)
                if (paras.get(i).succs.isEmpty())
                    l.add(paras.get(i));
            dagByDeep.clear();
            dagByDeep.add(l);
            setDagByDeep(1);
        }
        //Profondeur psf>0 = tous les param�tres dont tous les
        //successeurs sont � la profondeur psf-1
        else {
            OrderedArrayList<Para> l = new OrderedArrayList<Para>();
            for (int i = 0; i < paras.size(); i++)
                if (!paras.get(i).succs.isEmpty() &&
                        paras.get(i).succs.sublist(dagByDeep.get(deep - 1)))
                    l.add(paras.get(i));
            dagByDeep.add(l);
            if (l.size() > 0)
                setDagByDeep(deep + 1);
        }
    }

    // pour afficher les contraintes de Snoussi entre paramètres
    public void printDagConstraints() throws IOException {
        Out.psfln("# Snoussi useful constraints:");
        for (Para p1 : paras)
            for (Para p2 : p1.succs)
                Out.psfln(p1.getSMBname() + " <= " + p2.getSMBname() + ";");
    }

    public void print() {
        for (int i=0;i<dagByDeep.size();i++){
            System.out.println("Level " + i + dagByDeep.get(i));
        }
    }

    // méthodes sur le DAG utilisées pour propager les bornes en résolution
    // formelle
    public int size() {
        return dagByDeep.size();
    }

    public List<Para> getLevel(int i) {
        return dagByDeep.get(i);
    }
}
