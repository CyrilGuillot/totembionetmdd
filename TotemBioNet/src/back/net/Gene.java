package back.net;

import java.math.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import back.jlist.*;
import back.jlogic.Var;

/**
 * @author Adrien Richard
 * @author helen pour noSnoussi et les mises à jour des intervalles
 *         après réduction de Hoare
 */


public class Gene extends Var implements ObjectWithStringID{

    //Niveau min et max du gene
    final int min;
    final int max;
    // si auto-régulation
    boolean selfRegulated;
    // si on utilise Snoussi pour les paramètres
    boolean snoussi;
    //Regulations du gene
    private OrderedArrayList<Reg> regs = new OrderedArrayList<>();
    //Liste dans g�nes intervenant dans les r�gulations
    List<Gene> inputGenes = new ArrayList<>();
    //Param�tres associ�s au g�ne
    private OrderedArrayList<Para> paras = new OrderedArrayList<>();

    //Pour Snoussi
    private DagByDeep dag;

    //Pour l'enumeration des parametrages
    private ParaEnumerator parameterization;

    public Gene(String name, int min, int max,boolean snoussi) {
        super(name);
        this.min = min;
        this.max = max;
        this.snoussi = snoussi;
    }

    // appelé une fois que les paramètres et leurs domaines ont été correctement
    // construits
    public void setParaEnumerator() {
        if (snoussi) {
            this.dag = new DagByDeep(paras);
            this.parameterization = new GeneSnoussiParaEnumerator(paras, dag,name);
        }
        else {
            this.parameterization = new GeneParaEnumeraor(paras,name);
            setIntervals();
            this.dag = null;
        }
    }

    /////////////////////////////////////////
    // méthodes pour énumérer les configurations possibles du gène
    //Premier niveau possible pour le gene
    public void firstLevel() {
        setLevel(min);//heritage
    }

    //Niveau suivant pour le gene s'il existe (true);
    //premier niveau sinon (false)
    public boolean nextLevel() {
        if (getLevel() == max) {//heritage
            setLevel(min);//heritage
            return false;
        }
        setLevel(getLevel() + 1);//heritage
        return true;
    }

    //Premiere configuration des genes entrant
    private void firstConfig() {
        for (int i = 0; i < inputGenes.size(); i++)
            inputGenes.get(i).firstLevel();
    }

    //Configuration suivante des g�nes entrant
    private boolean nextConfig() {
        for (int i = 0; i < inputGenes.size(); i++)
            if (inputGenes.get(i).nextLevel())
                return true;
        return false;
    }


    ///////////////////////////////////////////////////
    // méthodes de construction des paramètres associés au gène
    // qui peuvent être ressource pour ce gène
    ///////////////////////////////////////////////////

    //Construction de la liste de param�tres: (1) On construit les
    //param�tres possibles; (2) On calcule les intervalles possibles de
    //chaque param�tre; (3) On donne, par d�faut, le plus grand
    //domaine de variation possible pour les param�tres; (4) On trie
    //les param�tres par ordre alphab�tique et nombre de r�gulations
    //croissant
    public void setParas() {
        this.selfRegulated = inputGenes.contains(this);
        if (!snoussi && selfRegulated) {
            System.err.println("WARNING : variable " + name + " is self regulated");
            System.err.println("          Any multiplex M will be considered satisfying the Snoussi condition on " + name);
            System.err.println("          (i.e. K_" + name +":w <= K_" + name +":w:M)");
        }
        paras.clear();
        // liste toutes les ressources possible et les ajoute dans le nom du paramètre
        enumerateRessources();
        // complète les trous si auto-régulation
        completeIntervalForSelfReg();
        //On donne une valeur minimale et maximale pour chaque param�tre
        for (int i = 0; i < paras.size(); i++)
            paras.get(i).setMinMax(min, max);
        // les ressources sont triées compatible Snoussi
        sortRessources();
    }

    //Enumeration des configurations possibles pour les gènes entrant
    // on ajoute le régulation qui ont au moins un config qui satisfait le multiplexe
    private void enumerateRessources() {
        firstConfig();
        do {
            //On fait la liste des r�gulations pr�sentes
            OrderedArrayList<Reg> paraRegs = new OrderedArrayList<Reg>();
            for (Reg r : regs) {
                if (r.isEffective()) {
                    paraRegs.addAlpha(r);
                }
            }
            //On construit le nom du param�tre
            String paraNuSMVName = "K" + name;
            String paraSMBName = "K" + "_" + name;
            for (int i = 0; i < paraRegs.size(); i++) {
                paraNuSMVName += "_" + paraRegs.get(i);
                paraSMBName += ":" + paraRegs.get(i);
            }
            //On regarde si le param�tre existe d�j�
            Para p = paras.getWithStringId(paraNuSMVName);

            if (p == null) {
                //Cr�ation du param�tre
                p = new Para(paraNuSMVName, paraSMBName, paraRegs);
                //psf.setSMBname(paraName);
                //Ajout dans la liste des param�tres
                paras.addAlpha(p);
                //Si le g�ne ne s'autor�gule pas, tous les niveaux du
                //g�ne sont compatibles avec le param�tre
                if (!selfRegulated)
                    for (int i = min; i <= max; i++)
                        p.intervals.add(new Interval(i));
            }
            p.nbTableRessource++;
            //Dans le cas o� le g�ne s'autor�gule, cr�ation d'un
            //intervalle singleton = le param�tre est compatible avec
            //le niveau actuel du g�ne
            if (selfRegulated) {
                int position;
                for (position = 0; position < p.intervals.size(); position++)
                    if (getLevel() <= p.intervals.get(position).min) {
                        if (getLevel() == p.intervals.get(position).min)
                            position = -1;
                        break;
                    }
                if (position >= 0)
                    p.intervals.add(position, new Interval(getLevel()));
            }
        } while (nextConfig());
    }

    //Si le g�ne s'autor�gule, on compl�te les intervalles
    //possibles pour chaque param�tre = les intervalles compris
    //entre les niveaux du g�ne compatible avec le param�tre
    private void completeIntervalForSelfReg() {
        if (selfRegulated)
            for (int i = 0; i < paras.size(); i++) {
                List<Interval> intervals = paras.get(i).intervals;
                //Intervalles interm�diaires
                for (int j = 0; j < intervals.size() - 1; j++)
                    if (intervals.get(j).max + 1 <= intervals.get(j + 1).min - 1) {
                        intervals.add(j + 1, new Interval(intervals.get(j).max + 1, intervals.get(j + 1).min - 1));
                        j++;
                    }
                //Plus petit intervalle
                if (min < intervals.get(0).min)
                    intervals.add(0, new Interval(min, intervals.get(0).min - 1));
                //Plus grand intervalle
                if (intervals.get(intervals.size() - 1).max < max)
                    intervals.add(intervals.size(),
                            new Interval(intervals.get(intervals.size() - 1).max + 1, max));
            }
    }

    //On trie les param�tres: si le nombre de r�gulateurs pour le
    //param�tre psf est < au nombre de r�gulateurs pour psf', alors psf
    //est devant psf' dans la liste
    private void sortRessources() {
        boolean permut = true;
        while (permut) {
            permut = false;
            for (int i = 0; i < paras.size(); i++)
                for (int j = i + 1; j < paras.size(); j++)
                    if (paras.get(j).regs.size() < paras.get(i).regs.size()) {
                        //permutation
                        Para p = paras.get(i);
                        paras.set(i, paras.get(j));
                        paras.set(j, p);
                        permut = true;
                    }
        }
    }

    ////////////////////////////////////////////////////
    // méthodes sur les paramétrisations possibles pour le gène
    ///////////////////////////////////////////////////

    public void firstParameterization() {
        parameterization.firstParameterization();
    }

    public boolean nextParameterization() {
        return parameterization.nextParameterization();
    }

    //Nombre de paramétrages
    public BigInteger nbParameterizations() {
        BigInteger nb = BigInteger.ONE;
        parameterization.firstParameterization();
        while (parameterization.nextParameterization())
            nb = nb.add(BigInteger.ONE);
        return nb;
    }

    // met à jour les intervalles avec les valeurs trouvées
    // dans le bloc PARAS ou après réduction de Hoare
    public void setIntervals()  {
        for (int i = 0; i < paras.size(); i++) {
            paras.get(i).updateMinIntMaxInt();
        }
    }

    // met à jour le DAG si les bornes ont été réduites par Hoare
    public void updateSnoussiCondition() {

        // si ça a bougé
        for (Para p : paras) {
            p.updateMinIntMaxInt();
        }

        // mise à jour des intervalles pour respecter Snoussi
        for (int i = 0; i < paras.size(); i++) {
            Para p1 = paras.get(i);
            for (Para p2 : p1.succs) {
                //R�duction de l'intervalle max de p1
                p1.setMaxIntWeakLessThan(p2.maxInt());
                //Augmentation de l'intervalle min de p2
                p2.setMinIntWeakGreaterThan(p1.minInt());
            }
        }
    }

    // pour afficher les contraintes de Snoussi entre paramètres
    public void printDagConstraints() throws IOException {
        if (snoussi)
            dag.printDagConstraints();
    }

    public void printDag() {
        if (snoussi)
            dag.print();
    }

    // méthodes sur le DAG utilisées pour propager les bornes en résolution
    // formelle
    public int dagSize() {
        return dag.size();
    }

    public List<Para> getDagLevel(int i) {
        return dag.getLevel(i);
    }


    /////////////////////////////////////////////
    // méthode pour la simulation
    //retourne le param�tre vers lequel le g�ne �volue
    public Para getFocalPara() {
        for (int i = paras.size() - 1; i >= 0; i--) {
            Para p = paras.get(i);
            for (int j = 0; j < p.regs.size(); j++)
                if (!p.regs.get(j).isEffective()) {
                    p = null;
                    break;
                }
            if (p != null)
                return p;
        }
        return null;
    }


    ///////////////////////////////////////////
    // accesseurs

    public int min() {
        return  min;
    }

    public int max() {
        return  max;
    }

    public boolean isSnoussi() {return snoussi;}

    public boolean selfRegulated() {return selfRegulated;}

    public OrderedArrayList<Para> paras() {
        return paras;
    }

    public Para para(int i) {return paras.get(i);}

    public int paraSize() {return paras.size();}


    //////////////////////////////////////////////
    // affichage


    // pour afficher la liste des paras en format SMB
    public String paraListSMBString() {
        if (paras.isEmpty())
            return "[]";
        String res= "[";
        int i=0;
        while (i<paras.size()-1) {
            res+=paras.get(i).getSMBname()+",";
            i++;
        }
        res+=paras.get(i).getSMBname()+"]";
        return res;
    }

    // pour afficher tous les paramètres
    public String paraSMBString(int verbose) {
        String s=(verbose>1)?"# domains\n":"";
        for (Para p : paras) {
            s += p.getSMBname() + " = " + p.minLevel();
            if (p.minLevel() < p.maxLevel())
                s+=".." + p.maxLevel();
            s+=";";
            if (verbose >=1)
                s+="   # " + p.allIntervalsToString();
            s+="\n";
        }
        return s;
    }

    @Override
    public String stringId() {
        return name;
    }

    public OrderedArrayList<Reg> getRegs() {
        return regs;
    }

    public OrderedArrayList<Para> getParas() {
        return paras;
    }
}
