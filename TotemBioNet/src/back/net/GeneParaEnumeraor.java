package back.net;

import back.jlist.OrderedArrayList;

/**
 * @author Hélène Collavizza
 */
public class GeneParaEnumeraor implements ParaEnumerator {

    private OrderedArrayList<Para> paras;
    private String geneName;


    public GeneParaEnumeraor(OrderedArrayList<Para> paras, String name){
        geneName=name;
        this.paras=paras;
    }

    //ENUMERATION DES PARAMETRAGES SANS SNOUSSI
    ///////////////////////////////////////////
    public void firstParameterization() {
        //On minimise les intervalles courants de tous les param�tres
        for (int i = 0; i < paras.size(); i++) {
            paras.get(i).firstCurrentInt();
            paras.get(i).setCurrentMaxToMax();
        }
    }

    // renvoie la prochaine paramétrisation non Snoussi
    public boolean nextParameterization() {
        for (int i=0;i<paras.size();i++) {
            if (paras.get(i).nextCurrentInt())
                return true;
        }
        return false;
    }

    public String toString() {
        return "NO Snoussi parameterizator for " + geneName;
    }
}
