package back.net;

import java.math.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import back.hoare.HoareFormulas;
import back.hoare.RunHoare;
import back.jlist.*;

import back.jlogic.Formula;
import back.jlogic.Var;

import front.smbParser.VarType;
import front.smbParser.Visitor;

import util.Out;


/**
 * @author Adrien Richard
 * @author Hélène Collavizza pour le traitement de:
 * - ENVAR
 * - FAIRCTL
 * - CTL
 * - INVAR
 * - Hoare
 * - construction du réseau via le SMBParser
 * @author Sokhna Ndeye pour le traitement des visiteurs Hoare
 */


public class Net  {

    // nb de paramétrisations
    public BigInteger NB_PARAMETERIZATIONS;

    // les gènes
    private OrderedArrayList<Gene> genes;

    // indique si on a pu traiter les contraintes de Hoare
    // de façon statique ou si au contraire il faut le utiliser
    // pour filtrer les modèles lors de l'énumération
    private boolean needDynamicHoare;
    // boutons blancs = variables d'environnement
    private ArrayList<Var> envVar;
    // on a une liste de spécification CTL
    private List<Formula> ctl;
    // on a une liste de spécification fair CTL
    // celles-ci seront traduites en fair bio selon l'algo d'Adrien
    private List<Formula> fairCtl;
    // listes de HOARE constraints
    private HoareFormulas hoare;

    //CONSTRUCTION DU RESEAU

    // si checkPara est true, on se contente de lire le
    // fichier d'entrée pour pouvoir afficher les paramètres possibles
    public Net(Visitor visitor, String pathName, String inputFile, boolean checkPara) throws Exception {
        genes = new OrderedArrayList<Gene>();
        // Lecture des blocks VAR et ENV_VAR
        // doit être fait avant la création des REG car les VAR et ENV_VAR
        // interviennent dans les multiplexes
        envVar = new ArrayList<Var>();
        for (front.smbParser.Var va : visitor.getVarBlock().getData()) {
            Gene vb = va.toBack();
            if (va.getVarType() == VarType.ENV_VAR) {
                envVar.add(vb);
            }
            genes.add(vb);
        }
        // bloc des régulations
        for (front.smbParser.Regul r : visitor.getRegBlock().getRegs()) {
            Reg br = r.toBack();
            for (String targetId : r.getTargets()) {
                Gene targetGene = genes.getWithStringId(visitor.getVarBlock().getVarWithId(targetId).getId());
                targetGene.getRegs().add(br);
            }
        }

        //Construction du r�seau
        buildGraph();

        // création des paramètres associés aux gènes
        for (Gene g : genes) {
            if (!isEnvVar(g)) {
                g.setParas();
            }
        }

        if (!checkPara) {
            // information du bloc PARA
            buildParas(visitor);

            // on construit l'énumérateur sur les valeurs de paramètres
            for (Gene g : genes) {
                g.setParaEnumerator();
//                System.out.println("Parameters for " + g + ": " + g.paras);
//                for (Para p : g.paras) {
//                    System.out.println(p + " SUCCS " + p.succs);
//                }
            }

            NB_PARAMETERIZATIONS =nbParameterizations();

            // initialisation du bloc Hoare s'il existe
            if (visitor.hasHoareBlock()) {
                initHoare(visitor, pathName, inputFile);
            }

            // lecture des propriétés
            List<Formula>[] props = visitor.getCtlBlock().getBackFormat();
            ctl = props[0];
            fairCtl = props[1];
        }
    }


    // Lecture des contraintes sur les paramétres
    // et modification des bornes
    private void buildParas(Visitor visitor){
        // pour tous les Param rencontrés dans le bloc PARA
        for (front.smbParser.Param p : visitor.getParaBlock().getParas()) {
            Para pb = null;
            // pour tous les gènes
            for (Gene g : genes) {
                // si p est un para de g, on met à jour ses bornes
                pb = g.getParas().getWithStringId(p.getFinalid());
                if (pb != null) {
                    pb.setMinMax(p.getValMin(), p.getValMax());
                    //pb.setSMBname(psf.getSMBid());
                    if (p.getValMin() == p.getValMax())
                        pb.fixe = true;
                    break;
                }
            }
            // il est possible que ce paramètre n'existe pas
            //     - il est ok d'un point de vue SMBParser
            //     - mais d'un point de vue dynamique, aucun état ne permet
            //       d'obtenir cette liste de ressources
            if (pb == null) {
                String message = "Unknown parameter " + p.getSMBid() + "\n\n";
                message += "  Use -paras option to know effective parameters";
                System.err.println(message);
            }
        }
    }

    /** 1. lance la création des triplets de Hoare (.ml)
     * 2. lance ocaml sur les ml générés et écrit le résultat dans .out
     * 3. transforme le résultat en json et le lit en formula
    */
    private void initHoare(Visitor visitor, String path, String inputFile) throws Exception {
        System.out.println("******** HOARE processing ********");
        System.out.println("* Building Hoare formulas...");
        // generate the .ml files
        RunHoare rh = new RunHoare(visitor, path, inputFile);
        // is everything is ok
        if (rh.canBeRun()) {
            // run the ml files
            hoare = rh.runHoare();
            // on sait travailler seulement si la wp contient
            // uniquement des variables paramètres
            if (!hoare.isEmpty() & hoare.isOnlyPara) {
                // si c'est une conjonction de comparateurs on réduit les bornes
                if (hoare.isOnlyConjunct) {
                    hoare.propagateHoareConstraints(this);
                    // les DAG de Snoussi doit être mis à jour car
                    // les domains ont pu être réduits
                    for (Gene g : genes)
                        g.updateSnoussiCondition();
                    BigInteger nbp= nbParameterizations();
                    hoare.setNbRemovedParam(NB_PARAMETERIZATIONS.subtract(nbp));
                    NB_PARAMETERIZATIONS=nbp;
                }
                // sinon, il faut vérifier les conditions de hoare
                // pour chaque modèle
                else {
                    needDynamicHoare = true;
                }
            }
        }
        System.out.println("******** HOARE end ********");
    }

    //(1) Pour chaque g�ne, on construit la liste des g�nes qui
    //interviennent dans ses r�gulations. (2) On remplace les
    //variables qui interviennent dans la formule de chaque régulation
    //par les génes du réseau.
    private void buildGraph() throws Exception {
        //CONSTRUCTION DES LIENS GENE -> GENE
        for (Gene g : genes) {
            g.inputGenes.clear();
            for (int j = 0; j < g.getRegs().size(); j++) {
                //Insertions des gènes du réseau dans la formule de r
                for (Gene gk : genes) {
                    //Si l'insertion du gene k a lieu, on l'ajoute
                    //dans la liste des genes entrant du gene i
                    if (g.getRegs().get(j).formula.insert(gk))
                        if (!g.inputGenes.contains(gk))
                            g.inputGenes.add(gk);
                }
            }
        }
    }

    //ENUMERATION DES PARAMETRAGES
    //////////////////////////////////////

    public void firstParameterization() {
        for (Gene g : genes)
            g.firstParameterization();
        //printAllCurrentIntervals();
    }

    public boolean nextParameterization() {
        for (Gene g : genes)
            if (g.nextParameterization()) {
                //printAllCurrentIntervals();
                return true;
            }
        return false;
    }


    // NOMBRE DE PARAMETRAGES TOTAL DU RESEAU
    public BigInteger nbParameterizations() {
        BigInteger nb = BigInteger.ONE;
        for (Gene g : genes) {
            BigInteger n = g.nbParameterizations();
            nb = nb.multiply(n);
        }
        return nb;
    }

    // nombre de variables param�tres
    public int nbParams() {
        int n = 0;
        for (Gene g : genes) {
            n += g.getParas().size();
        }
        return n;
    }

    //////////////////////////////////////
    // méthode utilisée pour générer le graphe d'états

    // construction valeur de l'état courant
    public ArrayList<Long> currentState() {
        ArrayList<Long> cs = new ArrayList<Long>();
        for (Gene g : genes) {
            if (!isEnvVar(g)) {
                cs.add((long)g.level);
            }
        }
        return cs;
    }

    // affichage du nom des gènes pour connaître l'ordre d'apparition de leurs
    // valeurs dans l'état
    public String geneList(){
        String s="";
        for (Gene g : genes) {
            if (!isEnvVar(g)) {
                s+=g.name+" ";
            }
        }
        return s;
    }

    //AFFICHAGE
    //////////////////////////////////////////////////

    public void print() throws Exception {
        int verbose = Out.verb();

        //GENES
        Out.psfln("\nVAR\n");
        BigInteger nbStates = BigInteger.ONE;
        for (Gene g : genes) {
            String gene = g + " = "+ g.min();
            if (isEnvVar(g)) {
                gene += " (ENV_VAR)";
            }
            else {
                gene += " " + g.max();
                if (!g.isSnoussi())
                    gene += " (NS)";
                if (verbose > 1 && g.selfRegulated)
                    gene += " (self reg)";
            }
            Out.psfln( gene + " ;");
            nbStates = nbStates.multiply(BigInteger.valueOf(g.max() - g.min() + 1));
        }

//        if (verbose > 0)
//            Out.psfln("\n# Number of states = " + nbStates+"\n");

        //REGULATIONS
        Out.psfln("\nREG\n");
        printRegulations();

        //PARAMETRES
        Out.psfln("\nPARA\n");
        BigInteger nb = BigInteger.ONE;
        for (Gene g : genes) {
            if (!isEnvVar(g)) {
                //Affichage des param�tres
                Out.psfln("# Parameters for " + g + "\n");
                Out.psf(g.paraSMBString(verbose));
                //dag
                if (verbose > 0)
                    g.printDagConstraints();
                Out.psfln();

            }
        }

        //FORMULES
        printAllSpec();

        // Hoare
        if (hoare!=null)
            hoare.writeHoareInfos();

        Out.psfln("\n======================");
        Out.psfln("# Number of effective parameterizations : " + NB_PARAMETERIZATIONS);
        Out.psfln("======================" + "\n\n");

        ;
    }

    // on a besoin de parcourir de façon globale toutes les régulations
    // et de voir si un des gène est cible de la régulation
    private void printRegulations() throws IOException{
        //Calcul de la liste de r�gulation
        OrderedArrayList<Reg> regs = new OrderedArrayList<Reg>();
        for (Gene g : genes)
            for (int j = 0; j < g.getRegs().size(); j++)
                if (!regs.contains(g.getRegs().get(j)))
                    regs.add(g.getRegs().get(j));
        for (int i = 0; i < regs.size(); i++) {
            Out.psf(regs.get(i).toSMBString());
            for (Gene g : genes)
                if (g.getRegs().contains(regs.get(i)))
                    Out.psf(" " + g);
            Out.psfln(" ;");
        }
    }

    private void printAllSpec() throws Exception {
        printSpec("CTL", ctl);
        printSpec("FAIRCTL", fairCtl);
        //printSpec("HOARE", back.hoare);
    }

    private void printSpec(String mess, List<Formula> spec) throws Exception {
        if (spec.size() == 0) {
            Out.psfln("NO " + mess + " FORMULA");
        } else {
            Out.psfln(mess);
            Out.psf("[");
            for (Formula f : spec) {
                if ("FAIRCTL".equals(mess))
                    Out.psfln(f.toString() + ",");
                else if ("HOARE".equals(mess))
                    Out.psfln(this.getHoare() + ",");
                else
                    Out.psfln(f.toString() + ",");
            }
            Out.psfln("]");
        }
    }

    // renvoie la liste des paramètres possible pour chaque gène
    public void printParameterSet() {
        String res = "Set of effective parameters :\n";
        for (Gene g : genes) {
            if (!isEnvVar(g))
                res += "   " + g + " : " + g.paraListSMBString() + "\n";
        }
        System.out.println(res);
    }


    //Affichage du param�trage courant

    public void printCurrentParameterization() {
        for (Gene g : genes) {
            OrderedArrayList<Para> p = g.getParas();
            for (int j = 0; j < p.size(); j++) {
                System.out.print(p.get(j) + " = " + p.get(j).currentMin());
                if (p.get(j).currentMin() < p.get(j).currentInt().max)
                    System.out.print(" " + p.get(j).currentInt().max);
                System.out.print("  ");
            }
            System.out.println();
        }
    }


    //Code un indice pour le param�trage courant qui ne d�pend que du
    //r�seau.

    public BigInteger idCurrentParameterization() {
        BigInteger base = BigInteger.ONE;
        BigInteger id = BigInteger.ZERO;
        BigInteger min, max, level;
        for (Gene g : genes) {
            for (int j = 0; j < g.getParas().size(); j++) {
                min = BigInteger.valueOf((long) g.min());
                max = BigInteger.valueOf((long) g.max());
                level = BigInteger.valueOf((long) g.getParas().get(j).currentMin());
                id = id.add((level.subtract(min)).multiply(base));
                base = base.multiply((max.subtract(min)).add(BigInteger.ONE));
            }
        }
        return id;
    }



    // pour afficher toutes les parametrisations
    // utilisée pour énumérer les modèles
    public void printAllParametrizationValues() {
        int nbParam = 1;
        firstParameterization();
        System.out.println("#" + nbParam + "------------");
        printCurrentParameterization();
        while (nextParameterization()) {
            nbParam++;
            System.out.println("#" + nbParam + "------------");
            printCurrentParameterization();
        }
        System.out.println("existing parameterizations " + nbParam);
    }

    // trace pour afficher tous les intervalles des parametres
    public void printAllDetailedIntervals() {
        System.out.println("");
        for (Gene g : genes) {
            System.out.print("gene " + g + " : ");
            for (Para p : g.getParas()) {
                System.out.print(p + " " + p.intervals + " ");
                System.out.println(p.minLevel() + " .. " + p.maxLevel());
                System.out.println("curr " + p.currentInt());
                System.out.println("int min et max  " + p.minInt() + " " + p.maxInt());
            }
            System.out.println("");
        }
    }

    // pour debug
    public void printAllIntervals() {
        for (Gene g : genes) {
            for (int j = 0; j< g.getParas().size(); j++) {
                System.out.println(g.getParas().get(j).intervalsToString());
            }
        }
    }

    /////////////////////////////////////////////////
    // pour l'option -paras
    /////////////////////////////////////////////////

    // calcul des paramètres impossibles
    public void printForbiddenParas() {
        System.out.println("######################");
        System.out.println("Set of NON effective parameters :");
        for (Gene g : genes) {
            if (!isEnvVar(g))
                System.out.println("  " + g + " : " + forbiddenParasForGene(g));
        }
    }

    // @author Hélène Collavizza
    public ArrayList<String> forbiddenParasForGene(Gene g) {
        // on créé la liste des noms de régulation
        ArrayList<String> regNames = new ArrayList<String>();
        for (Reg r : g.getRegs()) {
            regNames.add(r.name);
        }
        // on calcule toutes les possibilités à 1, 2, ... regs.size régulations
        ArrayList<String> forb = new ArrayList<String>();
        ArrayList<HashSet<String>> all = allPara(g.getRegs().size(), regNames);

        // on créé le nom du paramètre en le classant par ordre alpha
        forb.add("K_" + g.name);
        for (HashSet<String> h : all) {
            String name = "K_" + g.name;
            ArrayList<String> ordered =new ArrayList<>();
            ordered.addAll(h);
            Collections.sort(ordered);
//            for (String s : h)
//                ordered.addAlpha(s);
            for (String s : ordered) {
                name += ":" + s;
            }
            forb.add(name);
        }
        // on supprime les paramètres effectifs
        for (Para p : g.getParas()) {
            if (forb.contains(p.getSMBname()))
                forb.remove(p.getSMBname());
        }
        return forb;
    }

    // @author Hélène Collavizza
    // fonction récursive qui permet de générer tous les ensembles de taille n
    // à partir des String contenus dans l
    private ArrayList<HashSet<String>> allPara(int n, ArrayList<String> l) {
        if (n == 0) {
            ArrayList<HashSet<String>> res = new ArrayList<HashSet<String>>();
            for (String s : l) {
                HashSet<String> hs = new HashSet<String>();
                hs.add(s);
                res.add(hs);
            }
            return res;
        }

        ArrayList<HashSet<String>> r = allPara(n - 1, l);
        ArrayList<HashSet<String>> cr = (ArrayList<HashSet<String>>) r.clone();
        for (String x : l) {
            for (HashSet<String> y : r) {
                if (!y.contains(x)) {
                    HashSet<String> cy = (HashSet<String>) y.clone();
                    cy.add(x);
                    if (!cr.contains(cy))
                        cr.add(cy);
                }
            }
        }
        return cr;
    }

    ///////////////////////////////////////////////////////////
    // pour la version formelle
    ///////////////////////////////////


    // FORMAL : pour remettre les niveaux formels à l'intervalle d'origine
    public void resetFormalLevels() {
        for (Gene g : genes) {
            ArrayList<Para> paras = g.getParas();
            for (Para p : paras) {
                p.resetFormalLevels();
            }
        }
    }

    // pour avoir les contraintes de Snoussi sur le nom des paramètres
    public void printParametersConstraints() throws IOException {
        for (Gene g : genes)
            g.printDagConstraints();
    }


    // accesseurs
    ///////////////////////////////////////////////////////////

    public boolean needDynamicHoare() {
        return needDynamicHoare;
    }

    public List<Formula> getCtl() {
        return ctl;
    }

    public List<Formula> getFairCtl() {
        return fairCtl;
    }

    public HoareFormulas getHoare() {
        return hoare;
    }

    public ArrayList<Var> getEnvVar() {
        return envVar;
    }

    public boolean isEnvVar(Gene v) {
        return envVar.contains(v);
    }

    // pour connaitre le nombre réel de gènes
    public int nbOfGene() {
        return genes.size() - envVar.size();
    }

    public OrderedArrayList<Gene> genes() {
        return genes;
    }

    public boolean hasTemporalLogicProp() {
        return !(fairCtl.isEmpty()&&ctl.isEmpty());
    }

    public OrderedArrayList<Gene> getGenes() {
        return genes;
    }
}
