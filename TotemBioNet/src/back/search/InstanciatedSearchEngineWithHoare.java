package back.search;

/**
 * This class is used to search for valid models.
 * <psf>
 * All models are enumerated but only models that validate Hoare constraints are
 * generated and checked with NuSMV
 *
 * @author Hélène Collavizza
 */

import back.hoare.HoareFormulas;
import back.jclock.Clock;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import back.run.NuSMV;
import util.Out;
import util.StdoutReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;


public class InstanciatedSearchEngineWithHoare extends AbstractSearchEngine {

    // nb modèles résolus par NuSMV
    static long nbSolvedModel = 0;

    // nb modèles non compatibles avec les contraintes de Hoare
    static long nbHoareRemovedModel = 0;

    // les formules de Hoare à valider
    private HoareFormulas hoare;

    public InstanciatedSearchEngineWithHoare(Net n, String nuSMVcommand) {
        super(n, nuSMVcommand);
        hoare = net.getHoare();
    }

    /////////////////////////////////////////////
    // gestion de la recherche

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String h = " [#Hoare Removed Models : " + nbHoareRemovedModel + "] ";
        String s2 = "[#Selected  Models : " + nbSelected + "] ";
        return s1 + h + s2;
    }

    ///////////////////////////////
    // méthodes de recherche de l'interface Search
    public void initSearch(String name) throws Exception {
        // on calcule le 1er modèle
        super.net.firstParameterization();
    }

    public boolean hasNext() {
        return super.net.nextParameterization();
    }

    // valeur actuelle de chaque paramètre
    private HashMap<String, Integer> currentModelValue() {
        HashMap<String, Integer> model = new HashMap<String, Integer>();
        for (Gene g : net.genes()) {
            for (Para p : g.paras()) {
                model.put(p.getSMBname(), p.currentMin());
            }
        }
        return model;
    }

    // si l'état courant est OK pour Hoare, on vérifie ce modèle sinon on ne fait rien
    // => on passe à l'état suivant
    public void doSearch(String name, boolean csv) throws Exception {
        HashMap<String, Integer> model = currentModelValue();
        //System.out.println(model);
        if (hoare.valid(model))
            search(name, csv);
        else {
            if (Out.verb()>0)
                Out.psfln("Hoare removed model: " + model);
            nbHoareRemovedModel++;
        }
    }

    // resoud un modèle s'il y en a un
    public void search(String name, boolean csv) throws Exception {
        Process proc;

        NuSMV.writeNet(net, name, null, csv);

        //System.out.println(".smv file has been generated, launching NuSMV");
        //EXECUTION DE NUSMV
        //proc = Runtime.getRuntime().exec(nuSMVcommand);
        proc = super.nuSMVCall(name);
        nbSolvedModel++;
        Iterator<String> iter = StdoutReader.getIteratorStream(proc);
        //StdoutReader.readNuSMVHeader(iter, NuSMV.HEADER_SIZE);
        String line = StdoutReader.readNuSMVHeader(iter);
        // si ça s'arrête là, il y a un problème d'execution et on affiche le
        //message d'erreur de NuSMV
        if (!line.startsWith("-- specification")&&!iter.hasNext()) {
            StdoutReader.readNuSMVError(proc);
        } else {
            byte[] model = ParasAsArray.currentModelAsArray();
            super.analyseModel(line,iter, model);
        }
        proc.destroy();
    }


    ////////////////////
    private void writeStats(Clock c) throws Exception {
        Out.psfln();
        Out.pfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Number of models removed from Hoare triple " + nbHoareRemovedModel);
        Out.psfln("# Number of checked models : " + (super.net.NB_PARAMETERIZATIONS.subtract(BigInteger.valueOf(nbHoareRemovedModel))));
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);
        Out.psfln("################################################\n");
    }

    // sauvegarde des résultats
    public void writeSearch(Clock c) throws Exception {
        //System.out.println("Order of parameters when printing");
        //System.out.println(ParasAsArray.printGeneParam());
        writeStats(c);
        //Out.pfln("Order of parameters when printing");
        //Out.pfln(ParasAsArray.printGeneParam());
        //Out.pfln("################################################\n");
        for (int i=0;i<super.OKModels.size();i++) {
            ParasAsArray.writeDetailedModel(super.OKModels.get(i), true, i);
            Out.pfln("");
        }
        Out.pfln("\n########## KO models #########\n");
        for (int i=0;i<super.KOModels.size();i++) {
            ParasAsArray.writeDetailedModel(super.KOModels.get(i), false,i);
            if (Out.verb()>0)
                Out.pf("   Error explanation: " + super.errorExplanation.get(i).toString());
            Out.pfln("");
        }

    }

    // sauvegarde des résultats
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        //System.out.println("Order of parameters when printing");
        //System.out.println(ParasAsArray.printGeneParam());
        writeStats(c);
        String name = file + ".csv";
        Out.psfln("Models have been written in " + name);
        Out.psfln();

        BufferedWriter w = new BufferedWriter(new FileWriter(new File(name)));
        w.write(super.getCSVHead());
        int n = 1;
        for (byte[] b : super.OKModels)
            w.write(ParasAsArray.getCSV(n++, b, true)+"\n");
        //w.write(separationLine(back.net.nbParams()));
        int i= 0;
        while (i< super.KOModels.size()) {
            w.write(ParasAsArray.getCSV(n++, super.KOModels.get(i), false));
            // écriture de l'erreur
            w.write("\""+super.errorExplanation.get(i).toString()+"\"\n");
            i++;
        }
        // fermeture du fichier
        w.close();
    }

}
