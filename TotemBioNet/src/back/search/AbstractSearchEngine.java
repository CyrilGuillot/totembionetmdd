package back.search;

/**
 * This class is used to search for valid models, either by using partly-intanciated patterns
 * or constant models.
 * Patterns are used to negate the CTL property to refute many intanciated models.
 *
 * @author Hélène Collavizza
 */

import back.net.Gene;
import back.net.Net;
import back.net.Para;
import util.Out;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


public abstract class AbstractSearchEngine implements Search {
    // le réseau qu'on est en train de traiter
    protected Net net;

    // la commande NuSMV
    protected String nuSMVcommand;

    // nb de modèles OK
    public static int nbSelected;
    // nb de modèles KO
    public static int nbKO;

    // sauvegarde des modèles ou pattern résolus
    protected ArrayList<byte[]> OKModels;
    protected ArrayList<byte[]> KOModels;
    // explication des modèles KO
    protected ArrayList<ArrayList<String>> errorExplanation;


    public AbstractSearchEngine(Net n, String nuSMVcommand) {
        ParasAsArray.init(n);
        this.net = n;
        OKModels = new ArrayList<byte[]>();
        KOModels = new ArrayList<byte[]>();
        errorExplanation = new ArrayList<ArrayList<String>>();
        this.nuSMVcommand = nuSMVcommand;
    }

    /////////////////////////////////////////////
    // gestion de la recherche


    protected Process nuSMVCall(String name) throws IOException {
        Process proc = Runtime.getRuntime().exec(nuSMVcommand + " " + name + ".smv");
        return proc;
    }

    protected boolean falseSpecification(String line) {
        if (line.startsWith("-- specification")) {
            return line.endsWith("is false");
        }
        return false;
    }

    private boolean specificationIsFalse(String line) {
        return line.endsWith("is false");
    }

    private boolean isSpecification(String line) {
        return line.startsWith("-- specification");
    }

    // iter est le stream résultat d'appel de NuSMV
    // analyse ce stream pour gérer les modèles OK ou KO
    protected void analyseModel(String l,Iterator<String> iter, byte[] model) throws Exception {

        int specNumber = 0;
        boolean ok = true;

        // l is the last line
        if (isSpecification(l)) {
            specNumber++;
            if (specificationIsFalse(l)) {
                ok = false;
            }
        }
        String line = "";
        // on part à la recherche d'une spécification fausse
        // dans le cas où il y a plusieurs formules CTL
        while (ok && iter.hasNext()) {
            line = iter.next();
            if (isSpecification(line)) {
                specNumber++;
                if (specificationIsFalse(line))
                    ok = false;
            }
        }
        saveModel(model, ok);
        // le modèle est KO, on explique pourquoi
        // ATTENTION : tout repose sur le fait que NuSMV traite les formules dans
        //             le même ordre que le stockage dans les listes
        //             ctl et fairctl de Net
        if (!ok) {
            newExplanation(nbKO-1, specNumber-1);
            while (iter.hasNext()) {
                line = iter.next();
                if (isSpecification(line)) {
                    specNumber++;
                    if (specificationIsFalse(line)) {
                        addExplanation(nbKO-1, specNumber-1);
                    }
                }
            }
        }
    }
    // créé une explication d'erreur pour le modèle courant et la spec courante
    private void newExplanation(int modelNumber, int specNumber) {
        errorExplanation.add(modelNumber, new ArrayList<String>());
        addExplanation( modelNumber, specNumber);
    }

    // ajoute une explication pour un modèle KO
    private void addExplanation(int modelNumber, int specNumber){
        int nbCtl = net.getCtl().size();
        String spec;
        if (specNumber<nbCtl)
            spec = net.getCtl().get(specNumber).toExplanationString();
        else
            spec = net.getFairCtl().get(specNumber-nbCtl).toExplanationString();
        errorExplanation.get(modelNumber).add(spec);
    }

    // ajoute un modèle aux listes de modèles déjà résolus
    protected void saveModel(byte[] model, boolean ok) {
        if (ok) {
            OKModels.add(model);
            nbSelected++;
        } else {
            KOModels.add(model);
            nbKO++;
        }
    }

    protected String separationLine(int n) {
        String line = "---;---;";
        for (int i = 0; i < n; i++)
            line += "---;";
        line += "\r\n";
        return line;
    }


    protected String getCSVHead() {
        // n° du modèle, OK ou KO
        String s = "n°;valid;";
        int i = 0; // n° du gène
        // paramètres
        for (Gene g : net.genes()) {
            if ((!net.isEnvVar(g))) {
                for (Para p : g.paras()) {
                    s += p.getSMBname() + ";";
                }
                // si csv et verbose 2 alors on met aussi les param non effectifs
                if (Out.verb()==2) {
                    ArrayList<String> forb = net.forbiddenParasForGene(g);
                    ParasAsArray.setNonEffective(i,forb.size());
                    for (String f : forb)
                        s += f + ";";
                }
            }
            i++;
        }
        // colonne d'explication d'erreur
        s += "Error explanation\n";
        return s;
    }

}
