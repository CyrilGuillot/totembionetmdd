package back.search;

/**
 * This class is used to search for valid models, either by using partly-intanciated patterns
 * or constant models.
 * Patterns are used to negate the CTL property to refute many intanciated models.
 *
 * TODO : not yet used, no enough efficient
 * @author Hélène Collavizza
 */

import back.jclock.Clock;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import back.run.NuSMV;
import util.Out;
import util.StdoutReader;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class FormalSearchEngine extends AbstractSearchEngine {
    // le time-out pour la résolution du pattern en seconde
    private static int TIME_OUT = 120;

     static long nbTimeOut = 0; // nb de time_out

     static long nbNuSMVCall = 0; // nb appel à NuSMV

    // nb modèles résolus par NuSMV
    static long nbSolvedModel = 0;

    // sauvegarde des modèles ou pattern résolus
    private ArrayList<byte[]> counterExamples;
    private ArrayList<SolvedPattern> KOPatterns;


    // construction des modèles et pattern
    // besoin du modèle courant et du suivant à cause du firstParameterisation
    private byte[] currentModel;
    private byte[] nextModel;
    private byte[] currentPattern;

    // nb de modèles subsumés par un pattern résolu
    public static int nbSubsumed;
    // nb pattern solved (i.e. not_phi est vraie)
    public static int nbSolvedPattern;
    // nb de modèles OK
    public static int nbSelected;

    // vrai si le pattern à permis de montrer not_phi
    private boolean patternIsSolved;

    // pour s'arrêter proprement aussi bien avec la version pattern que sans
    private boolean isLast;


    public FormalSearchEngine(Net n, String nuSMVcommand) {
        super(n, nuSMVcommand);
        ParasAsArray.init(n);
        ParasAsArray.initFormalParas();
        System.out.println("formals " + ParasAsArray.formalParas);
        super.net.printAllDetailedIntervals();
        counterExamples = new ArrayList<byte[]>();
        KOPatterns = new ArrayList<SolvedPattern>();
    }


    /////////////////////////////////////////////
    // gestion de la recherche

    protected Process nuSMVCall(String name) throws IOException {
        nbNuSMVCall++;
        return super.nuSMVCall(name);
    }

    public void initSearch(String name) {
        // on calcule le 1er modèle
        super.net.firstParameterization();
        //***nextModel = ParasAsArray.currentModelAsArray();
        currentModel = ParasAsArray.currentModelAsArray();
        patternIsSolved = false;
        isLast = false;
    }

    // passe au pattern et au modèle suivant
    // renvoie vri s'il y a un pattern formel à tester
    public boolean nextPattern() {
        //***currentModel = nextModel;
        resetPatternSearchInfo();
        currentPattern = null;
        while (currentModel != null && isKnown(currentModel)) {
//            System.out.println("Subsumed model ");
//            try {
//                ParasAsArray.printModel(currentModel);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            currentModel = null;
            if (super.net.nextParameterization())
                currentModel = ParasAsArray.currentModelAsArray();
        }
        if (currentModel != null) {
            currentPattern = ParasAsArray.modelToPattern(currentModel);
//            if (notSolvedPattern.contains(currentPattern))
//                currentPattern = null;
        } else
            isLast = true;
		/*if (currentPattern!=null)
			System.out.println("pattern " + ParasAsArray.getParaValue(currentPattern) + " model " + ParasAsArray.getParaValue(currentModel));
		else
			System.out.println("pattern and model null" );*/
        return currentPattern != null;
    }


    // on résoud le modèle courant s'il existe et que le pattern précédent n'a pas été résolu
    public boolean currentModelMustBeSolved() {
        return currentModel != null && !patternIsSolved;
    }

    // renvoie vrai si l'on peut générer un nouveau modèle formel
    public boolean hasNext() {
        if (isLast)
            return false;
        boolean has = false;
        if (super.net.nextParameterization()) {
            has = true;
            currentModel = ParasAsArray.currentModelAsArray();
        }
        return has;
        //***return nextModel != null && !isLast;
    }

    // pour remettre les variables du réseau dans leur état initial
    // c'est à dire sans variables libres
    public void cleanSearch() {
        ParasAsArray.unFree();
    }


    public void doSearch(String name, boolean csv) throws Exception {
        doPatternSearch(name, csv);
        doModelSearch(name, csv);
    }


    // resoud un modèle s'il y en a un
    public void doModelSearch(String name, boolean csv) throws Exception {
        Process proc;
        if (currentModelMustBeSolved()) {
            //System.out.println("model");
            byte[] model = getCurrentModel();
//            System.out.println("solved model ");
//            ParasAsArray.printModel(model);
            NuSMV.writeNet(net, name, model, csv);
            //System.out.println(".smv file has been generated, launching NuSMV");
            //EXECUTION DE NUSMV
            proc = nuSMVCall(name);
            nbSolvedModel++;
            Iterator<String> iter = StdoutReader.getIteratorStream(proc);
            String line = StdoutReader.readNuSMVHeader(iter);
            //StdoutReader.readNuSMVHeader(iter, NuSMV.HEADER_SIZE);
            // si ça s'arrête là, il y a un problème d'execution et on affiche le
            //message d'erreur de NuSMV
            if (!iter.hasNext()) {
                StdoutReader.readNuSMVError(proc);
            } else
                super.analyseModel(line,iter, model);
            proc.destroy();
        }
    }

    // créé un pattern à partir du modèle courant si c'est possible
    public void doPatternSearch(String name, boolean csv) throws Exception {
        Process proc;
        if (nextPattern()) {
            byte[] model = getCurrentPattern();
//            System.out.println("solved pattern ");
//            ParasAsArray.printModel(model);
            //Écriture du fichier NuSMV
            NuSMV.writeNet(super.net, name, model, csv);
            //System.out.println(".smv file has been generated, launching NuSMV");
            //EXECUTION DE NUSMV
            proc = nuSMVCall(name);
            InputStream inputstream = proc.getInputStream();
            if (!proc.waitFor(TIME_OUT, TimeUnit.SECONDS)) {
                nbTimeOut++;
                System.out.println("!!!! timeout");
            } else {
                Iterator<String> iter = StdoutReader.getIteratorStream(proc);
                StdoutReader.readNuSMVHeader(iter);
                //StdoutReader.readNuSMVHeader(iter, NuSMV.HEADER_SIZE);
                // si ça s'arrête là, il y a un problème d'execution et on affiche le
                //message d'erreur de NuSMV
                if (!iter.hasNext()) {
                    StdoutReader.readNuSMVError(proc);
                } else
                    analysePattern(iter, model);
            }
            //}
            //cleanSearch();
            proc.destroy();
        }
    }


    ////////////////
    // accesseurs
    public byte[] getCurrentModel() {
        return currentModel;
    }

    public byte[] getCurrentPattern() {
        return currentPattern;
    }

    private void resetPatternSearchInfo() {
        patternIsSolved = false;
    }

    ////////////////////////////
    // informations sur les modèles

    // pour savoir si le modèle est inféré par un pattern
    // ou a déjà été résolu comme contre-exemple du pattern
    private boolean isKnown(byte[] model) {
        return exist(model, counterExamples) || isSolvedPattern(model);
    }

    // renvoie vrai si model matche un pattern déjà résolu
    private boolean isSolvedPattern(byte[] model) {
        for (SolvedPattern pat : KOPatterns) {
            if (pat.match(model)) {
                //KOModels.add(model);
                nbSubsumed++;
                return true;
            }
        }
        return false;
    }

    // renvoie vrai si model existe dans list
    // NOTA : recherche "à la main" pour ne pas passer par des objets et définir un equals
    private boolean exist(byte[] model, ArrayList<byte[]> list) {
        for (byte[] ba : list) {
            if (Arrays.equals(model, ba))
                return true;
        }
        return false;
    }


    ///////////////////////////////
    // analyse de la recherche


    // on part à la recherche d'une spécification fausse (donc not phi true)
    // dans le cas où il y a plusieurs formules CTL

    // TODO : utiliser un thread pour lire au fur et à mesure le flux de NuSMV
    //      => arrêt dès qu'une Phi est faussse
    // https://stackoverflow.com/questions/1732455/redirect-process-output-to-stdout
    private void analysePattern(Iterator<String> iter, byte[] model) throws Exception {
        String line = "";
        resetPatternSearchInfo();
        boolean ok = true;
        String allLine = "";
        // analyse non PHI
        do {
//            System.out.println(line);
            line = iter.next();
            allLine += line;
            if (super.falseSpecification(line)) {
//                System.out.println(line);
                ok = false;
            }
        } while (ok && iter.hasNext());
        if (ok) {
//
            patternIsSolved = true;
            //saveModel(model, false);
            savePattern(model);
            nbSolvedPattern++;
            //System.out.println("pattern is solved ");
        }
    }

    // le pattern courant est insatisfialbe, on le sauve
    private void savePattern(byte[] pat) {
        KOPatterns.add(new SolvedPattern(pat));
    }


    // renvoie un byte qui correspond au CE renvoyé par NuSMV
    private byte[] getCounterExample(Iterator<String> iter) {
        //System.out.println("build CE  ");
        HashMap<String, Byte> paraValue = counterExampleAsMap(iter);
        //System.out.println(paraValue);
        try {
            ParasAsArray.printModel(currentModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // construit le byte[] correspondant
        byte[] model = new byte[super.net.nbParams() + 1];
        // le CE contredit Snoussi => pas intéressant
        if (!snoussiIsVerified(paraValue)) {
            model[0] = -2;
            return model;
        }
        // le modèle est intéressant
        model[0] = 0;
        int k = 1;
        for (int i = 0; i < super.net.nbOfGene(); i++) {
            Gene g = super.net.genes().get(i);
            for (int j = 0; j < g.paraSize(); j++) {
                Para p = g.para(j);
                model[k] = paraValue.get(p.getNuSMVname());
                if (model[k] < currentModel[k])
                    System.out.println("AIIIIIIIIIIIIIII");
                k++;
            }
        }
        return model;
    }

    // lien nom du paramètre / valeur dans le contre-exemple
    private HashMap<String, Byte> counterExampleAsMap(Iterator<String> iter) {
        HashMap<String, Byte> paraValue = new HashMap<String, Byte>();
        // lit la valeur de tous les paramètres dans le contre-exemple
        String line;
        do {
            line = iter.next();
            if (line.startsWith("    K"))
                addValueForK(paraValue, line);
        } while (iter.hasNext());
        return paraValue;
    }

    private boolean snoussiIsVerified(HashMap<String, Byte> paraValue) {
        boolean snoussiOK = true;
        int ind = 0;
        while (snoussiOK & ind < super.net.nbOfGene()) {
            Gene g = super.net.genes().get(ind);
            List<Para> p = g.paras();
            int i = 0;
            while (snoussiOK & i < p.size()) {
                Para pi = p.get(i);
                int val = paraValue.get(pi.getNuSMVname());
                snoussiOK = true;
                for (Para sp : pi.succs) {
                    //System.out.println(" pi " + pi + " " + val + " sp " + sp + " " + paraValue.getWithStringId(sp.SMBname));
                    if (val > paraValue.get(sp.getNuSMVname()))
                        snoussiOK = false;
                }
                i++;
            }
            ind++;
        }
        if (!snoussiOK)
            System.out.println("SNOUSIIIIIIIII");
        return snoussiOK;
    }

    // PRE : la ligne commence par "    K"
    // POST : associe au paramètre correspondant la valeur du CE
    //      NOTa : tout est fait psf/r au =
    private void addValueForK(HashMap<String, Byte> paravalue, String line) {
        int i = line.indexOf("=");
        String paraName = line.substring(4, i).trim();
        //Byte value = new Byte(line.substring(i + 1, line.length()).trim());
        Byte value = Byte.parseByte(line.substring(i + 1, line.length()).trim());
        paravalue.put(paraName, value);
    }

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String s2 = "[#Selected  Models : " + FormalSearchEngine.nbSelected + "] ";
        String s3 = "[#Calls to NuSMV : " + nbNuSMVCall + "] [#Subsumed models : " +
                +nbSubsumed + "] [#Checked patterns : " +
                +nbSolvedPattern + "] [#TIME_OUT :" + nbTimeOut + "]";
        return s1 + s2 + s3;
    }


    ////////////////////
    // sauvegarde des résultats
    public void writeSearch(Clock c) throws Exception {
        //System.out.println("Order of parameters when printing");
        //System.out.println(ParasAsArray.printGeneParam());
        Out.psfln();
        Out.pfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Selected models: " + (super.OKModels.size() + counterExamples.size()));
        Out.psfln("# Computation time: " + c);

        Out.psfln("# Models subsumed by patterns : " + nbSubsumed);
        Out.psfln("# Models as counter-example : " + counterExamples.size());
        Out.psfln("# Calls to NuSMV : " + nbNuSMVCall);

        Out.pfln("################################################\n");
        //Out.pfln("Order of parameters when printing");
        //Out.pfln(ParasAsArray.printGeneParam());
        //Out.pfln("################################################\n");
        //System.out.println("ok " + super.OKModels.size() + " CE " + counterExamples.size());
        for (int i=0; i<OKModels.size();i++)
            ParasAsArray.writeDetailedModel(OKModels.get(i), true,i);
        Out.pfln("counterExamples :");
        for (int i=0; i<counterExamples.size();i++)
            ParasAsArray.writeDetailedModel(counterExamples.get(i), true,i);
        Out.pfln("KO models :");
        for (int i=0; i<KOModels.size();i++)
            ParasAsArray.writeDetailedModel(KOModels.get(i), false,i);
        if (KOPatterns.size() > 0) {
            for (SolvedPattern pat : KOPatterns) {
                Out.pfln("KO pattern :");
                pat.writeDetailled();
            }
        }
    }

    //////// CSV
    // sauvegarde des résultats en CSV
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        Out.psfln();
        Out.psfln();
        Out.pfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Selected models: " + super.OKModels.size() + counterExamples.size());
        Out.psfln("# Computation time: " + c);
        Out.psfln();
        String name = file + ".csv";
        Out.psfln("Models have been written in " + name);
        Out.psfln();

        BufferedWriter w = new BufferedWriter(new FileWriter(new File(name)));
        w.write(super.getCSVHead());
        int n = 1;
        for (byte[] b : super.OKModels)
            w.write(ParasAsArray.getCSV(n++, b, true));
        //w.write(separationLine(back.net.nbParams()));
        for (byte[] b : super.KOModels)
            w.write(ParasAsArray.getCSV(n++, b, false));
        // fermeture du fichier
        w.close();
    }

}
