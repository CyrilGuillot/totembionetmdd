package back.search;

import back.net.Gene;
import back.net.Net;
import back.net.Para;
import util.Out;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class is used to represent parameterizations as arrays
 * usefull to generate abstract parameterizations
 * to cut some branches during exploration
 *
 * 1 byte = la valeur de l'un des paramètres
 * byte[] : une paramétrisation possible f
 * NOTA : l'indice 0 vaut -1 si c'est formel, 0 sinon
 *
 * @author Hélène Collavizza
 */


public class ParasAsArray {


    // facteur pour générer les variables abstraites
    // 1/ABSTRACT_FACTOR variables sont abstraites
    private static int ABSTRACT_FACTOR = 3;

    // associe à un gène i le n° de début et de fin qui correspond a ses paramètres dans un byte[]
    private static HashMap<Integer, int[]> indiceGene;
    // associe à un gène i son nb de param non effectif
    // utile pour générer un csv avec les paras non effectifs
    private static HashMap<Integer, Integer> nbNonEffectiveParam;

    // associe à un gène g une map qui donne pour un niveau i la liste des
    // paramètres non fixé pour ce niveau
    public static HashMap<Gene, HashMap<Integer, List<Para>>> notFixedByDAGDeep;

    // indice des variables que l'on rend formelles
    // elles sont fixées une fois pour toute
    public static ArrayList<Integer> formalParasIndices;
    public static HashMap<Integer, Para> formalParas;

    // nb max de paramètres associé à un gène
    private static int maxParamNumber;
    // nb de gènes du réseau
    private static int nbOfGenes;
    private static Net net;

    // initialisation en fonction réseau dans
    // l'état initial de paramétrisation
    public static void init(Net n) {
        net = n;
        nbOfGenes = n.genes().size();
        maxParamNumber = 0;
        nbNonEffectiveParam = new HashMap<Integer,Integer>();
        System.out.println("nbNonEffec " + nbNonEffectiveParam);
        initIndice();
    }

    // fait l'indexation des gènes psf/r au tableau de byte
    private static void initIndice() {
        indiceGene = new HashMap<>();
        byte currentDeb = 1;
        for (int i = 0; i < nbOfGenes; i++) {
            int[] debFin = new int[2];
            debFin[0] = currentDeb;
            int si = net.genes().get(i).paraSize();
            if (maxParamNumber < si)
                maxParamNumber = si;
            debFin[1] = currentDeb + si - 1;
            currentDeb += si;
            indiceGene.put(i, debFin);
        }
    }

    // fait l'indexation des gènes psf/r au tableau de byte
    static void setNonEffective(int i, int nb) {
        nbNonEffectiveParam.put(i,nb);
    }

    // sélectionne les paramètres libres
    public static void initFormalParas() {
        formalParasIndices = new ArrayList<Integer>();
        formalParas = new HashMap<Integer, Para>();
        if (numberCanBeFree() > 0) {
            for (int i = 0; i < nbOfGenes; i++) {
                int debut = debFin(i)[0];
                Gene g = net.genes().get(i);
                List<Para> p = g.paras();
                for (int j = 0; j < p.size(); j++)
                    if (p.get(j).libre) {
                        //System.out.println("libre " + psf.getWithStringId(j));
                        formalParasIndices.add(debut + j);
                        formalParas.put(debut + j, p.get(j));
                    }
            }
        }
    }

    // renvoie le nb de paramètres que l'on rend libre en fonction
    // du facteur d'abstraction, du DAG et du nombre de paramètres fixés par
    // l'utilisateur
    // on rend libre en partant du haut du DAG (paramètres avec le plus de valeurs possibles)
    private static int numberCanBeFree() {
        int nbFree = 0;
        for (Gene g : net.genes()) {
            int nbPosibleFreePara = 0;
            for (Para p : g.paras())
                if (!p.fixe) nbPosibleFreePara++;
            int nbToBeFree = Math.min(nbPosibleFreePara, g.paraSize() / ABSTRACT_FACTOR);
            nbToBeFree = Math.max(1, nbToBeFree);
            //System.out.println("free " + nbToBeFree);
            int nbFreeP = 0;
            // pour chaque paramètres pris par ordre du DAG
            // en prenant les paras les plus complets d'abord
            for (int i = 0; i < g.dagSize() - 1 && nbFreeP < nbToBeFree; i++) {
                List<Para> paras = g.getDagLevel(i);
                for (Para p : paras)
                    if (!p.fixe) {
                        p.libre = true;
                        nbFreeP++;
                        nbFree++;
                    }
            }
        }
        return nbFree;
    }


    // renvoie le byte[] correspondant à la paramétrisation courante
    public static byte[] currentModelAsArray() {
        byte[] para = new byte[net.nbParams() + 1];
        para[0] = 0;
        int k = 1;
        for (int i = 0; i < nbOfGenes; i++) {
            Gene g = net.genes().get(i);
            for (Para p : g.paras()) {
                para[k] = (byte) p.currentMin();
                k++;
            }
        }
        //System.out.println("paras current " + getParaValue(para));
        return para;
    }


    public static byte[] modelToPattern(byte[] para) {
        if (formalParasIndices.isEmpty())
            return null;
        byte[] pat = para.clone();
        // c'est formel
        pat[0] = -1;
        int nbFormel = 0;
        // si le paramètre est libre et a plus d'une valeur alors on met -1
        for (int k : formalParasIndices) {
            Para p = formalParas.get(k);
            if (p.currentMin() < p.maxLevel()) {
                pat[k] = -1;
                nbFormel++;
            }
        }
        if (nbFormel == 0) return null;
        return pat;
    }

    // pour propager les informations de bornes
    // si s est un successeur de psf alors si s est formel, s.max <=psf.val
    private static void propagateFormalBounds() {
        net.resetFormalLevels();
        // pour chaque gène
        for (Gene g : net.genes()) {
            // pour chaque paramètres pris par ordre du DAG
            // en prenant les paras les plus complets d'abord
            for (int i = 0; i < g.dagSize() - 1; i++) {
                List<Para> paras = g.getDagLevel(i);
                //              System.out.println("level " + i + paras);
                for (Para p : paras) {
                    // borne max du successeur libre :
                    //     * borne max de psf si est libre
                    //     * valeur courante de psf sinon
                    // borne min du successeur libre : sa valeur courante
                    //     puisque les modèles sont énumérés par ordre croissant
                    //    System.out.println("pred " + psf.preds);
                    for (Para pred : p.preds) {
                        if (pred.libre) {
                            int m = p.libre ? p.maxFormalLevel() : p.currentMin();
                            pred.setMaxFormalLevel(m);
                            //pred.setMinFormalLevel(pred.currentMin());
                        }
                    }
                }
            }
            for (Para p : g.paras()) {

                if (p.minFormalLevel() == p.maxFormalLevel())
                    p.libre = false;

            }
        }
    }

    // tous les param qui ont été libérés peuvent reprendre une valeur
    public static void unFree() {
        for (Gene g : net.genes()) {
            for (Para p : g.paras())
                p.libre = false;
        }
    }

    // sauve le model pattern dans le fichier ouvert dans util.Out
    public static void writeModel(byte[] pattern, boolean isPhi) throws Exception {
        String ok = (isPhi) ? "OK" : "KO";
        String type = (pattern[0] == -1) ? "Pattern" : "Model";
        Out.psfln(ok + " " + type);
        for (int i = 0; i < nbOfGenes; i++) {
            int[] db = indiceGene.get(i);
            Out.psfln("    " + net.genes().get(i) + " " + getParaValue(pattern, db[0], db[1] + 1));
        }
    }

    public static void printDetailedModel(byte[] pattern, int n) throws Exception {
        String type = (pattern[0] == -1) ? "Pattern" : "Model";
        Out.psf(type + " " + n + ":");
        for (int i = 0; i < nbOfGenes; i++) {
            int[] db = indiceGene.get(i);
            int k = db[0];
            Gene g = net.genes().get(i);
            for (Para p : g.paras()) {
                String val = (pattern[k] == -1) ? "X" : pattern[k] + "";
                k++;
                Out.psfln("   " + p.getSMBname() + " : " + val);
            }
            Out.psfln();
        }
    }

    // sauve le model pattern dans le fichier ouvert dans util.Out
    public static void writeDetailedModel(byte[] pattern, boolean isPhi,int m) throws Exception {
        String ok = (isPhi) ? "OK" : "KO";
        String type = (pattern[0] == -1) ? "Pattern" : "Model";
        Out.pfln(ok + " " + type + " #" + (m+1));
        for (int i = 0; i < nbOfGenes; i++) {
            int[] db = indiceGene.get(i);
            int k = db[0];
            Gene g = net.genes().get(i);
            for (Para p : g.paras()) {
                String val = (pattern[k] == -1) ? "X" : pattern[k] + "";
                k++;
                Out.pfln("   " + p.getSMBname() + " : " + val);
            }
        }
    }

    public static void writeDetailedPattern(byte[] pattern, HashMap<Integer, Integer> formalParasMin) throws Exception {
        for (int i = 0; i < nbOfGenes; i++) {
            int[] db = indiceGene.get(i);
            int k = db[0];
            Gene g = net.genes().get(i);
            for (Para p : g.paras()) {
                String val;
                if (pattern[k] == -1)
                    val = "X" + "   " + formalParasMin.get(k) + ".." + g.max();
                else
                    val = pattern[k] + "";
                k++;
                Out.pfln("   " + p + " : " + val);
            }
        }
        Out.pfln("");
    }


    // affiche le model pattern sur la console
    public static void printModel(byte[] pattern) throws Exception {
        if (pattern != null) {
            for (int i = 0; i < nbOfGenes; i++) {
                int[] db = indiceGene.get(i);
                Out.psfln("    " + net.genes().get(i) + " " + getParaValue(pattern, db[0], db[1] + 1));
            }
        }
    }

    // affichage de la valeurs des paramètres pour un gène
    public static String getParaValue(byte[] b, int d, int f) {
        String r = "";
        for (int i = d; i < f; i++)
            r += (b[i] == -1) ? "-" : b[i] + "";
        return r;
    }

    // renvoie une ligne avec toutes les valeurs des paramètres séparées par ;
    // pour mettre dans un CSV
    public static String getCSV(int n, byte[] pattern, boolean isPhi) throws Exception {
        String ok = (isPhi) ? "OK" : "KO";
        String csv = n + ";" + ok + ";";
        csv += getParasInCSV(pattern);
        return csv;
    }

    // renvoie la valeur des paramètres séparés par des ; pour format csv
    public static String getParasInCSV(byte[] pattern){
        String csv ="";
        for (int i = 0; i < nbOfGenes; i++) {
            int[] db = indiceGene.get(i);
            csv += getValueInCSV(pattern, db[0], db[1] + 1);
            // si on écrit dans un csv avec option verbose 2
            if (Out.verb()==2) {
                Gene g = net.genes().get(i);
                if (! net.isEnvVar(g)) {
                    for (int k = 0; k < nbNonEffectiveParam.get(i); k++)
                        csv += "-;";
                }
            }
        }
        return csv;
    }

    // affichage de la valeurs des paramètres pour un gène
    private static String getValueInCSV(byte[] b, int d, int f) {
        String r = "";
        for (int i = d; i < f; i++)
            r += (b[i] == -1) ? "-" : b[i] + ";";
        return r;
    }

    // affichage du nom des paramètres pour chaque gènes
    public static String printGeneParam() throws Exception {
        String s = "";
        for (int i = 0; i < nbOfGenes; i++) {
            Gene g = net.genes().get(i);
            s += g + ": ";
            for (Para p : g.paras())
                s += p + " ";
            if (i < nbOfGenes - 1)
                s += "\n";
        }
        return s;
    }

    ///////////////////////////
    // accesseurs
    public static int[] debFin(int n) {
        return indiceGene.get(n);
    }



//    public static void main(String[] s) {
//        back.search.ParasAsArray psf = new back.search.ParasAsArray();
//        byte[] pat = {0, 0, -1, 0, -1, 0};
//        byte[] ss = {0, 0, 0, 0, 0, 0};
//        byte[] ss2 = {2, 0, 0, 0, 0, 0};
//        System.out.println(psf.match(pat, ss2));
//    }

}



