package back.search;

/**
 * This class is used to search for valid models
 * All models are enumerated
 *
 * @author Hélène Collavizza
 */

import back.jclock.Clock;
import back.net.Net;
import back.run.NuSMV;
import util.Out;
import util.StdoutReader;

import java.io.*;
import java.util.Iterator;


public class InstanciatedSearchEngine extends AbstractSearchEngine {

    // nb modèles résolus par NuSMV
    static long nbSolvedModel = 0;

    public InstanciatedSearchEngine(Net n, String nuSMVcommand) {
        super(n, nuSMVcommand);
    }

    /////////////////////////////////////////////
    // gestion de la recherche

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String s2 = "[#Selected  Models : " + nbSelected + "] ";
        return s1 + s2;
    }

    ///////////////////////////////
    // méthodes de recherche de l'interface Search
    public void initSearch(String name) throws Exception {
        // on calcule le 1er modèle
        net.firstParameterization();
    }

    public boolean hasNext() {
        return super.net.nextParameterization();
    }


    // resoud un modèle s'il y en a un
    public void doSearch(String name, boolean csv) throws Exception {
        Process proc;

        //back.net.printAllCurrentIntervals();
        NuSMV.writeNet(net, name, null, csv);

        //System.out.println(".smv file has been generated, launching NuSMV");
        //EXECUTION DE NUSMV
        proc = super.nuSMVCall(name);
        nbSolvedModel++;
        Iterator<String> iter = StdoutReader.getIteratorStream(proc);
        String line = StdoutReader.readNuSMVHeader(iter);
        //StdoutReader.readNuSMVHeader(iter, NuSMV.HEADER_SIZE);
        // si ça s'arrête là, il y a un problème d'execution et on affiche le
        //message d'erreur de NuSMV
        if (!line.startsWith("-- specification")&&!iter.hasNext()) {
            StdoutReader.readNuSMVError(proc);
        } else {
            byte[] model = ParasAsArray.currentModelAsArray();
            super.analyseModel(line,iter, model);
        }
        proc.destroy();
    }



    ////////////////////
    // sauvegarde des résultats
    public void writeSearch(Clock c) throws Exception {
        //System.out.println("Order of parameters when printing");
        //System.out.println(ParasAsArray.printGeneParam());
        Out.psfln();
        Out.pfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);
        Out.psfln("################################################\n");
        //Out.pfln("Order of parameters when printing");
        //Out.pfln(ParasAsArray.printGeneParam());
        //Out.pfln("################################################\n");

        for (int i=0;i< super.OKModels.size();i++) {
            ParasAsArray.writeDetailedModel(super.OKModels.get(i), true, i);
            Out.pfln("");
        }
        Out.pfln("\n########## KO models #########\n");
        for (int i=0;i< super.KOModels.size();i++) {
            ParasAsArray.writeDetailedModel(super.KOModels.get(i), false,i);
            if (Out.verb()>0)
                Out.pf("   Error explanation: " + super.errorExplanation.get(i).toString());
            Out.pfln("");
        }

    }

    // sauvegarde des résultats
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        Out.psfln();
        Out.psfln();
        Out.pfln("########### Result of model checking ###########");
        Out.psfln("# Total number of models " + super.net.NB_PARAMETERIZATIONS);
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);
        Out.psfln();
        String name = file + ".csv";
        Out.psfln("Models have been written in " + name);
        Out.psfln();

        BufferedWriter w = new BufferedWriter(new FileWriter(new File(name)));
        w.write(super.getCSVHead());
        int n = 1;
        for (byte[] b : super.OKModels)
            w.write(ParasAsArray.getCSV(n++, b, true)+"\n");
        //w.write(separationLine(back.net.nbParams()));
        int i= 0;
        while (i< super.KOModels.size()) {
            w.write(ParasAsArray.getCSV(n++, super.KOModels.get(i), false));
            // écriture de l'erreur
            w.write("\""+super.errorExplanation.get(i).toString()+"\"\n");
            i++;
        }

        // fermeture du fichier
        w.close();
    }

}


