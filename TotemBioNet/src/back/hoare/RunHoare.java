package back.hoare;

import front.hoareFol.hoareParser.Parser;
import front.smbParser.Visitor;
import util.TotemBionetException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


/**
 * @author Hélène Collavizza
 */
public class RunHoare {
    private String command;
    private String hoarePath;
    private String fileName;
    private boolean hoareIsBuilt;

    public RunHoare(Visitor visitor, String path, String inputFile) {
        try {
            // Create a temporary directory
            hoarePath = path + "hoare/";
            new File(hoarePath).mkdirs();

            fileName = hoarePath + inputFile;

            String distdir = System.getenv("TOTEMPATH") + "/TotemBioNet/ressources/";
            FileOutputStream outstream = new FileOutputStream(new File(fileName + ".ml"));

            // Copy Ocaml core1
            insertfile(distdir + "core1.ml", outstream);

            // Generate BRN
            visitor.generateBRNOCaml(outstream);

            // Copy Ocaml core2
            insertfile(distdir + "core2.ml", outstream);

            // Generate triplets
            hoareIsBuilt = visitor.generateTripletOCaml(outstream);

            outstream.close();

            command = "ocaml " + fileName + ".ml > " + fileName + ".out";

        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
    }

    private static void insertfile(String src, FileOutputStream outstream) {
        try {
            FileInputStream instream = new FileInputStream(new File(src));
            byte[] buffer = new byte[1024];
            int length;

            while ((length = instream.read(buffer)) > 0) {
                outstream.write(buffer, 0, length);
            }

            instream.close();
        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
    }

    public HoareFormulas runHoare() throws Exception {
        // System.out.println(command);
        String cmd[] = {"sh", "-c", command};
        Process proc = Runtime.getRuntime().exec(cmd);

        int exitVal = proc.waitFor();
        if (exitVal == 0) {
            Parser hoareParser = new Parser();
            hoareParser.parse(fileName + ".out");
            String jsonFile = fileName + ".json";
            if (!(new File(jsonFile)).exists())
                throw new Exception("A problem has succeeded when creating Hoare json result file " + fileName + ".json");
            return new HoareFormulas(jsonFile);

        } else {
            throw new Exception(fileName + ".out has not been generated");
        }
    }

    public boolean canBeRun() {
        return hoareIsBuilt;
    }
}
