package back.run;

import front.yedParser.Parser;
import util.TotemBionetException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Hélène Collavizza
 */
public class Main {

    private static void printHelp() {
        System.out.println("TotemBioNet V1.1.1");
        System.out.println("******************\n");
        System.out.println("Parameter identification: ");
        System.out.println("   ./totembionet <input>.smb <options>");
        System.out.println("   Input : <input>.smb contains influence graph and (possibly) information on parameters, temporal properties, Hoare triple");
        System.out.println("   Output : <input>.out contains two sets of pameterizations : those that satisfy the properties and the others.");
        System.out.println("   Option details");
        System.out.println("      -o name    : Generate the output in <name>.out ");
        System.out.println("      -csv       : Generate also the output in a CSV file <output>.csv");
        System.out.println("      -verbose n : Verbosity level. 1: more information is printed, 2: auxilliary files (for NuSMV and Hoare) are not deleted");
        System.out.println("\nInformation on the BRN: ");
        System.out.println("   ./totembionet <input>.smb <options>");
        System.out.println("   Option details");
        System.out.println("      -paras     : Give the set of all effective parameters");
        System.out.println("      -simu      : To start simulation mode (without Hoare, without environment variables, not maintained)");
        System.out.println("\nTranslation to json / from yed");
        System.out.println("  ./totembionet <input>.smb -json     : Translate <input>.smb file in a JSON format <input>.json");
        System.out.println("  ./totembionet <input>.graphml -yed  : Translate <input>.graphml into <input>FromYed.smb.");
        System.out.println("       <input>.graphml contains BRN information built from yEd using some convention ");
        System.out.println("\nGeneration of the state transition graphs");
        System.out.println("  ./totembionet <input>.smb -stg     : Generate the SYNchronous and ASYNchrounous state transition graph into <input>-stg.json file in a JSON format");
        System.out.println("\n./totembionet -help : this help guide.");
    }

    public static void main(String[] args) throws Exception {

        //Présence d'une entrée
        if (args.length == 0)
            throw new TotemBionetException("No input, use -help");

        String[] options = {"-yed","-o","-verbose","-paras","-help","-json","-simu","-csv","-debug","-stg"};
        List<String> optionList = Arrays.asList(options);

        String input = "";

        //OPTIONS
        ArrayList<String> opts = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            if (args[i].startsWith("-")) {
                if (!optionList.contains(args[i]))
                    throw new TotemBionetException("Unknown option " + args[i]);
                opts.add(args[i]);
            }
            else if (i > 0 && (args[i - 1].equals("-verbose") || args[i - 1].equals("-o"))) {
                opts.add(args[i]);
            } else {
                input = args[i];
            }
        }

        String path = "./";
        int slash = input.lastIndexOf("/");

        if (slash != -1 && slash != input.length()) {
            path = input.substring(0, slash + 1);
            input = input.substring(slash + 1);
        }

        if (opts.contains("-yed")) {
            Parser yedParser = new Parser();
            if (!input.endsWith(".graphml"))
                throw new TotemBionetException("The input file must be a .graphml file");
            yedParser.generateSMBFiles(path, input.substring(0, input.length() - 8));

        } else if (opts.contains("-help")) {
            printHelp();
        } else {
            if (!input.endsWith(".smb"))
                throw new TotemBionetException("The input file must be a .smb file");
            Run run = new Run(opts, path, input.substring(0, input.length() - 4));
            run.run();
        }
    }
}


