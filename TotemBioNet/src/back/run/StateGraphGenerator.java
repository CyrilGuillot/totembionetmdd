package back.run;


import back.jlist.OrderedArrayList;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import back.net.Reg;
import util.TotemBionetException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Classe pour générer le graphe d'états sous la forme d'un fichier json
 * On peut générer un graphe synchrone et asynchrone
 *
 * Intentionnellement pas très objet ... pas de classe Graph et classes filles
 *     SyncGraph et AsyncGraph
 *
 * Des méthodes sur le réseau pourraient passer dans la classe Net mais pas utile
 * ailleurs
 *
 * @author helen
 */
class StateGraphGenerator {
    private Net net;
    private HashMap<Long, HashSet<Long>> asyncGraph;
    private HashMap<Long, HashSet<Long>> syncGraph;

    StateGraphGenerator(Net n) throws TotemBionetException {
        net=n;
        asyncGraph = new HashMap<Long, HashSet<Long>>();
        syncGraph = new HashMap<Long, HashSet<Long>>();
        buildGraphs();
    }

    private void buildGraphs() throws TotemBionetException{
        buildAsynchronousGraph(asyncGraph);
        buildSynchronousGraph(syncGraph);
    }

    ///////////////////////////////////
    // gestion des arcs et construction du graphe

    /**
     *
     * @param from: départ de l'arc
     * @param to : arrivée de l'arc
     */
    private void addOneEdge(HashMap<Long, HashSet<Long>> g,Long from, Long to){
        if (g.containsKey(from)) {
            HashSet list = g.get(from);
            list.add(to);
        }
        else {
            HashSet list = new HashSet<Long>();
            list.add(to);
            g.put(from,list);
        }
    }

    /**
     * for adding an edge from vectors of Longs
     * @param from
     * @param to
     */
    private void addOneVectorEdge(HashMap<Long, HashSet<Long>> g,ArrayList<Long> from, ArrayList<Long> to){
        addOneEdge(g,vectorToLong(from),vectorToLong(to));
    }

    /**
     * construit le graphe synchrone
     * il faut énumérer tous les états possibles et dans chaque état
     * calculer le paramètre applicable => c'est le prochaine valeur de la variable
     * @param g : le graphe construit
     */
    public void buildSynchronousGraph(HashMap<Long, HashSet<Long>> g) throws TotemBionetException{
        //System.out.println("ordre des variables " + net.geneList());
        firstState();
        do {
//            System.out.println(net.currentState());
//            System.out.println(vectorToLong(net.currentState()));
//            System.out.println(getApplicableParas());
//            System.out.println(getApplicableParaValues());
            addOneVectorEdge(g,net.currentState(),getApplicableParaValues());
        } while (nextState());
    }

    /**
     * construit le graphe asynchrone
     * il faut énumérer tous les états possibles et dans chaque état
     * calculer le paramètre applicable
     * ensuite il faut asynchroniser en avançant par pas de 1
     * @param g : le graphe construit
     */
    public void buildAsynchronousGraph(HashMap<Long, HashSet<Long>> g) throws TotemBionetException {
        //System.out.println("ordre des variables " + net.geneList());
        firstState();
        do {
//            System.out.println(net.currentState());
//            System.out.println(vectorToLong(net.currentState()));
//            System.out.println(getApplicableParas());
//            System.out.println(getApplicableParaValues());
            addAsynchronousEdges(g,net.currentState(), getApplicableParaValues());
        } while (nextState());
    }

    /**
     *
     * @param g
     * @param state : état courant
     * @param nextState : valeurs des paramètres applicables pour l'état suivant
     */
    private void addAsynchronousEdges(HashMap<Long, HashSet<Long>> g, ArrayList<Long> state, ArrayList<Long> nextState) {
        for (int i = 0; i < state.size(); i++) {
            //System.out.println("couple " + state + "=>" + nextState);
            long sti = state.get(i);
            long nsti = nextState.get(i);
            ArrayList<Long> nst = (ArrayList<Long>) state.clone();
            if (sti < nsti) {
                nst.set(i, sti+1);
                //System.out.println("adding edge " + state + "=>" + nst);
                addOneVectorEdge(g,state, nst);
            }
            if (sti > nsti) {
                nst.set(i, sti-1);
                //System.out.println("adding edge " + state + "=>" + nst);
                addOneVectorEdge(g,state, nst);
            }
        }
    }
    ///////////////////////////////////
    // méthodes nécessaires sur le réseau net

    /**
     * pour parcourir tous les états
     */
    private void firstState() {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                g.firstLevel();
        }
    }
    private boolean nextState() {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g) & g.nextLevel())
                return true;
        }
        return false;
    }

    /**
     *
     * @return les paramètres applicables sur l'état courant
     * @throws TotemBionetException
     */
    private ArrayList<Para> getApplicableParas() throws TotemBionetException{
        ArrayList<Para> app = new ArrayList<Para>();
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                app.add(getApplicablePara(g));
        }
        return app;
    }

        /**
     *
     * @return la valeur des paramètres applicables sur l'état courant
     * @throws TotemBionetException
     */
    private ArrayList<Long> getApplicableParaValues() throws TotemBionetException {
        ArrayList<Long> app = new ArrayList<Long>();
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                // NOTA: je prends le min, puisque le paramètre est fixe,
                // ça ne pose pas de problème
                app.add((long) getApplicablePara(g).currentMin());
        }
        return app;
    }

    /**
     *
     * @param g : un gène
     * @return : le paramètre applicable de ce gène pour la
     *           configuration actuelle de l'état
     */
    private Para getApplicablePara(Gene g) throws TotemBionetException{
        OrderedArrayList<Reg> paraRegs = new OrderedArrayList<Reg>();
        for (Reg r : g.getRegs()) {
            if (r.isEffective()) {
                paraRegs.addAlpha(r);
            }
        }
        //On construit le nom du param�tre
        String paraNuSMVName = "K" + g.name;
        for (int i = 0; i < paraRegs.size(); i++) {
            paraNuSMVName += "_" + paraRegs.get(i);
        }
        //On regarde si le param�tre existe d�j�
        Para p = g.getParas().getWithStringId(paraNuSMVName);
        if (p.fixe)
            return p;
        throw new TotemBionetException("All parameters must have been set " +
                "before building the state transition graph.\n" +
                "Use option \"-paras\" to know the set of effective parameters");
    }

    /**
     * utilitaire pour traduire un vecteur d'entiers en Long
     */
    private Long vectorToLong(ArrayList<Long> list){
        long r =0;
        for (Long i : list) {
            r = r*10 + i;
        }
        return r;
    }

    //////////////////////////////////////
    // affichages
    public String toString() {
        return "Asynchronous graph\n" + asyncGraph + "Synchronous graph\n" + syncGraph;
    }

    // format json
    private String adjListToJson(HashMap<Long, HashSet<Long>> g){
        String j= "[";
        int i=0;
        for (Map.Entry<Long, HashSet<Long>> h: g.entrySet()) {
            j += "{" ;
            j += "\"node\": " + h.getKey() + ",";
            j += "\"succs\": " + h.getValue() ;
            j +="}";
            if (i<g.size()-1)
                j+= ",";
            i++;
        }
        j+= "]";
        return j;
    }
    public String toJson() {
        String j="{ \"sync-graph\":";
        j+=adjListToJson(syncGraph);
        j+=", \"async-graph\":";
        j+=adjListToJson(asyncGraph);
        j+="}";
        return j;
    }

}
