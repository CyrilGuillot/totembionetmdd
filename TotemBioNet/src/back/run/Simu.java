package back.run;

import java.io.*;

import back.net.Net;
import back.net.Para;
import util.Out;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adrien Richard
 */


public class Simu {

    //Les tendances
    private static final int UP = 1;
    private static final int STEADY = 0;
    private static final int DOWN = -1;
    private static final int UNKNOWN = -2;

    //Lecture des commandes
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    //Niveaux des g�nes � l'�tat courant
    private static int level[];
    //Param�tres focaux � l'�tat courant
    private static Para para[];
    //Niveau max des param�tres focaux
    private static int max[];
    //Niveau min des param�tres focaux;
    private static int min[];
    //Tendance des g�nes � l'�tat courant
    private static int tendancy[];
    //Pour la commande goto
    private static final List<int[]> states = new ArrayList<int[]>();
    //Pour la commande getParaValue
    private static final List<String> simu = new ArrayList<String>();

    //Parcours interactif dans le graphe de transitions d'�tats

    public static void run(Net net) {
        try {
            //Initialisation des variables
            level = new int[net.genes().size()];
            para = new Para[net.genes().size()];
            max = new int[net.genes().size()];
            min = new int[net.genes().size()];
            tendancy = new int[net.genes().size()];
            //�tat initial
            Out.psfln("# SIMULATION MODE");
            Out.ps("> init\n");
            init(net);
            //Addition de l'�tat et mise � jour des variables
            keepState(net);
            while (true) {
                Out.ps("> ");
                //Lecture de la commade
                String command = removeWhite(in.readLine());
                //Commande vide
                if (command.equals(""))
                    continue;
                //Exit
                if (command.equals("quit"))
                    break;
                //Help
                if (command.equals("help")) {
                    Out.ps("> use: name_of_gene, index_of_state, init, getParaValue, quit, help\n");
                    continue;
                }
                //Evolution d'un g�ne
                int g = net.genes().indexOf(net.genes().getWithStringId(command));
                if (g >= 0) {
                    //Augmentation du g�ne
                    if (tendancy[g] == UP) {
                        level[g] += 1;
                        keepState(net);
                        continue;
                    }
                    //Diminution du g�ne
                    if (tendancy[g] == DOWN) {
                        level[g] -= 1;
                        keepState(net);
                        continue;
                    }
                    //Pas de changement d'�tat
                    if (tendancy[g] == STEADY)
                        Out.ps("> gene " + command + " is steady\n");
                    if (tendancy[g] == UNKNOWN)
                        Out.ps("> unknown tendancy\n");
                    continue;
                }
                //Retour � un �tat pr�c�dant
                try {
                    //Indice de l'�tat
                    int t = Integer.valueOf(command);
                    if (t < 0 || t >= states.size()) {
                        Out.ps("> bad index of state\n");
                        continue;
                    }
                    //Enregistrement l'�tat t
                    for (int i = 0; i < net.genes().size(); i++)
                        level[i] = states.get(t)[i];
                    //Suppression de la m�moire � partir de t (t compris)
                    while (states.size() > t) {
                        states.remove(t);
                        simu.remove(t);
                    }
                    //On revient � l'�tat t
                    keepState(net);
                    continue;
                } catch (NumberFormatException e) {
                }
                //Re-initialisation
                if (command.equals("init")) {
                    init(net);
                    keepState(net);
                    continue;
                }
                //Affichage de la simu
                if (command.equals("getParaValue")) {
                    for (int i = 0; i < simu.size(); i++)
                        Out.psf(simu.get(i));
                    continue;
                }
                //Mauvaise commande
                Out.ps("> not a command (see commands with help)\n");
            }
        } catch (Exception e) {
            System.err.println("Error : " + e);
            e.printStackTrace();
            System.exit(2);
        }
    }

    //R�-initialisation interactive

    private static void init(Net net) throws Exception {
        for (int i = 0; i < net.genes().size(); i++) {
            while (true) {
                Out.ps("> " + net.genes().get(i) + " = ");
                try {
                    level[i] = Integer.valueOf(removeWhite(in.readLine()));
                } catch (NumberFormatException e) {
                    Out.ps("> bad level for " + net.genes().get(i) + "\n");
                    continue;
                }
                if (level[i] < net.genes().get(i).min() || net.genes().get(i).max() < level[i]) {
                    Out.ps("> bad level for " + net.genes().get(i) + "\n");
                    continue;
                }
                break;
            }
        }
        states.clear();
        simu.clear();
    }

    //Enregistrement et affichage de l'�tat courant

    private static void keepState(Net net) throws Exception {
        //Mise � jour de l'�tat du r�seau par level et m�morisation
        int mem[] = new int[net.genes().size()];
        for (int i = 0; i < net.genes().size(); i++) {
            mem[i] = level[i];
            net.genes().get(i).setLevel(level[i]);
        }
        states.add(mem);
        //Mise � jour des variables
        boolean end = true;
        for (int i = 0; i < net.genes().size(); i++) {
            para[i] = net.genes().get(i).getFocalPara();
            min[i] = para[i].minMin();
            max[i] = para[i].maxMax();
            tendancy[i] = UNKNOWN;
            if (level[i] < min[i]) {
                tendancy[i] = UP;
                end = false;
            }
            if (level[i] > max[i]) {
                tendancy[i] = DOWN;
                end = false;
            }
            if (level[i] == min[i] && level[i] == max[i])
                tendancy[i] = STEADY;
        }
        //Affichage de l'�tat et enregistrement dans simu
        String message = "";
        if (states.size() == 1)
            message += pMem("# SIMU\n");
        message += pMem("# STATE " + (states.size() - 1) + "\n");
        for (int i = 0; i < net.genes().size(); i++) {
            //Niveau du g�ne
            message += pMem("# " + net.genes().get(i) + " = " + level[i] + " ");
            //Tendance du g�ne
            if (tendancy[i] == UP)
                message += pMem("[+]");
            if (tendancy[i] == STEADY)
                message += pMem("   ");
            if (tendancy[i] == DOWN)
                message += pMem("[-]");
            if (tendancy[i] == UNKNOWN)
                message += pMem("[?]");
            //Param�tre
            message += pMem("  " + para[i] + " = " + min[i]);
            if (max[i] > min[i])
                message += pMem(" " + max[i]);
            message += pMem("\n");
        }
        if (end)
            message += pMem("# END\n");
        simu.add(message);
    }

    private static String pMem(String s) throws Exception {
        Out.psf(s);
        return s;
    }

    //Suprime les charact�res espaces devant et derri�re s

    private static String removeWhite(String s) throws Exception {
        while (s.length() > 0)
            if (s.charAt(0) == ' ' | s.charAt(0) == '\t')
                if (s.length() > 1)
                    s = s.substring(1, s.length());
                else
                    s = "";
            else
                break;
        while (s.length() > 0)
            if (s.charAt(s.length() - 1) == ' ' | s.charAt(s.length() - 1) == '\t')
                if (s.length() > 1)
                    s = s.substring(0, s.length() - 1);
                else
                    s = "";
            else
                break;
        return s;
    }
}