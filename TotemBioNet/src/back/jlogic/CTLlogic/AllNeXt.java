package back.jlogic.CTLlogic;

import back.jlogic.Formula;

/**
 * classe pour représenter l'opérateur CTL AX
 *
 * @author Hélène Collavizza
 */

public class AllNeXt extends UnaryCTLFormula {

    public AllNeXt(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.AX + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new ExistNeXt(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        return new AllNeXt(getLeft().fairBio());
    }
}
