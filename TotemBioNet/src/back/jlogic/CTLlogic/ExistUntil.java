package back.jlogic.CTLlogic;

import back.jlogic.Formula;

/**
 * classe pour représenter l'opérateur CTL EU
 *
 * @author Hélène Collavizza
 */


public class ExistUntil extends BinaryCTLFormula {

    public ExistUntil(Formula l, Formula r) {
        super(l, r);
    }

    @Override
    public String toString() {
        return "E [" + getLeft() + " U " + getRight() + "]";
    }

    @Override
    public Formula fairBio() {
        return new ExistUntil(getLeft().fairBio(), getRight().fairBio());
    }
}
