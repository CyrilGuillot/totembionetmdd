package back.jlogic.CTLlogic;

import back.jlogic.Formula;
import back.jlogic.blogic.Not;

/**
 * classe pour représenter l'opérateur CTL AF
 *
 * @author Hélène Collavizza
 */

public class AllFuture extends UnaryCTLFormula {

    public AllFuture(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.AF + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new ExistFuture(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        Formula f = new ExistGenerally(new Not(getLeft().fairBio()));
        return new Not(f.fairBio());
    }

}
