package back.jlogic.CTLlogic;

import back.jlogic.Formula;

/**
 * classe pour représenter l'opérateur CTL EU
 *
 * @author Hélène Collavizza
 */


public class ExistFuture extends UnaryCTLFormula {

    public ExistFuture(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.EF + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new AllFuture(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        return new ExistFuture(getLeft().fairBio());
    }
}
