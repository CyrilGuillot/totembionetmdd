package back.jlogic.CTLlogic;

import back.jlogic.Formula;

/**
 * classe pour représenter l'opérateur CTL EG
 *
 * @author Hélène Collavizza
 */


public class ExistGenerally extends UnaryCTLFormula {

    public ExistGenerally(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.EG + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new AllGenerally(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        Formula fairL = getLeft().fairBio();
        return new ExistUntil(fairL, new AllGenerally(fairL));
    }
}
