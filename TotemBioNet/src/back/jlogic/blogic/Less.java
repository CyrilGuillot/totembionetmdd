package back.jlogic.blogic;

import back.jlogic.Formula;
import back.net.Para;
import util.Out;
import util.TotemBionetException;

import java.util.HashMap;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public class Less extends Comparator {

    public Less(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() < getRight().eval())
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) < getRight().eval(state))
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.LESS + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return this;
    }

    // on suppose que c'est de la forme (var < val)
    public void setParaValue(Para p) throws Exception{
        String smbName = getLeft().toString();
        if (smbName.equals(p.getSMBname())) {
            int val = getRight().eval() - 1;
            if (p.fixe & p.maxLevel()>val)
                throw new TotemBionetException("No models. PARA and HOARE values are conflicting for parameter " + smbName);
            int pValMax = p.maxLevel();
            int pValMin = p.minLevel();
            if (pValMin <= val & val <pValMax) {
                p.setMinMax(pValMin, val);
                if  (Out.verb()>=1){
                    //System.out.println("Domain of " + smbName + " has been reduced using Hoare constraint");
                    System.out.println("      Old domain: " + pValMin + ".." + pValMax +", New domain: " + p.minLevel() + ".." + p.maxLevel());
                }
            }
            else if (val < pValMin){
                throw new TotemBionetException("No models. HOARE constraints are conflicting for parameter " + smbName);
            }
        }
    }


}