package back.jlogic.blogic;

import back.jlogic.Formula;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class Equi extends BooleanFormula {

    public Equi(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        int left = getLeft().eval();
        int right = getRight().eval();
        if ((left > 0 && right > 0) || (left < 1 && right < 1))
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        int left = getLeft().eval(state);
        int right = getRight().eval(state);
        if ((left > 0 && right > 0) || (left < 1 && right < 1))
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.EQUI + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new Equi(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Equivalence is not a comparator");
    }
}