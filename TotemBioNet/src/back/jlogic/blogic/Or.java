package back.jlogic.blogic;

import back.jlogic.Formula;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public class Or extends BooleanFormula {

    public Or(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() > 0 || getRight().eval() > 0)
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) > 0 || getRight().eval(state) > 0)
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.OR + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new Or(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Or expression is not a comparator");
    }
}