package back.jlogic;

import util.TotemBionetException;
import java.util.*;

/**
 *
 *
 * @author Hélène Collavizza pour la partie logique temporelle et négation
 *
 *  à partir d'une version d'Adrien Richard
 *  le parsing des expressions d'Adrien Richard a été complètement remplacé
 *  par le parsing antlr4
 *
 * TODO : la négation est not(this) sauf :
 * - pour une formule P=>Q où elle est psf=>notQ
 * - pour une formule P&Q avec P ou Q implication où elle est !P | !Q
 */

abstract public class Formula {

    public String name;

    protected Formula() {
        name = null;
    }

    public String toExplanationString() {
        if (name!=null)
            return name;
        return toString();
    }

    // unités lexicales utilisées pour générer les opértaeurs NuSMV

    // connecteurs logiques
    protected static final String EQUI = "<->";
    protected static final String IMPLY = "->";
    protected static final String OR = "|";
    protected static final String AND = "&";
    protected static final String NOT = "!";

    // opérateurs de comparaison
    protected static final String LESS_EQ = "<=";
    protected static final String LESS = "<";
    protected static final String GREAT_EQ = ">=";
    protected static final String GREAT = ">";
    protected static final String DIFF = "!=";
    protected static final String EQUA = "=";

    // opérateurs entiers
    protected static final String SOUS = "-";
    protected static final String ADD = "+";
    protected static final String DIV = "/";
    protected static final String MULT = "*";

    // opérateurs temporels
    protected static final String AG = "AG";
    protected static final String EG = "EG";
    protected static final String AX = "AX";
    protected static final String EX = "EX";
    protected static final String AF = "AF";
    protected static final String EF = "EF";


    //METHODES ABSTRAITES

    //�valuation de la formule
    abstract public int eval();

    //�valuation de la formule pour un modèle donné
    // i.e. une instantiation des paramètres
    abstract public int eval(HashMap<String, Integer> paraState);

    // return the negation of f
    abstract public Formula negate();

    // return a list that contains all the formulas of a conjunction
    // @author Hélène Collavizza
    abstract public List<Formula> toSetOfConjunct() throws TotemBionetException;

    // transforme la formule CTL en formule CTL fair au sens bio
    // de la note d'Adrien Richard
    // @author Hélène Collavizza
    abstract public Formula fairBio();

    //initialisation du niveau de la variable SMBname
    abstract public void set(String name, int level);

    //insertion d'une variable dans la formule
    abstract public boolean insert(Var v);

}
